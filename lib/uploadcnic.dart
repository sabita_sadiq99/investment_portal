import 'dart:convert';
import 'dart:io';
import 'dart:ui';
import 'package:im_stepper/stepper.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:investment_portal_window/uploadmodel.dart';
import './welcome.dart';
import 'package:flutter/gestures.dart';
import './change_pass.dart';
import './setting.dart';
import './terms.dart';
import 'package:image_picker/image_picker.dart';

import 'account_menu.dart';
import 'api.dart';

 List<dynamic> images_blob = List<dynamic>();
 List<dynamic> images_name = List<dynamic>();
class UploadCnic extends StatefulWidget{
  _UploadCnicState createState() => _UploadCnicState();
}
class _UploadCnicState extends State<UploadCnic>{
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  int activeStep = 0; // Initial step set to 5.
  int _selectedIndex = 4;

    bool valuefirst = false;  
   void _onItemTapped(int index) {
    if(index==0)
    {
       Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Welcome()),
  );
    }

   if(index == 4)
   {
      Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Setting()),
  );
   }
  }
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
       backgroundColor: Colors.white,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
             
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 10.0,
                    vertical: 60.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      
                      Row(
                        children:[
                          Padding(padding: EdgeInsets.only(left:10.0),child:
                                 IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: HexColor('#0071BC'),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  }, ), ), Spacer(),
 RichText(text: TextSpan(text:'Skip',recognizer: TapGestureRecognizer()..onTap= (){ Navigator.push(context,MaterialPageRoute(builder: (context) => ChangePass()),);},style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                          fontSize: 15.0,
                          decoration: TextDecoration.underline,
                        ), )),] ),
                     
   Center(
      child:Stack(children: [
        CircleAvatar(radius:90.0,
        child:new Image.asset(
              'logo-white.png',
              width: 120.0,
              height: 120.0,
              fit: BoxFit.contain,
),backgroundColor: HexColor('#1b2f35'),), ],)),
      NumberStepper(
    numbers:[
        1,
        2,],
                enableStepTapping: false,
                lineColor: HexColor('#1b2f35'),
                numberStyle: TextStyle(color:Colors.white),
                activeStepBorderColor: Colors.white,
                activeStepColor: HexColor('#0071BC'),
                enableNextPreviousButtons: false,
                activeStep: activeStep,
                onStepReached: (index) {
                  setState(() {
                    activeStep = index;
                  });
                },
              ),
              header(),])))]))),
        bottomNavigationBar:BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag_outlined),
            label: 'My Orders',
          ),
            BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
            
          ),
       
          
        ],
        iconSize: 30.0,
        currentIndex: _selectedIndex,
        selectedItemColor: HexColor('#0071BC'),
        unselectedItemColor: HexColor('#7C7D7E'),
        unselectedLabelStyle: TextStyle(color:Colors.black),
        onTap: _onItemTapped,
      ),
                     );
  }
  header()
  {
    switch(activeStep){
      case 0:
        return Column(children: [
          SizedBox(height: 90.0,),
           Text('Upload front and back of CNIC',style: GoogleFonts.montserrat(
                          color: Colors.black,
                          
                          fontSize: 20.0,
                          letterSpacing: 0.2 ,
                          fontWeight: FontWeight.w400,
                         
                        ),),
                         SizedBox(height: 10.0,),
                                     Row(children: <Widget>[
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 30.0, right: 0.0,),
                  padding: EdgeInsets.only(top:15.0),
                  child: Divider(
                    color: Colors.black,
                    thickness: 0.7,
                    height:50,
                    
                  )),
            ),
 
          IconButton(
      // Use the FaIcon Widget + FontAwesomeIcons class for the IconData
      icon: FaIcon(FontAwesomeIcons.idCard,color: HexColor('#0071BC'),size: 50.0,), 
      onPressed: () { _showPicker(context); },
      
     ),
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 20.0, right: 5.0),
                   padding: EdgeInsets.only(top:15.0,),
                  child: Divider(
                    color: Colors.black,
                      thickness: 0.7,
                    height: 30,
                  )),
            ),
            Padding(padding: EdgeInsets.only(top:15.0,left: 5.0)
            ,child:Text(
                        'and',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                         
                          fontSize: 20.0,
                          fontWeight: FontWeight.w600,
                        ),
                      ), ),
 
 Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 5.0, right: 0.0,),
                  padding: EdgeInsets.only(top:15.0),
                  child: Divider(
                    color: Colors.black,
                    thickness: 0.7,
                    height:50,
                    
                  )),
            ),
 
          IconButton(
      // Use the FaIcon Widget + FontAwesomeIcons class for the IconData
      icon: FaIcon(FontAwesomeIcons.idCard,color: HexColor('#0071BC'),size: 50.0,), 
       onPressed: () { _showPicker(context); },
      
     ),
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 20.0, right: 20.0),
                   padding: EdgeInsets.only(top:15.0),
                  child: Divider(
                    color: Colors.black,
                      thickness: 0.7,
                    height: 30,
                  )),
            ),
          ]),
            Container(
      
        height: MediaQuery.of(context).size.height*0.13, 
        color: Colors.white,
 ),
            Row(children: [
             Theme(
    child: Checkbox(
      value: this.valuefirst,
      onChanged: (value) {  
                        setState(() {  
                          this.valuefirst = value;  
                        });  
                      }, ), 
    data: ThemeData(
      primarySwatch: Colors.blue,
      unselectedWidgetColor: HexColor('#7C7D7E'), // Your color
    ),
  ),
  
                              RichText(text: TextSpan(text: "Do you agree with our",style: TextStyle(fontSize:18.0, color: HexColor('#7C7D7E')),
                              children: <TextSpan>[
                                TextSpan(text: " "),
                                TextSpan(
                                  text:'Terms & Conditions',
                                   recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      Navigator.of(context).push(
              MaterialPageRoute(
                builder: (
                  context,
                ) {
                  // _showLoadingDialog();
                  return Term();
                },
              ),
            );
                    },style: TextStyle(fontSize:18.0,color:HexColor('#0071BC'),decoration: TextDecoration.underline,
                                )
                              )])),
                          Spacer(),
         ],),
         _buildLoginBtn(),            
        ],);
      case 1:
           return Column(children: [
          SizedBox(height: 90.0,),
           Text('Take or Upload your photo',style: GoogleFonts.montserrat(
                          color: Colors.black,
                          
                          fontSize: 20.0,
                          letterSpacing: 0.2 ,
                          fontWeight: FontWeight.w400,
                         
                        ),),
                         SizedBox(height: 20.0,),
                                     Row(children: <Widget>[
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 20.0),
                  padding: EdgeInsets.only(top:15.0),
                  child: Divider(
                    color: Colors.black,
                    thickness: 0.7,
                    height:50,
                    
                  )),
            ),
               Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 10.0, ),
                   padding: EdgeInsets.only(top:15.0,),
                  child: Material(
  type: MaterialType.transparency, //Makes it usable on any background color, thanks @IanSmith
  child: Ink(
    decoration: BoxDecoration(
      border: Border.all(color: HexColor("#00A14B"), width: 1.0),
      color: Colors.white,
      shape: BoxShape.circle,
    ),
    
    child: InkWell(
      //This keeps the splash effect within the circle
      borderRadius: BorderRadius.circular(1000.0), //Something large to ensure a circle
      onTap: null,
      child: Padding(
        padding:EdgeInsets.all(0.0),
        child: IconButton(
          icon:Icon(Icons.person),
          iconSize: 30.0,
          color: HexColor('#0071BC'),
          onPressed: (){
            _imgFromGallery();
          },
        ),
      ),
    ),
  )),),),
         Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 7.0, ),
                   padding: EdgeInsets.only(top:15.0,),
                  child: Divider(
                    color: Colors.black,
                      thickness: 0.7,
                    height: 30,
                  )),
            ),
            Padding(padding: EdgeInsets.only(top:15.0,left: 5.0)
            ,child:Text(
                        'Or',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                          fontSize: 20.0,
                          fontWeight: FontWeight.w600,
                        ),
                      ), ),
              Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 5.0, right: 0.0,),
                  padding: EdgeInsets.only(top:15.0),
                  child: Divider(
                    color: Colors.black,
                    thickness: 0.7,
                    height:50,
 )),
            ),
   Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 10.0, ),
                   padding: EdgeInsets.only(top:15.0,),
                  child: Material(
  type: MaterialType.transparency, //Makes it usable on any background color, thanks @IanSmith
  child: Ink(
    decoration: BoxDecoration(
      border: Border.all(color: HexColor("#00A14B"), width: 1.0),
      color: Colors.white,
      shape: BoxShape.circle,
    ),
    child: InkWell(
      //This keeps the splash effect within the circle
      borderRadius: BorderRadius.circular(1000.0), //Something large to ensure a circle
      onTap: null,
      child: Padding(
        padding:EdgeInsets.all(0.0),
        child: IconButton(
          icon: Icon(Icons.camera_alt),
          iconSize: 25.0,
          onPressed:(){
          _imgFromCamera();
          } ,
          color: HexColor('#0071BC'),
        ),),),)),),),
            Expanded(
              child: new Container(
                  margin: const EdgeInsets.only(left: 5.0, right: 20.0),
                   padding: EdgeInsets.only(top:15.0),
                  child: Divider(
                    color: Colors.black,
                      thickness: 0.7,
                    height: 30,
                  )),
            ),
          ]),
          Container(
      
        height: MediaQuery.of(context).size.height*0.12, 
        color: Colors.white,
 ),
            Row(children: [
             Theme(
    child: Checkbox(
      value: this.valuefirst,
      onChanged: (value) {  
                        setState(() {  
                          this.valuefirst = value;  
                        });  
                      }, ), 
    data: ThemeData(
      primarySwatch: Colors.blue,
      unselectedWidgetColor: HexColor('#7C7D7E'), // Your color
    ),
  ),
  
                              RichText(text: TextSpan(text: "Do you agree with our",style: TextStyle(fontSize:18.0, color: HexColor('#7C7D7E')),
                              children: <TextSpan>[
                                TextSpan(text: " "),
                                TextSpan(
                                  text:'Terms & Conditions',
                                   recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      Navigator.of(context).push(
              MaterialPageRoute(
                builder: (
                  context,
                ) {
                  return Term();},),);},style: TextStyle(fontSize:18.0,color:HexColor('#0071BC'),decoration: TextDecoration.underline, ) )])),
                 Spacer(),
         ],),
         _buildLoginBtn1(),          
        ],);
    }
  }
     Widget _buildLoginBtn1() {
    return Container(
          width:400,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 100.0,left: 90.0,top: 15.0,bottom: 15.0),child: Text(
        "Upload",
        style: GoogleFonts.montserrat(fontSize: 20,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
        onPressed: () {
          print(images_blob[0]);
          print(images_blob[1]);
          print(images_blob[2]);
          print(images_name[0]);
          print(images_name[1]);
          print(images_name[2]);
          UploadModal up = new UploadModal.novalue();
          up.username='k173659@nu.edu.pk';
          up.frontcnic = images_blob[0];
          up.backcnic = images_blob[1];
          up.dp = images_blob[2];
          up.f1 = images_name[0];
          up.f2 = images_name[1];
          up.f3 = images_name[2];
          API.Api.upload_image(up, session_id).then((value) {
            print(value.body);
          });
          images_name.clear();
          images_blob.clear();
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Setting()),
  );
}
    ),
                  );
  }
   Widget _buildLoginBtn() {
    return Container(
          width:400,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 100.0,left: 90.0,top: 15.0,bottom: 15.0),child: Text(
        "Next",
        style: GoogleFonts.montserrat(fontSize: 20,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
        onPressed: () {
            setState(() {
                    activeStep = 1;
                  });
}
    ),
                  );
  }
}
 void _showPicker(context) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.photo_library),
                    title: new Text('From Camera'),
                    onTap: () {
                      _imgFromCamera();
                      Navigator.of(context).pop();
                    }),
                      new ListTile(
                    leading: new Icon(Icons.photo_library),
                    title: new Text('Photo Library'),
                    onTap: () {
                      _imgFromGallery();
                      Navigator.of(context).pop();
                    }),
                     ],),),);});
}
_imgFromGallery() async {
       var image = await ImagePicker().getImage(
      source: ImageSource.gallery, imageQuality: 50
  );
  if(image != null)
  {
 final bytes = File(image.path).readAsBytesSync();
   String img64 = base64Encode(bytes);
  images_blob.add(img64);
  String file_name =image.path.split('/').last;
  images_name.add(file_name);
  }
}
_imgFromCamera() async {
       var image = await ImagePicker().getImage(
      source: ImageSource.camera, imageQuality: 50
  );
  if(image != null)
  {
 final bytes = File(image.path).readAsBytesSync();
  String img64 = base64Encode(bytes);
  images_blob.add(img64);
  String file_name =image.path.split('/').last;
  images_name.add(file_name);

  }
}