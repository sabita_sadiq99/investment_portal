class UserUpdate{
  String username;
  String lastname;
  String firstname;
  String street;
  String city;
  String country;
  String state;
  String cnic;
  String phone;


  factory UserUpdate.fromJson(Map<String, dynamic> json){
  return UserUpdate(
    json['pUsername'],
    json['pLastName'],
    json['pFirstName'],
    json['pStreet'],
    json['pCity'],
    json['pCountry'],
    json['pState'],
    json['pCNIC'],
    json['pPhone'],
  );
}
UserUpdate.novalue();
Map<String,dynamic> toJson()=>{
    'pUsername':username,
    'pLastName':lastname,
    'pFirstName':firstname,
    'pStreet':street,
    'pCity':city,
    'pCountry':country,
    'pState':state,
    'pCNIC':cnic,
    'pPhone' : phone,

};
UserUpdate(
  this.username,
  this.lastname,
  this.firstname,
  this.street,
  this.city,
  this.country,
  this.state,
  this.cnic,
  this.phone,
);
}