import 'package:flutter/material.dart';
import 'package:sms_otp_auto_verify/sms_otp_auto_verify.dart';
import 'dart:async';
import 'dart:ui';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import './new_pass.dart';
import 'package:firebase_auth/firebase_auth.dart';
import 'package:flutter_otp/flutter_otp.dart';

class PinPutApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      debugShowCheckedModeBanner: false,
      home: Scaffold(body: PinPutView()),
    );
  }
}

class PinPutView extends StatefulWidget {
  @override
  PinPutViewState createState() => PinPutViewState();
}

class PinPutViewState extends State<PinPutView> {
int _otpCodeLength = 6;
  String phoneNumber="+923412037275", verificationId;
  String otp, authStatus = "";
  bool _isLoadingButton = false;
  bool _enableButton = false;
  String _otpCode = "";
  final _scaffoldKey = GlobalKey<ScaffoldState>();
void SenOTP() async{

}
  void initState(){
   
    super.initState();
  }
Widget _buildLoginBtn() {
    return Container(
          width:350,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 90.0,left: 90.0,top: 15.0,bottom: 15.0),child: Text(
        "Next",
        style: GoogleFonts.montserrat(fontSize: 20,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
   
        onPressed: () {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => NewPass()),
  );
}
    ),
                  );
  }

  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
             
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 0.0,
                    vertical: 100.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Sent an OTP to your Mobile',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                         
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height:10.0),
                      Text('Please check your cell number',style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          
                          fontSize: 18.0,
                          letterSpacing: 0.2 ,
                          fontWeight: FontWeight.w500,
                         
                        ),
                        textAlign: TextAlign.center,),
                        Text('+92****24 & continue to reset password',style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          
                          fontSize: 18.0,
                          letterSpacing: 0.2 ,
                          fontWeight: FontWeight.w500,
                         
                        ),
                        textAlign: TextAlign.center,),
                      SizedBox(height: 30.0),
        Row(
          children:
          [
 
           TextFieldPin(
                  filled: true,
                  filledColor: Colors.grey[100],
                  codeLength: _otpCodeLength,
                  boxSize: 40,
                  onOtpCallback: (code, isAutofill) =>
                      _onOtpCallBack(code, isAutofill),
                ),
              Spacer(),
              Spacer(),
          ]
        ),
        SizedBox(height: 30.0),
        _buildLoginBtn(),
        SizedBox(height:10.0),
                          Row(
                            children:[
                              Spacer(),
                                 Text("Didn't Receive?",style: TextStyle(fontSize:18.0),),
                          Text(" Click Here",style: TextStyle(fontSize:18.0,color:HexColor('#0071BC') ),),
                          Spacer(),
                            ]
                          ),
                    ],
      ),

    ))]))));

  }
    Future<void> signIn(String otp) async {
    await FirebaseAuth.instance
        .signInWithCredential(PhoneAuthProvider.credential(
      verificationId: verificationId,
      smsCode: otp,
    ));
  }

  _verifyOtpCode() {
    FocusScope.of(context).requestFocus(new FocusNode());
    Timer(Duration(milliseconds: 4000), () {
      setState(() {
        _isLoadingButton = false;
        _enableButton = false;
      });

      _scaffoldKey.currentState.showSnackBar(
          SnackBar(content: Text("Verification OTP Code $_otpCode Success")));
    });
  }
  _onOtpCallBack(String otpCode, bool isAutofill) {
    setState(() {
      this._otpCode = otpCode;
      if (otpCode.length == _otpCodeLength && isAutofill) {
        _enableButton = false;
        _isLoadingButton = true;
        _verifyOtpCode();
      } else if (otpCode.length == _otpCodeLength && !isAutofill) {
        _enableButton = true;
        _isLoadingButton = false;
      }else{
        _enableButton = false;
      }
    });
  }
  otpDialogBox(BuildContext context) {
    return showDialog(
        context: context,
        barrierDismissible: false,
        builder: (BuildContext context) {
          return new AlertDialog(
            title: Text('Enter your OTP'),
            content: Padding(
              padding: const EdgeInsets.all(8.0),
              child: TextFormField(
                decoration: InputDecoration(
                  border: new OutlineInputBorder(
                    borderRadius: const BorderRadius.all(
                      const Radius.circular(30),
                    ),
                  ),
                ),
                onChanged: (value) {
                  otp = value;
                },
              ),
            ),
            contentPadding: EdgeInsets.all(10.0),
            actions: <Widget>[
              FlatButton(
                onPressed: () {
                  Navigator.of(context).pop();
                  signIn(otp);
                },
                child: Text(
                  'Submit',
                ),
              ),
            ],
          );
        });
  }
  
}

