import 'dart:convert';
import 'dart:ui';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:investment_portal_window/payment.dart';
import 'package:investment_portal_window/terms.dart';
import './setting.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import './welcome.dart';
import 'api.dart';
import 'favourites.dart';
import 'myorders.dart';
import 'ordermodel.dart';
import 'wallet.dart';
import './userimage.dart';
import 'package:investment_portal_window/sign_in.dart';
import 'package:investment_portal_window/account_menu.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class Checkout extends StatefulWidget{
  final int result;
  final String product_id;
  final int count;
  Checkout({Key key,@required this.result, this.product_id, this.count}) : super(key: key);
  _CheckoutState createState() => _CheckoutState(result: this.result,product_id: this.product_id,count: this.count);

}
class _CheckoutState extends State<Checkout>{
  int result;
  String product_id;
  int count;
  double total_price=0;
  bool receive = false;
  List<dynamic> productDetails = List<dynamic>();
  List<dynamic> subDetails = List<dynamic>();
  _CheckoutState({this.result,this.product_id,this.count});
 final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
   int _selectedIndex = 1;
       void _onItemTapped(int index) {
    if (index == 0) {
      Navigator.push(context,MaterialPageRoute(builder: (context) => Welcome()),);
    }
     if (index == 1) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Favourites()),
      );
    }
    if (index == 2) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Orders()),
      );
    }

    if (index == 4) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Setting()),
      );
    }
  }
     void initState() {
    API.Api.getDetail(session_id).then((value) {
       setState(() {
      var data = json.decode(value.body);
      var res = data as List;
     // print(res[0]["TotalQuantity"]);
      for (int i = 0; i < res.length; i++) {
        productDetails.add(res[i]["prdDetails"]);
        // print(res[i]["pMaster"]);
        for(int j=0;j<productDetails.length;j++)
        {
          for(int k=0;k< productDetails[j].length;k++)
          {
            
            subDetails.add(productDetails[j][k]);
          }
        }
        productDetails.clear();
      }
      for(int i=0;i<subDetails.length;i++)
      {
        if(subDetails[i]['pDetails']['Id'] == product_id)
        {     List<dynamic> temp = List<dynamic>();
              temp.add(subDetails[i]);
              subDetails.clear();
              subDetails.add(temp[0]);
              print(subDetails[0]);
        }
      }
    total_price = count *  subDetails[0]['pDetails']['Price__c'];
     receive = true;
      });
    });
    super.initState();
  }
    @override
  Widget build(BuildContext context) {
    return receive?Scaffold(
       key: _scaffoldKey,
           drawer:Drawer(
        child: Container(
          //child: Your widget,
          color: HexColor('#1b2f35'),
          width: double.infinity,
          height: double.infinity,
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: HexColor('#1b2f35'),
                ),
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft + Alignment(0.2, 0),
                      child: new Image.asset(
                        'logo-white.png',
                        width: 100.0,
                        height: 100.0,
                        fit: BoxFit.contain,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomLeft + Alignment(0.2, 1.5),
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                           get_image()),
                        radius: 30.0,
                      ),
                    ),
                    Align(
                      alignment: Alignment(0.3, 1.5),
                      child: Text(
                        get_name(),
                        style: TextStyle(color: Colors.white, fontSize: 20.0),
                      ),
                    ),
                    SizedBox(
                      height: 2.0,
                    ),
                    Align(
                      alignment: Alignment(0.6, 1.8),
                      child: Text(
                        get_email(),
                        style: TextStyle(color: Colors.white, fontSize: 15.0),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 50.0),
              new Divider(
                color: Colors.white,
                thickness: 0.8,
              ),
              ListTile(
                leading: Icon(
                  Icons.home,
                  color: Colors.white,
                ),
                title: Text(
                  'Home',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: null,
              ),
              ListTile(
                leading: Icon(
                  Icons.shopping_bag,
                  color: Colors.white,
                ),
                title: Text(
                  'My Orders',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: (){
                   Navigator.push(context,MaterialPageRoute(builder: (context) => Orders()));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.account_balance_wallet,
                  color: Colors.white,
                ),
                title: Text(
                  'My Wallet',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
               onTap: (){
                   Navigator.push(context,MaterialPageRoute(builder: (context) => Wallet()));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.settings,
                  color: Colors.white,
                ),
                title: Text(
                  'Account Settings',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Setting()),
                  );
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.power_settings_new_rounded,
                  color: Colors.white,
                ),
                title: Text(
                  'Sign Out',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: () => _onBasicAlertPressed(context),),],),),),
                      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag_outlined),
            label: 'My Orders',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
          ),
        ],
        iconSize: 30.0,
      selectedItemColor: HexColor('#7C7D7E'),
         currentIndex: _selectedIndex,
        unselectedItemColor: HexColor('#7C7D7E'),
        unselectedLabelStyle: TextStyle(color: Colors.black),
        onTap: _onItemTapped,
        showUnselectedLabels: true,
      ),
       backgroundColor: Colors.white,
            body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 16.0,
                    vertical: 60.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children:[
                          Padding(padding: EdgeInsets.only(left:0.0),child:
                   IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: HexColor('#0071BC'),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),),
                CircularProfileAvatar(
                get_image(), 
                  radius: 25,
                  backgroundColor: Colors.transparent, 
                  elevation: 5.0, 
                  cacheImage: true, 
                  onTap: () => _scaffoldKey.currentState.openDrawer(),
                  showInitialTextAbovePicture: true, 
                  ),
                  Padding(padding:EdgeInsets.only(left: MediaQuery.of(context).size.width / 10,top:10.0) ,child:Text(
                          'Checkout',
                          style: GoogleFonts.montserrat(
                            color: HexColor('#0071BC'),
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),),
            
                        Spacer(),
                        Spacer(),
                      ]),
                      SizedBox(height: 20.0),
                      Padding(padding:EdgeInsets.only(left:14.0) ,child: Text(
                        'Complete your order by completing the form below.',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          fontSize: 18.0,
                          letterSpacing: 0.1,
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                        ),),
                      SizedBox(
                        height: 20.0,
                      ),
                          SizedBox(height:15.0),
                          Row(children: [
                            Padding(padding: EdgeInsets.only(left:10.0),child: Text('Order Summary',style: GoogleFonts.montserrat(
                          color: Colors.black,
                          fontSize: 18.0,
                          letterSpacing: 0.1,
                          fontWeight: FontWeight.w600,
                        ),),)],),
                        SizedBox(height:15.0),
                        breed_type("Prod_Name", subDetails[0]['pDetails']['Name']),
                        SizedBox(height:10),
                        breed_type("Total Quantity", count.toString()),
                         SizedBox(height:10.0),
                         breed_type("Unit Price", subDetails[0]['pDetails']['Price__c'].toString()),
                         SizedBox(height:10.0),
                         breed_type("Total Price", total_price.toString()),
                         SizedBox(height:10.0),
                         for_quantity("Total Quantity"),
                         SizedBox(height:30.0),
                           Row(children: [
                            Padding(padding: EdgeInsets.only(left:10.0),child: Text('Payment Method',style: GoogleFonts.montserrat(
                          color: Colors.black,
                          fontSize: 18.0,
                          letterSpacing: 0.1,
                          fontWeight: FontWeight.w600,
                        ),),)],),
                        SizedBox(height:20.0),
                        Row(children: [
                            Padding(padding: EdgeInsets.only(left:10.0),child: Text('We Accept',style: GoogleFonts.montserrat(
                          color: Colors.black,
                          fontSize: 18.0,
                          letterSpacing: 0.1,
                          fontWeight: FontWeight.w400,
                        ),),),
                        Padding(padding: EdgeInsets.only(left:10.0,right:10.0),child:Image(image: AssetImage('assets/visa.png')),),
                        Padding(padding: EdgeInsets.only(right: 10.0),child:Image(image: AssetImage('assets/mastercard.png')),),
                        Padding(padding: EdgeInsets.only(right: 10.0),child:Image(image: AssetImage('assets/americanexpress-color-large.png')),),
                        Padding(padding: EdgeInsets.only(right: 10.0),child:Image(image: AssetImage('assets/discover-light-large.png')),),
                        ],),
                        SizedBox(height:30.0),
                        Row(children: [
                          Spacer(),
                          _buildLoginBtn(),
                          Spacer(),
                        ],),
                        SizedBox(height:15.0),
                        Row(children: <Widget>[
                        Expanded(
                          child: new Container(
                              margin: const EdgeInsets.only(
                                  left: 10.0, right: 15.0),
                              child: Divider(
                                color: Colors.black,
                                thickness: 1,
                                height: 50,
                              )),
                        ),
                        Text("OR",style:GoogleFonts.montserrat(color: HexColor('#7C7D7E'),fontWeight: FontWeight.w500),),
                        Expanded(
                          child: new Container(
                              margin: const EdgeInsets.only(
                                  left: 15.0, right: 10.0),
                              child: Divider(
                                color: Colors.black,
                                thickness: 1,
                                height: 30,
                              )),
                        ),
                      ]),
                      SizedBox(height:10.0),
                      Row(children: [
               new InkWell(
              child: new Text('Pay by Bank and Upload Deposit Slip',style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                          fontSize: 16.0,
                          letterSpacing: 0.1,
                          decoration: TextDecoration.underline,
                        )),
              onTap: () {
              })],),
              SizedBox(height:30.0),
              _investBTN(product_id,count,subDetails[0]['pDetails']['Price__c']), 
              ])
                ))])))):Scaffold(
                              backgroundColor: Colors.white,
                              body:Center(
                              child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(height: 40.0),
                                CircularProgressIndicator(),
                              ],
                            )));}
                  _onBasicAlertPressed(context) {
                 Alert(
                 context: context,
                 title: "Are you sure you want to logout?",
                style: AlertStyle(
                titleStyle: GoogleFonts.montserrat(
                color: HexColor('#4A4B4D'),
                 fontSize: 18,
                fontWeight: FontWeight.w700),
                descStyle: GoogleFonts.montserrat(
                color: HexColor('#4A4B4D'),
                fontSize: 17,
                fontWeight: FontWeight.w400)),
                desc: "We can't notify you of new stocks if you do.",
                buttons: [
                DialogButton(
                child: Text(
                "Cancel",
                 style: GoogleFonts.montserrat(
                 color: HexColor('#4A4B4D'),
                fontSize: 18,
                 fontWeight: FontWeight.w700),
                 ),
                onPressed: () => Navigator.pop(context),
                color: Colors.white,
                border: Border.fromBorderSide(BorderSide(
                color: Colors.black, width: 0.8, style: BorderStyle.solid)),
                 radius: const BorderRadius.all(Radius.circular(50))),
                 DialogButton(
                child: Text(
                 "Logout",
                style: GoogleFonts.montserrat(
                 color: Colors.white,
                 fontSize: 18,
                fontWeight: FontWeight.w700),
                ),
                onPressed: () =>{
                if(get_login_method() == 1)
                 {
                signOutGoogle(),
                session_id = null,
                }
                else if(get_login_method() == 2)
                {

                },
                Navigator.push(context, MaterialPageRoute(builder: (context) => Account_select()),),
                },
                color: HexColor('#00A14B'),
                 border: Border.fromBorderSide(BorderSide(color: HexColor('#00A14B'), width: 2,style: BorderStyle.solid)),radius: const BorderRadius.all(Radius.circular(50))),], ).show();
      }
Widget for_quantity(String input1)
{
   return  
    new SizedBox(
        height: 46.0,
        child: new Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Container(
                alignment: Alignment.center,
                width:180,
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: Padding(
           padding: const EdgeInsets.only(top: 0.0, bottom: 0.0,left: 0.0,right: 0.0),
            child: new Text(input1,
               style:GoogleFonts.montserrat(
                    color: Colors.black,
                    fontSize: 17.0,
                    fontWeight: FontWeight.w300)),
          ),
           decoration: BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(50.0),bottomLeft:Radius.circular(50.0)),color: Colors.grey[100]),
          ),
         new Container(
                width:180,
                alignment: Alignment.center,
                child:Row(
                  children: [
                     Padding(
            padding: const EdgeInsets.only(top: 0.0, bottom: 0.0,left: 80.0,right:0.0),
            child: new Text("$count",
               style:GoogleFonts.montserrat(
                    color:  new HexColor('#7C7D7E'),
                    fontSize: 16.0,
                    fontWeight: FontWeight.w500)),
                   ),
                   Spacer(),
                   Padding(
                    child:Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: [                 
                    Container(margin: EdgeInsets.zero,padding: const EdgeInsets.all(0.0),height: 2,width: 30.0, child:IconButton(onPressed: (){print("hello");}, icon: Icon(Icons.arrow_drop_up),),),
                    SizedBox(height:10),
                    Container(padding: const EdgeInsets.all(0.0),height: 2,width: 30.0, child: IconButton(onPressed: (){}, icon: Icon(Icons.arrow_drop_down),),)],),
                    padding:EdgeInsets.only(right:15.0),
                    )
     ],),
          decoration: BoxDecoration(borderRadius: BorderRadius.only(topRight: Radius.circular(50.0),bottomRight:Radius.circular(50.0)),color: Colors.grey[50]),
            ),
        ],
      ),);
}

Widget breed_type(String input1, String input2)
    {
   
     return  
    new SizedBox(
        height: 46.0,
        child: new Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Container(
                alignment: Alignment.center,
                width:180,
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: Padding(
           padding: const EdgeInsets.only(top: 0.0, bottom: 0.0,left: 0.0,right: 0.0),
            child: new Text(input1,
               style:GoogleFonts.montserrat(
                    color: Colors.black,
                    fontSize: 17.0,
                    fontWeight: FontWeight.w300)),
          ),
           decoration: BoxDecoration(borderRadius: BorderRadius.only(topLeft: Radius.circular(50.0),bottomLeft:Radius.circular(50.0)),color: Colors.grey[100]),
          ),
         new Container(
                width:180,
                alignment: Alignment.center,
                padding: const EdgeInsets.symmetric(horizontal: 0.0),
                child: Padding(
            padding: const EdgeInsets.only(top: 0.0, bottom: 0.0,left: 0.0,right:0.0),
            child: new Text(input2,
               style:GoogleFonts.montserrat(
                    color:  new HexColor('#7C7D7E'),
                    fontSize: 16.0,
                    fontWeight: FontWeight.w500)),
          ),
          decoration: BoxDecoration(borderRadius: BorderRadius.only(topRight: Radius.circular(50.0),bottomRight:Radius.circular(50.0)),color: Colors.grey[50]),
            ),
        ],
      ),

   
);
    }
  Widget _buildLoginBtn() {
    return Container(
          width:200,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 0.0,left: 0.0,top: 15.0,bottom: 15.0),child: Text(
        "Add New Card",
        style: GoogleFonts.montserrat(fontSize: 15,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(HexColor('#7C7D7E')),
        shadowColor:  MaterialStateProperty.all<Color>(HexColor("#0071BC")),
        backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#0071BC"))
          )
        ),
         elevation:MaterialStateProperty.all(5.0),
      ),
        onPressed: () {
          Navigator.push(context,MaterialPageRoute(builder: (context) => Payment()),);
}
    ),
                  );
  }
   Widget _investBTN(String pid,int count,double price) {
    return Container(
          width:380,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 50.0,left: 50.0,top: 16.0,bottom: 16.0),child: Text(
        "Checkout",
        style: GoogleFonts.montserrat(fontSize: 20,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
        onPressed: () {
           Order ord = new Order.novalue();
            ord.productid =pid;
            ord.quantity= count.toString();
            ord.price =price.toString();
            ord.userid="0054K0000047AHYQA2";
            API.Api.createorder(ord, session_id).then((value) {
             String str = value.body;
             var arr = str.split('(');
             var arr1 = arr[1].split(')');
             var orderid= arr1[0];
            });}), );}
}
 