import 'dart:ui';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/gestures.dart';
import './userimage.dart';
import './setting.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import './welcome.dart';
import './terms.dart';
import './myorders.dart';
import 'package:dotted_line/dotted_line.dart';
import 'package:responsive_flutter/responsive_flutter.dart';
import 'package:investment_portal_window/sign_in.dart';
import 'package:investment_portal_window/account_menu.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

import 'wallet.dart';
class Transaction extends StatefulWidget{
  _TransactionState createState() => _TransactionState();
}
class _TransactionState extends State<Transaction> {
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();

       void _onItemTapped(int index) {
    if (index == 0) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Welcome()),
      );
    }
    if (index == 2) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Orders()),
      );
    }

    if (index == 4) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Setting()),
      );
    }
  }
 @override
 Widget build(BuildContext context) {
     double width = MediaQuery.of(context).size.width;
     print(width);
    return Scaffold(
       key: _scaffoldKey,
    drawer: Drawer(
        child: Container(
          //child: Your widget,
          color: HexColor('#1b2f35'),
          width: double.infinity,
          height: double.infinity,
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: HexColor('#1b2f35'),
                ),
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft + Alignment(0.2, 0),
                      child: new Image.asset(
                        'logo-white.png',
                        width: 100.0,
                        height: 100.0,
                        fit: BoxFit.contain,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomLeft + Alignment(0.2, 1.5),
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                           get_image()),
                        radius: 30.0,
                      ),
                    ),
                    Align(
                      alignment: Alignment(0.3, 1.5),
                      child: Text(
                        get_name(),
                        style: TextStyle(color: Colors.white, fontSize: 20.0),
                      ),
                    ),
                    SizedBox(
                      height: 2.0,
                    ),
                    Align(
                      alignment: Alignment(0.6, 1.8),
                      child: Text(
                        get_email(),
                        style: TextStyle(color: Colors.white, fontSize: 15.0),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 50.0),
              new Divider(
                color: Colors.white,
                thickness: 0.8,
              ),
              ListTile(
                leading: Icon(
                  Icons.home,
                  color: Colors.white,
                ),
                title: Text(
                  'Home',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: null,
              ),
              ListTile(
                leading: Icon(
                  Icons.shopping_bag,
                  color: Colors.white,
                ),
                title: Text(
                  'My Orders',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: (){
                   Navigator.push(context,MaterialPageRoute(builder: (context) => Orders()));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.account_balance_wallet,
                  color: Colors.white,
                ),
                title: Text(
                  'My Wallet',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
               onTap: (){
                   Navigator.push(context,MaterialPageRoute(builder: (context) => Wallet()));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.settings,
                  color: Colors.white,
                ),
                title: Text(
                  'Account Settings',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Setting()),
                  );
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.power_settings_new_rounded,
                  color: Colors.white,
                ),
                title: Text(
                  'Sign Out',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: () => _onBasicAlertPressed(context),
              ),
            ],
          ),
        ),
      ),
       bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag_outlined),
            label: 'My Orders',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
          ),
        ],
        iconSize: 30.0,
      selectedItemColor:HexColor('#7C7D7E'),
        unselectedItemColor: HexColor('#7C7D7E'),
        unselectedLabelStyle: TextStyle(color: Colors.black),
         showSelectedLabels: false,
      showUnselectedLabels: false,
        onTap: _onItemTapped,
      ),
      backgroundColor: Colors.white,
            body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[

              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal:ResponsiveFlutter.of(context).scale(10),
                    vertical: 70.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children:[
                          Padding(padding: EdgeInsets.only(left:0.0),child:
                                 IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: HexColor('#0071BC'),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),),
         CircularProfileAvatar(
          get_image(), 
          radius: 20,
          backgroundColor: Colors.transparent, 
          elevation: 5.0, 
          cacheImage: true, 
          onTap: () => _scaffoldKey.currentState.openDrawer(),
          showInitialTextAbovePicture: true, 
          ),
          Spacer(),
         
          Column(
            children:[
                     Text(
                        'Transaction',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),

                      ),
                      Padding(padding: EdgeInsets.only(right:9.0),child:Text(
                        'History',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold, ),),),]),
                      Spacer(),
                      Spacer(),
                    Spacer(),
                      ]
                      ),
                      SizedBox(height:40.0),
                       Row(
                      children: <Widget>[
                          Text("Mon, 1st March 2021",style: GoogleFonts.montserrat(
                                                      color: Colors.grey[600],
                                                      fontSize: 18.0)),        
                          Expanded(
                              child: Divider(thickness: 3.0,)
                          ),
                      ]
                  ),Container(
                              
                              child: ListView.builder(
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemExtent: 172.0,
                              padding: EdgeInsets.only(top:0.0,bottom: 0.0),
                              itemCount:2,
                              itemBuilder: (BuildContext context, int index) {
                               return Container(  
                                 margin: EdgeInsets.only(top:10.0),
                                height: double.infinity,
                                width: double.infinity,
                                decoration: BoxDecoration(
                                  color: HexColor('#F8F8F8'),
                               
                                  boxShadow: [
                                    BoxShadow(
                                      color: Colors.grey.withOpacity(0.5),
                                    
                                      offset: Offset(0, 3), // changes position of shadow
                                    ),
                                  ],
                                ),
                                child: Column(children: [
                                  Row(children: [
                                     Container(
                                       margin:EdgeInsets.all(10.0),
                                              width: 150.0,
                                              height: 80.0,
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    fit: BoxFit.cover,
                                                    image: NetworkImage('https://avatars0.githubusercontent.com/u/8264639?s=460&v=4')),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8.0)),),
                                            ),
                                 Column(
                                    crossAxisAlignment: CrossAxisAlignment.start,
                                   children: [
                                     Text("Buffalo",textAlign: TextAlign.start,style: GoogleFonts.montserrat(
                                                      color: Colors.grey[600],
                                                      fontSize: 20.0)),
                                    SizedBox(height:10.0),
                                   Text("ID: 42198726",style: GoogleFonts.montserrat(
                                                      color: Colors.grey[600],
                                                      fontSize: 20.0)),
                                 ],)
                                  ],),
                                  Row(children: [
                              DottedLine(lineLength:370,dashLength: 12,dashColor: HexColor('#DCDCDC'),dashGapLength: 12.0,),
                                  ],),
                                  Row(
                                      children: [
                                        Padding(child: Text('Quantity: 500 ',style: GoogleFonts.montserrat(color: Colors.grey[900],fontSize: 15.0,fontWeight: FontWeight.w500),),padding: EdgeInsets.only(left:10.0,top:5.0),),
                                        Spacer(),
                                          Padding(child:  RichText(
                                          text: TextSpan(
                                              text: 'Invested',
                                              style: GoogleFonts.montserrat(color: Colors.grey[900],fontSize: 15.0,fontWeight: FontWeight.w500),
                                              children: <TextSpan>[
                                                TextSpan(text: ' Rs',
                                                    style:GoogleFonts.montserrat(color: HexColor('#00A14B'),fontSize: 18.0),
                                                ),
                                                TextSpan(
                                                  text: ' 65,000',
                                            style: GoogleFonts.montserrat(color: Colors.grey[900],fontSize: 15.0,fontWeight: FontWeight.w500),
                                                )
                                              ]
                                          ),
                                        ) ,padding: EdgeInsets.only(right:10.0,top:5.0),),
                                         
                                      ],
                                  ),
                                  Row(children: [
                                    Padding(child: Text('Age: 62 months ', style: GoogleFonts.montserrat(color: Colors.grey[900],fontSize: 15.0,fontWeight: FontWeight.w500),),padding: EdgeInsets.only(left:10.0,top:5.0),),
                                    Spacer(),
                                  
                                          Padding(child:  RichText(
                                          text: TextSpan(
                                              text: 'Expected ROI',
                                              style: GoogleFonts.montserrat(color: Colors.grey[900],fontSize: 15.0,fontWeight: FontWeight.w500),
                                              children:[
                                                TextSpan(text: ' Rs',
                                                    style:GoogleFonts.montserrat(color: HexColor('#00A14B'),fontSize: 15.0),
                                                ),
                                                TextSpan(
                                                  text: ' 65,000',
                                              style: GoogleFonts.montserrat(color: Colors.grey[900],fontSize: 15.0,fontWeight: FontWeight.w500),
                                                )
                                              ]
                                          ),
                                        ) ,padding: EdgeInsets.only(right:10.0,top:5.0),),
                                  ],)
                                ],),
                              ); },))
                                        ])))]))));}
                                           _onBasicAlertPressed(context) {
                                                    Alert(
                                                      context: context,
                                                      title: "Are you sure you want to logout?",
                                                      style: AlertStyle(
                                                          titleStyle: GoogleFonts.montserrat(
                                                              color: HexColor('#4A4B4D'),
                                                              fontSize: 18,
                                                              fontWeight: FontWeight.w700),
                                                          descStyle: GoogleFonts.montserrat(
                                                              color: HexColor('#4A4B4D'),
                                                              fontSize: 17,
                                                              fontWeight: FontWeight.w400)),
                                                      desc: "We can't notify you of new stocks if you do.",
                                                      buttons: [
                                                        DialogButton(
                                                            child: Text(
                                                              "Cancel",
                                                              style: GoogleFonts.montserrat(
                                                                  color: HexColor('#4A4B4D'),
                                                                  fontSize: 18,
                                                                  fontWeight: FontWeight.w700),
                                                            ),
                                                            onPressed: () => Navigator.pop(context),
                                                            color: Colors.white,
                                                            border: Border.fromBorderSide(BorderSide(
                                                                color: Colors.black, width: 0.8, style: BorderStyle.solid)),
                                                            radius: const BorderRadius.all(Radius.circular(50))),
                                                        DialogButton(
                                                            child: Text(
                                                              "Logout",
                                                              style: GoogleFonts.montserrat(
                                                                  color: Colors.white,
                                                                  fontSize: 18,
                                                                  fontWeight: FontWeight.w700),
                                                            ),
                                                            onPressed: () =>{
                                                            if(get_login_method() == 1)
                                                            {
                                                              signOutGoogle(),
                                                              session_id = null,
                                                            }
                                                            else if(get_login_method() == 2)
                                                            {

                                                            },
                                                          
                                                            Navigator.push(context, MaterialPageRoute(builder: (context) => Account_select()),),
                                                            },
                                                            color: HexColor('#00A14B'),
                                                            border: Border.fromBorderSide(BorderSide(
                                                                color: HexColor('#00A14B'),
                                                                width: 2,
                                                                style: BorderStyle.solid)),
                                                            radius: const BorderRadius.all(Radius.circular(50))),
                                                      ],
                                                    ).show();
                                        }
}