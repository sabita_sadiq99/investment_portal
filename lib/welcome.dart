import 'dart:ui';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:custom_radio_grouped_button/custom_radio_grouped_button.dart';
import 'package:investment_portal_window/account_menu.dart';
import 'package:investment_portal_window/filtermodel.dart';
import 'package:investment_portal_window/sign_in.dart';
import 'package:investment_portal_window/userimage.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import './setting.dart';
import './detail.dart';
import 'package:english_words/english_words.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:getwidget/getwidget.dart';
import './api.dart';
import 'dart:convert';

import 'favouritemodel.dart';
import 'favourites.dart';
import 'myorders.dart';
import 'wallet.dart';


class Welcome extends StatefulWidget {
  _WelcomeState createState() => _WelcomeState();
}
class _WelcomeState extends State<Welcome> {
  List<dynamic> product_Ids = List<dynamic>();
  List<String> savedWords = [];
  List<dynamic> productDetails = List<dynamic>();
  List<dynamic> subDetails = List<dynamic>();
  List<dynamic> productDetails_search = List<dynamic>();
  List<dynamic> subDetails_search = List<dynamic>();
  List<dynamic> Category = List<dynamic>();
  List<dynamic> Breed = List<dynamic>();
  List<Map<dynamic, dynamic>> lists = [];
  int total = 0;
  int receive = 0;
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  bool isDairy = false;
  bool isPoultry = false;
  bool isCamel = false;
  bool isBulls = false;
  bool isSahiwal = false;
  bool isRedSindhi= false;
  bool isTharparkar = false;
  bool isNilli = false;
  bool isSibi = false;
  bool isDera = false;
  bool isBuffalo =  false;
  int _value = 1;
  List<dynamic> price = new List<dynamic>();
  bool isfav = false;
  int groupValue = 0;
  void initState()  {
    API.Api.getDetail(session_id).then((value) {
      setState(() {
      var data = json.decode(value.body);
      var res = data as List;
      for (int i = 0; i < res.length; i++) {
        total = total + res[i]["TotalQuantity"];
       
        productDetails.add(res[i]["prdDetails"]);

        for(int j=0;j<productDetails.length;j++)
        {
          for(int k=0;k<productDetails[j].length;k++)
          {
            subDetails.add(productDetails[j][k]);
          }
        }
        productDetails.clear();
      }
        receive = 1;
      });
    
    });

    super.initState();
  }

  void _onItemTapped(int index) {
    if (index == 0) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Welcome()),
      );
    }
     if (index == 1) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Favourites()),
      );
    }
     if (index == 2) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Orders()),
      );
    }

    if (index == 4) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Setting()),
      );
    }
  }

  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      drawer: Drawer(
        child: Container(
          //child: Your widget,
          color: HexColor('#1b2f35'),
          width: double.infinity,
          height: double.infinity,
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: HexColor('#1b2f35'),
                ),
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft + Alignment(0.2, 0),
                      child: new Image.asset(
                        'logo-white.png',
                        width: 100.0,
                        height: 100.0,
                        fit: BoxFit.contain,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomLeft + Alignment(0.2, 1.5),
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                           get_image()),
                        radius: 30.0,
                      ),
                    ),
                    Align(
                      alignment: Alignment(0.3, 1.5),
                      child: Text(
                        get_name(),
                        style: TextStyle(color: Colors.white, fontSize: 20.0),
                      ),
                    ),
                    SizedBox(
                      height: 2.0,
                    ),
                    Align(
                      alignment: Alignment(0.6, 1.8),
                      child: Text(
                        get_email(),
                        style: TextStyle(color: Colors.white, fontSize: 15.0),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 50.0),
              new Divider(
                color: Colors.white,
                thickness: 0.8,
              ),
              ListTile(
                leading: Icon(
                  Icons.home,
                  color: Colors.white,
                ),
                title: Text(
                  'Home',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: null,
              ),
              ListTile(
                leading: Icon(
                  Icons.shopping_bag,
                  color: Colors.white,
                ),
                title: Text(
                  'My Orders',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: (){
                   Navigator.push(context,MaterialPageRoute(builder: (context) => Orders()));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.account_balance_wallet,
                  color: Colors.white,
                ),
                title: Text(
                  'My Wallet',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
               onTap: (){
                   Navigator.push(context,MaterialPageRoute(builder: (context) => Wallet()));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.settings,
                  color: Colors.white,
                ),
                title: Text(
                  'Account Settings',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Setting()),
                  );
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.power_settings_new_rounded,
                  color: Colors.white,
                ),
                title: Text(
                  'Sign Out',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: () => _onBasicAlertPressed(context),
              ),
            ],
          ),
        ),
      ),
      endDrawer: Container(
        width: MediaQuery.of(context).size.width,
        child :Drawer(
        child: Container(
          height: double.infinity,
          width: double.infinity,
          color: Colors.white,
          child: ListView(
            children: <Widget>[
              new SizedBox(
                height: 50.0,
                child: new DrawerHeader(
                    child: Stack(
                      children: <Widget>[
                        Padding(
                          padding: EdgeInsets.only(left: 10.0, top: 10.0),
                          child: Row(children: [
                            IconButton(
                                onPressed: () {
                                  Navigator.pop(context);
                                },
                                icon: Icon(Icons.close),
                                color: Colors.red),
                            Text('  Filters',
                                style: GoogleFonts.montserrat(fontSize: 18.0)),
                          ]),
                        )
                      ],
                    ),
                    decoration: new BoxDecoration(color: Colors.white),
                    margin: EdgeInsets.zero,
                    padding: EdgeInsets.zero),
              ),
              Align(
                alignment: Alignment.topLeft + Alignment(0.2, 1.0),
                child: Text(
                  'Sort',
                  style: GoogleFonts.montserrat(
                    color: HexColor('#4A4B4D'),
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
            Sort(),
             
              SizedBox(height: 15.0),
              Align(
                alignment: Alignment.topLeft + Alignment(0.2, 1.0),
                child: Text(
                  'Price',
                  style: GoogleFonts.montserrat(
                    color: HexColor('#4A4B4D'),
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              SizedBox(height: 10.0),
              CustomCheckBoxGroup(
                buttonTextStyle: ButtonTextStyle(
                  selectedColor: Colors.white,
                  unSelectedColor: HexColor('#00A14B'),
                  textStyle: GoogleFonts.montserrat(
                      fontSize: 16, fontWeight: FontWeight.w400),
                ),
                autoWidth: false,
                enableButtonWrap: true,
                wrapAlignment: WrapAlignment.center,
                unSelectedColor: Colors.grey[200],
                buttonLables: [
                  "5K-10K",
                  "10K-15K",
                  "15K-20K",
                ],
                buttonValuesList: [
                  "First Range",
                  "Second Range",
                  "Third Range",
                ],
                checkBoxButtonValues: (values) {
                  //print(values);
                  price =  values;
                  print(price);
                },
                defaultSelected: ["1"],
                horizontal: false,
                width: 120,
                // hight: 50,
                selectedColor: HexColor('#00A14B'),
                selectedBorderColor: HexColor('#00A14B'),
                unSelectedBorderColor: Colors.grey[200],
                elevation: 6,
                padding: 5,
                enableShape: true,
              ),
              SizedBox(height: 15.0),
              Align(
                alignment: Alignment.topLeft + Alignment(0.2, 1.0),
                child: Text(
                  'Category',
                  style: GoogleFonts.montserrat(
                    color: HexColor('#4A4B4D'),
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
              category(),
              SizedBox(height: 15.0),
              Align(
                alignment: Alignment.topLeft + Alignment(0.2, 1.0),
                child: Text(
                  'Breed',
                  style: GoogleFonts.montserrat(
                    color: HexColor('#4A4B4D'),
                    fontSize: 22.0,
                    fontWeight: FontWeight.bold,
                  ),
                ),
              ),
               breed(),
              SizedBox(height: 50.0),
              Padding(
                padding: EdgeInsets.only(left: 20.0, right: 20.0),
                child: applybtn(),
              ),
                SizedBox(height: 20.0),
            ],
          ),
        ),
      )),
      backgroundColor: Colors.white,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 20.0,
                    vertical: 60.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(children: [
                        Padding(
                          padding: EdgeInsets.only(left: 40.0),
                          child: CircularProfileAvatar(
                            get_image(), //sets image path, it should be a URL string. default value is empty string, if path is empty it will display only initials
                            radius: 20, // sets radius, default 50.0
                            backgroundColor: Colors
                                .transparent, // sets background color, default Colors.white
                            elevation:
                                5.0, // sets elevation (shadow of the profile picture), default value is 0.0
                            cacheImage:
                                true, // allow widget to cache image against provided url
                            onTap: () => _scaffoldKey.currentState.openDrawer(),
                            showInitialTextAbovePicture:
                                true, // setting it true will show initials text above profile picture, default false
                          ),
                        ),
                        Spacer(),
                        Text(
                          'Welcome!',
                          style: GoogleFonts.montserrat(
                            color: HexColor('#0071BC'),
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),
                        Spacer(),
                        Spacer(),
                      ]),
                      SizedBox(height: 10.0),
                      Text(
                        'Choose where you love to Invest in',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          fontSize: 18.0,
                          letterSpacing: 0.1,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      SizedBox(
                        height: 20.0,
                      ),
                      Container(
                        height: 50,
                        width: MediaQuery.of(context).size.width / 1.1,
                        child: Row(
                          children: [
                            Expanded(
                              child: new Theme(
                                data: new ThemeData(
                                  primaryColor: HexColor('#0071BC'),
                                  primaryColorDark: HexColor('#0071BC'),
                                ),
                                child: Padding(
                                  padding: EdgeInsets.only(top:12.0),
                                  child:TextField(
                                  decoration: InputDecoration(
                                    hintText: 'Search Here',
                                    hintStyle: GoogleFonts.montserrat(
                                      color: HexColor('#7C7D7E'),
                                      fontSize: 18.0,
                                      letterSpacing: 0.1,
                                    ),
                                    contentPadding:
                                        EdgeInsets.only(top: 8.0, left: 25.0),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(50.0)),
                                      borderSide: BorderSide(
                                        color: HexColor('#0071BC'),
                                      ),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(50.0)),
                                      borderSide: BorderSide(
                                        color: HexColor('#0071BC'),
                                      ),
                                    ),
                                    fillColor: Colors.white,
                                    filled: true,
                                    suffixIcon: Icon(Icons.search,
                                        color: HexColor('#0071BC')),
                                  ),
                                ),),
                              ),
                            ),
                            Container(
                              decoration: BoxDecoration(
                                color: Colors.white,
                                borderRadius: BorderRadius.circular(9.0),
                              ),
                              child: IconButton(
                                icon: Icon(
                                  Icons.tune,
                                  color: HexColor('#0071BC'),
                                  size: 40.0,
                                ),
                                onPressed: () =>
                                    _scaffoldKey.currentState.openEndDrawer(),
                              ),
                            ),
                          ],
                        ),
                      ),
                      SizedBox(height: 30.0),
                     Row(children: [
                        Text(
                          "Dairy",
                          style: GoogleFonts.montserrat(
                            color: HexColor('#7C7D7E'),
                            fontSize: 20.0,
                            letterSpacing: 0.1,
                            fontWeight: FontWeight.w600,
                          ),
                        ),
                        Spacer(),
                        Text(
                          "See all",
                          style: GoogleFonts.montserrat(
                            color: HexColor('#0071BC'),
                            fontSize: 18.0,
                          ),
                        ),
                      ]),
                      if (receive == 1)
                        data_body()
                      else if(receive ==2)
                        data_body_search()
                      else
                       Center(
                              child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(height: 40.0),
                                CircularProgressIndicator(),
                              ],
                            )),
                    ],
                  ),
                ),
              ),
            ],
          ),
        ),
      ),
      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag_outlined),
            label: 'My Orders',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
          ),
        ],
        iconSize: 30.0,
        selectedItemColor: HexColor('#0071BC'),
        unselectedItemColor: HexColor('#7C7D7E'),
        unselectedLabelStyle: TextStyle(color: Colors.black),
        onTap: _onItemTapped,
        showUnselectedLabels: true,
      ),
    );
  }
Widget Sort()
{
  return Padding(
      padding: EdgeInsets.only(left: 25.0),
      child: Column(
        children: [
          SizedBox(height: 20.0),
          Row(children: [
            GFRadio(
              size: 25.0,
              activeBgColor: HexColor('#00A14B'),
              activeBorderColor: HexColor('#00A14B'),
              inactiveBorderColor: HexColor('#7C7D7E'),
              value: 0,
              groupValue: groupValue,
            onChanged: (val) {
              setState(() {
                groupValue = val;
                print(groupValue);
              });
            },
              inactiveIcon: null,
            ),
            Text(
              '  Recommended',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 18.0,
                letterSpacing: 0.1,
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
          SizedBox(height: 10.0),
          Row(children: [
            GFRadio(
              size: 25.0,
              activeBgColor: HexColor('#00A14B'),
              activeBorderColor: HexColor('#00A14B'),
              inactiveBorderColor: HexColor('#7C7D7E'),
              value: 1,
              groupValue: groupValue,
            onChanged: (val) {
              setState(() {
                groupValue = val;
                print(groupValue);
              });
            },
              inactiveIcon: null,
            ),
            Text(
              '  Age',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 18.0,
                letterSpacing: 0.1,
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
          SizedBox(height: 10.0),
          Row(children: [
           GFRadio(
              size: 25.0,
              activeBgColor: HexColor('#00A14B'),
              activeBorderColor: HexColor('#00A14B'),
              inactiveBorderColor: HexColor('#7C7D7E'),
              value: 2,
              groupValue: groupValue,
            onChanged: (val) {
              setState(() {
                groupValue = val;
                print(groupValue);
              });
            },
              inactiveIcon: null,
            ),
            Text(
              '  Lifestage',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 18.0,
                letterSpacing: 0.1,
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
          SizedBox(height: 10.0),
          Row(children: [
             GFRadio(
              size: 25.0,
              activeBgColor: HexColor('#00A14B'),
              activeBorderColor: HexColor('#00A14B'),
              inactiveBorderColor: HexColor('#7C7D7E'),
              value: 3,
              groupValue: groupValue,
            onChanged: (val) {
              setState(() {
                groupValue = val;
                print(groupValue);
              });
            },
              inactiveIcon: null,
            ),
            Text(
              '  Top rated',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 18.0,
                letterSpacing: 0.1,
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
        ],
      ),
    );
}
  Widget category() {
    return Padding(
      padding: EdgeInsets.only(left: 25.0),
      child: Column(
        children: [
          SizedBox(height: 20.0),
          Row(children: [
            GFCheckbox(
              size: 25.0,
              value: isDairy,
              activeBgColor: HexColor('#00A14B'),
              activeBorderColor: HexColor('#00A14B'),
              inactiveBorderColor: HexColor('#7C7D7E'),
              type: GFCheckboxType.square,
              onChanged: (value) {
                setState(() {
                  isDairy = value;
               if(value ==  true)
                  {
                    Category.add("'Dairy'");
                  }
                  else
                  {
                    Category.remove("'Dairy'");
                  }
                });
              },
            
              inactiveIcon: null,
            ),
            Text(
              '  Dairy',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 18.0,
                letterSpacing: 0.1,
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
          SizedBox(height: 10.0),
          Row(children: [
            GFCheckbox(
              size: 25.0,
              activeBgColor: HexColor('#00A14B'),
              activeBorderColor: HexColor('#00A14B'),
              inactiveBorderColor: HexColor('#7C7D7E'),
              type: GFCheckboxType.square,
              onChanged: (value) {
                setState(() {
                  isPoultry = value;
                 if(value ==  true)
                  {
                    Category.add("'Poultry'");
                  }
                  else
                  {
                    Category.remove("'Poultry'");
                  }
                });
              },
              value: isPoultry,
              inactiveIcon: null,
            ),
            Text(
              '  Poultry',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 18.0,
                letterSpacing: 0.1,
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
          SizedBox(height: 10.0),
          Row(children: [
            GFCheckbox(
              size: 25.0,
              activeBgColor: HexColor('#00A14B'),
              activeBorderColor: HexColor('#00A14B'),
              inactiveBorderColor: HexColor('#7C7D7E'),
              type: GFCheckboxType.square,
              onChanged: (value) {
                setState(() {
                  isCamel = value;
                  if(value ==  true)
                  {
                    Category.add("'Camel'");
                  }
                  else
                  {
                    Category.remove("'Camel'");
                  }
                  
                });
              },
              value: isCamel,
              inactiveIcon: null,
            ),
            Text(
              '  Camel',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 18.0,
                letterSpacing: 0.1,
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
          SizedBox(height: 10.0),
          Row(children: [
            GFCheckbox(
              size: 25.0,
              activeBgColor: HexColor('#00A14B'),
              activeBorderColor: HexColor('#00A14B'),
              inactiveBorderColor: HexColor('#7C7D7E'),
              type: GFCheckboxType.square,
              onChanged: (value) {
                setState(() {
                  isBulls = value;
                   if(value ==  true)
                  {
                    Category.add("'Bulls'");
                  }
                  else
                  {
                    Category.remove("'Bulls'");
                  }
                });
              },
              value: isBulls,
              inactiveIcon: null,
            ),
            Text(
              '  Bulls',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 18.0,
                letterSpacing: 0.1,
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
            SizedBox(height: 10.0),
          Row(children: [
            GFCheckbox(
              size: 25.0,
              activeBgColor: HexColor('#00A14B'),
              activeBorderColor: HexColor('#00A14B'),
              inactiveBorderColor: HexColor('#7C7D7E'),
              type: GFCheckboxType.square,
              onChanged: (value) {
                setState(() {
                  isBuffalo = value;
                   if(value ==  true)
                  {
                    Category.add("'Buffalo'");
                  }
                  else
                  {
                    Category.remove("'Buffalo'");
                  }
                });
              },
              value: isBuffalo,
              inactiveIcon: null,
            ),
            Text(
              '  Buffalo',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 18.0,
                letterSpacing: 0.1,
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
        ],
      ),
    );
  }

  Widget breed() {
    return Padding(
      padding: EdgeInsets.only(left: 25.0),
      child: Column(
        children: [
          SizedBox(height: 20.0),
          Row(children: [
            GFCheckbox(
              size: 25.0,
              activeBgColor: HexColor('#00A14B'),
              activeBorderColor: HexColor('#00A14B'),
              inactiveBorderColor: HexColor('#7C7D7E'),
              type: GFCheckboxType.square,
              onChanged: (value) {
                setState(() {
                  isSahiwal = value;
                  if(value ==  true)
                  {
                    Breed.add("'Sahiwal'");
                    print(Breed);
                  }
                  else
                  {
                    Breed.remove("'Sahiwal'");
                     print(Breed);
                  }
                });
              },
              value: isSahiwal,
              inactiveIcon: null,
            ),
            Text(
              '  Sahiwal',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 18.0,
                letterSpacing: 0.1,
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
          SizedBox(height: 10.0),
          Row(children: [
            GFCheckbox(
              size: 25.0,
              activeBgColor: HexColor('#00A14B'),
              activeBorderColor: HexColor('#00A14B'),
              inactiveBorderColor: HexColor('#7C7D7E'),
              type: GFCheckboxType.square,
              onChanged: (value) {
                setState(() {
                  isRedSindhi = value;
                     if(value ==  true)
                  {
                    Breed.add("'Red Sindhi'");
                     print(Breed);
                  }
                  else
                  {
                    Breed.remove("'Red Sindhi'");
                     print(Breed);
                  }
                });
              },
              value: isRedSindhi,
              inactiveIcon: null,
            ),
            Text(
              '  Red Sindhi',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 18.0,
                letterSpacing: 0.1,
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
          SizedBox(height: 10.0),
          Row(children: [
            GFCheckbox(
              size: 25.0,
              activeBgColor: HexColor('#00A14B'),
              activeBorderColor: HexColor('#00A14B'),
              inactiveBorderColor: HexColor('#7C7D7E'),
              type: GFCheckboxType.square,
              onChanged: (value) {
                setState(() {
                  isTharparkar = value;
                   if(value ==  true)
                  {
                    Breed.add("'Tharparkar'");
                   print(Breed);
                  }
                  else
                  {
                    Breed.remove("'Tharparkar'");
                     print(Breed);
                  }
                });
              },
              value: isTharparkar,
              inactiveIcon: null,
            ),
            Text(
              '  Tharparkar',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 18.0,
                letterSpacing: 0.1,
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
          SizedBox(height: 10.0),
          Row(children: [
            GFCheckbox(
              size: 25.0,
              activeBgColor: HexColor('#00A14B'),
              activeBorderColor: HexColor('#00A14B'),
              inactiveBorderColor: HexColor('#7C7D7E'),
              type: GFCheckboxType.square,
              onChanged: (value) {
                setState(() {
                  isNilli = value;
                    if(value ==  true)
                  {
                    Breed.add("'Nilli-Ravi'");
                   print(Breed);
                  }
                  else
                  {
                    Breed.remove("'Nilli-Ravi'");
                   print(Breed);
                  }
                });
              },
              value: isNilli,
              inactiveIcon: null,
            ),
            Text(
              '  Nilli-Ravi',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 18.0,
                letterSpacing: 0.1,
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
          SizedBox(height: 10.0),
          Row(children: [
            GFCheckbox(
              size: 25.0,
              activeBgColor: HexColor('#00A14B'),
              activeBorderColor: HexColor('#00A14B'),
              inactiveBorderColor: HexColor('#7C7D7E'),
              type: GFCheckboxType.square,
              onChanged: (value) {
                setState(() {
                  isSibi = value;
                  if(value ==  true)
                  {
                    Breed.add("'Sibi Bhagnari'");
                   print(Breed);
                  }
                  else
                  {
                    Breed.remove("'Sibi Bhagnari'");
                   print(Breed);
                  }
                });
              },
              value: isSibi,
              inactiveIcon: null,
            ),
            Text(
              '  Sibi Bhagnari',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 18.0,
                letterSpacing: 0.1,
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
          SizedBox(height: 10.0),
          Row(children: [
            GFCheckbox(
              size: 25.0,
              activeBgColor: HexColor('#00A14B'),
              activeBorderColor: HexColor('#00A14B'),
              inactiveBorderColor: HexColor('#7C7D7E'),
              type: GFCheckboxType.square,
              onChanged: (value) {
                setState(() {
                  isDera = value;
                   if(value ==  true)
                  {
                    Breed.add("'Dera Din'");
                   print(Breed);
                  }
                  else
                  {
                    Breed.remove("'Dera Din'");
                   print(Breed);
                  }
                });
              },
              value: isDera,
              inactiveIcon: null,
            ),
            Text(
              '  Dera Din',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 18.0,
                letterSpacing: 0.1,
                fontWeight: FontWeight.w400,
              ),
            ),
          ]),
        ],
      ),
    );
  }
  Widget data_body()
  {
      return Container(
                              child: ListView.builder(
                              padding: EdgeInsets.only(top:10.0),
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemExtent: 100.0,
                              itemCount: subDetails.length,
                              itemBuilder: (BuildContext context, int index) {
                              String word = subDetails[index]["pDetails"]['Id'];
                                bool isSaved =  subDetails[index]["pDetails"]["Favourite__c"];
                                return Card(
                                    elevation: 8.0,
                                    child: Container(
                                        decoration:
                                            BoxDecoration(color: Colors.white),
                                        child: ListTile(
                                            horizontalTitleGap: 5.0,
                                          leading: Wrap(children: [
                                            Container(
                                              width: 80.0,
                                              height: 60.0,
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    fit: BoxFit.cover,
                                                    image: NetworkImage('https://avatars0.githubusercontent.com/u/8264639?s=460&v=4')
                                                    ),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8.0)),
                                              ),
                                            ),
                                            Container(
                                                height: 60,
                                                child: VerticalDivider(
                                                    color: Colors.grey[500])),
                                          ]),
                                          title: Padding(
                                            padding: EdgeInsets.only(
                                                top: 5.0, left: 0.0),
                                            child: Text(
                                              subDetails[index]["pDetails"]
                                                  ["Name"],
                                              style: GoogleFonts.montserrat(
                                                color: HexColor('#4A4B4D'),
                                                fontSize: 20.0,
                                                letterSpacing: 0.1,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                          subtitle: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(height: 5.0),
                                              RichText(
                                                text: TextSpan(
                                                  text: 'Price',
                                                  style: GoogleFonts.montserrat(
                                                      color: Colors.black,
                                                      fontSize: 15.0),
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text: '     ${subDetails[index]["pDetails"]["Price__c"]}',
                                                        style: TextStyle(
                                                            color: HexColor(
                                                                '#7C7D7E'),
                                                            fontSize: 15.0))
                                                  ],
                                                ),
                                              ),
                                              RichText(
                                                text: TextSpan(
                                                  text: 'Mts',
                                                  style: GoogleFonts.montserrat(
                                                      color: Colors.black,
                                                      fontSize: 15.0),
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                       text: '       ${subDetails[index]["pDetails"]["Age__c"]}',
                                                        style: TextStyle(
                                                            color: HexColor(
                                                                '#7C7D7E'),
                                                            fontSize: 15.0))
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                          trailing: Column(children: [
                                            Icon(
                                              isSaved || subDetails[index]["pDetails"]["Favourite__c"]
                                                  ? Icons.favorite
                                                  : Icons.favorite_border,
                                              color: isSaved
                                                  ? Colors.red
                                                  : Colors.red,
                                            ),
                                            SizedBox(height: 14.0),
                                            InkWell(
                                              child: Icon(
                                                Icons.arrow_forward_ios,
                                                size: 18.0,
                                                color: HexColor('#00A14B'),
                                              ),
                                              onTap: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          Detail(product_id: subDetails[index]["pDetails"]
                                                  ["Id"] ,)),
                                                );
                                              },
                                            ),
                                          ]),
                                          onTap: () {
                                                      setState(() {
                                              if (isSaved) {
                                               
                                                savedWords.remove(word);
                                                Favourite fav = new Favourite.novalue();
                                                fav.productID = word;
                                                fav.flag = false;
                                                API.Api.submitfav(fav, session_id).then((value){
                                                  print(value.body);
                                                });
                                                
                                               } else {
                                                 
                                                savedWords.add(word);
                                                Favourite fav = new Favourite.novalue();
                                                fav.productID = word;
                                                fav.flag = true;
                                                API.Api.submitfav(fav, session_id).then((value){
                                                  print(value.body);
                                                });
                                              }
                                            });
                                            print(savedWords);
                                          },
                                        )));
                              },
                            ));
  }
  Widget data_body_search()
  {
    return Container(
                              child: ListView.builder(
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemExtent: 100.0,
                              itemCount: subDetails_search.length,
                              itemBuilder: (BuildContext context, int index) {
                                String word = product_Ids[index];

                                bool isSaved = subDetails[index]["pDetails"]["Favourite__c"];
                                print(isSaved);
                                return Card(
                                    elevation: 8.0,
                                    child: Container(
                                        decoration:
                                            BoxDecoration(color: Colors.white),
                                        child: ListTile(
                                            horizontalTitleGap: 5.0,
                                          leading: Wrap(children: [
                                            Container(
                                              width: 80.0,
                                              height: 60.0,
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    fit: BoxFit.cover,
                                                    image: MemoryImage(base64Decode(subDetails_search[index]['blobImageString']))),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8.0)),
                                              ),
                                            ),
                                            Container(
                                                height: 60,
                                                child: VerticalDivider(
                                                    color: Colors.grey[500])),
                                          ]),
                                          title: Padding(
                                            padding: EdgeInsets.only(
                                                top: 5.0, left: 0.0),
                                            child: Text(
                                              subDetails_search[index]["pDetails"]
                                                  ["Name"],
                                              style: GoogleFonts.montserrat(
                                                color: HexColor('#4A4B4D'),
                                                fontSize: 20.0,
                                                letterSpacing: 0.1,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                          subtitle: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(height: 5.0),
                                              RichText(
                                                text: TextSpan(
                                                  text: 'Price',
                                                  style: GoogleFonts.montserrat(
                                                      color: Colors.black,
                                                      fontSize: 15.0),
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text: '     ${subDetails_search[index]["pDetails"]["Price__c"]}',
                                                        style: TextStyle(
                                                            color: HexColor(
                                                                '#7C7D7E'),
                                                            fontSize: 15.0))
                                                  ],
                                                ),
                                              ),
                                              RichText(
                                                text: TextSpan(
                                                  text: 'Mts',
                                                  style: GoogleFonts.montserrat(
                                                      color: Colors.black,
                                                      fontSize: 15.0),
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                       text: '       ${subDetails_search[index]["pDetails"]["Age__c"]}',
                                                        style: TextStyle(
                                                            color: HexColor(
                                                                '#7C7D7E'),
                                                            fontSize: 15.0))
                                                  ],
                                                ),
                                              )
                                            ],
                                          ),
                                          trailing: Column(children: [
                                            Icon(
                                              isSaved
                                                  ? Icons.favorite
                                                  : Icons.favorite_border,
                                              color: isSaved
                                                  ? Colors.red
                                                  : Colors.red,
                                            ),
                                            SizedBox(height: 14.0),
                                            InkWell(
                                              child: Icon(
                                                Icons.arrow_forward_ios,
                                                size: 18.0,
                                                color: HexColor('#00A14B'),
                                              ),
                                              onTap: () {
                                                Navigator.push(
                                                  context,
                                                  MaterialPageRoute(
                                                      builder: (context) =>
                                                          Detail(product_id: subDetails_search[index]["pDetails"]
                                                  ["Id"] ,)),
                                                );
                                              },
                                            ),
                                          ]),
                                          onTap: () {
                                            setState(() {
                                              if (isSaved) {
                                                savedWords.remove(word);
                                              } else {
                                                savedWords.add(word);
                                              }
                                            });
                                          },
                                        )));
                              },
                            ));
  }

  Widget applybtn() {
    return Container(
      width: 20,
      child: ElevatedButton(
          child: Padding(
            padding: EdgeInsets.only(
                right: 20.0, left: 20.0, top: 15.0, bottom: 15.0),
            child: Text("Apply",
                style: GoogleFonts.montserrat(
                    fontSize: 20, fontWeight: FontWeight.w600)),
          ),
          style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              backgroundColor:
                  MaterialStateProperty.all<Color>(HexColor("#0071BC")),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                      side: BorderSide(color: HexColor("#0071BC"))))),
          onPressed: () { 
            Filter filter = new Filter.novalue();
            filter.Breed = get_string_breed() ;
            filter.Category  = get_string_category();
            filter.sort ="0";
            filter.PriceRange ="First Range,Second Range";
            API.Api.get_Filter(filter,session_id).then((value) {
                 setState(() {
                 var data = json.decode(value.body);
                  var res = data as List;
                  print(res[0]["TotalQuantity"]);
                  for (int i = 0; i < res.length; i++) {
                    total = total + res[i]["TotalQuantity"];
                  
                    productDetails_search.add(res[i]["prdDetails"]);

                    for(int j=0;j<productDetails_search.length;j++)
                    {
                      for(int k=0;k<productDetails_search[j].length;k++)
                      {
                        subDetails_search.add(productDetails_search[j][k]);
                      }
                    }
                    productDetails_search.clear();
                  }
               
                    receive = 2;
                  });
                    Navigator.pop(context);
            }); }),); }

  _onBasicAlertPressed(context) {
    Alert(
      context: context,
      title: "Are you sure you want to logout?",
      style: AlertStyle(
          titleStyle: GoogleFonts.montserrat(
              color: HexColor('#4A4B4D'),
              fontSize: 18,
              fontWeight: FontWeight.w700),
          descStyle: GoogleFonts.montserrat(
              color: HexColor('#4A4B4D'),
              fontSize: 17,
              fontWeight: FontWeight.w400)),
      desc: "We can't notify you of new stocks if you do.",
      buttons: [
        DialogButton(
            child: Text(
              "Cancel",
              style: GoogleFonts.montserrat(
                  color: HexColor('#4A4B4D'),
                  fontSize: 18,
                  fontWeight: FontWeight.w700),
            ),
            onPressed: () => Navigator.pop(context),
            color: Colors.white,
            border: Border.fromBorderSide(BorderSide(
                color: Colors.black, width: 0.8, style: BorderStyle.solid)),
            radius: const BorderRadius.all(Radius.circular(50))),
        DialogButton(
            child: Text(
              "Logout",
              style: GoogleFonts.montserrat(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.w700),
            ),
            onPressed: () =>{
            if(get_login_method() == 1)
            {
               signOutGoogle(),
               session_id = null,
            }
            else if(get_login_method() == 2)
            {

            },
           
            Navigator.push(context, MaterialPageRoute(builder: (context) => Account_select()),),
            },
            color: HexColor('#00A14B'),
            border: Border.fromBorderSide(BorderSide(
                color: HexColor('#00A14B'),
                width: 2,
                style: BorderStyle.solid)),
            radius: const BorderRadius.all(Radius.circular(50))),
      ],
    ).show();
  }
  String get_string_breed()
{
  String breeds="";
  for(int i=0;i<Breed.length-1;i++)
  {
      breeds = breeds + Breed[i] +',';
  }
    breeds = breeds + Breed[Breed.length-1];
    print(breeds);
    return(breeds);
}
  String get_string_category()
{
  String categorys="";
  for(int i=0;i<Category.length-1;i++)
  {
      categorys = categorys + Category[i] +',';
  }
    categorys = categorys + Category[Category.length-1];
    return (categorys);
}
}
