import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:flutter/services.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:english_words/english_words.dart';
import 'package:investment_portal_window/checkout.dart';
import 'package:investment_portal_window/terms.dart';
import './calculator.dart';
import './api.dart';
import 'dart:convert';
import './login.dart';
import 'account_menu.dart';


class Detail extends StatefulWidget{
  final String product_id;
  Detail({this.product_id});
  _DetailState createState() => _DetailState(product_id: this.product_id);
}
class _DetailState extends State<Detail>{
  String product_id;
  _DetailState({this.product_id});
  
  List<String> words = nouns.take(40).toList();
  List<String> savedWords = [];
  List <int> exampleList =  [1,2,3,4];
  List<dynamic> productDetails = List<dynamic>();
  List<dynamic> subDetails = List<dynamic>();
  List<Map<dynamic, dynamic>> lists = [];
  String breed;
  String master;
  String category;
  var image;
  bool receive = false;
  int quantity_count=0;
  
    void initState() {
    API.Api.getDetail(session_id).then((value) {
       setState(() {
      var data = json.decode(value.body);
      var res = data as List;
     // print(res[0]["TotalQuantity"]);
      for (int i = 0; i < res.length; i++) {
        productDetails.add(res[i]["prdDetails"]);
        // print(res[i]["pMaster"]);
        for(int j=0;j<productDetails.length;j++)
        {
          for(int k=0;k< productDetails[j].length;k++)
          {
            
            subDetails.add(productDetails[j][k]);
          }
        }
        productDetails.clear();
      }
      for(int i=0;i<subDetails.length;i++)
      {
        if(subDetails[i]['pDetails']['Id'] == product_id)
        {     List<dynamic> temp = List<dynamic>();
              temp.add(subDetails[i]);
              subDetails.clear();
              subDetails.add(temp[0]);
              print(subDetails[0]);
        }
      }
      master = subDetails[0]['pDetails']['Product_Master__c'];
      //image = base64Decode(subDetails[0]['blobImageString']);
      for(int i=0;i<res.length;i++)
      {
        if(res[i]['pMaster']['Id']== master)
        {
          breed = res[i]['pMaster']['Breed__c'];
          category = res[i]['pMaster']['Product_Category__c'];
        }
      }
       
        receive = true;
      });
    });
    super.initState();
  }
  Widget build(BuildContext context)
  {
    return receive?Scaffold(
        appBar: AppBar(
        toolbarHeight: 150,
  flexibleSpace: Container(
    decoration: 
      BoxDecoration(
        borderRadius: BorderRadius.only(bottomLeft:Radius.circular(30.0),bottomRight: Radius.circular(30.0)),
        image: DecorationImage(
          colorFilter: ColorFilter.mode(Colors.black.withOpacity(0.4),BlendMode.srcOver),
          image: NetworkImage('https://avatars0.githubusercontent.com/u/8264639?s=460&v=4'),

          fit: BoxFit.cover,
        ),
      ),
  ),
  backgroundColor: Colors.white,
  elevation: 0.0,
  title: Column(
    mainAxisAlignment: MainAxisAlignment.start,
    children: [ Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Spacer(),
                Padding(child:ClipOval(
                child: Container(
                  color: Colors.white,
                  child: Icon(        
                    Icons.favorite,
                    color: Colors.red,
                    size: 35.0,
                  ),
                  width: 50,
                  height: 50,
                ),
              ),padding:EdgeInsets.only(right:10.0))
              ]),
              Row(
              mainAxisAlignment: MainAxisAlignment.center,
              children: <Widget>[
                Spacer(),
                Padding(padding:EdgeInsets.only(right:40.0) ,child:Text(
                  category.toUpperCase(),
                  textAlign: TextAlign.center,
                  style: GoogleFonts.montserrat(
                          color: Colors.white,
                          fontSize: 25.0,
                          letterSpacing: 0.1 ,
                          fontWeight: FontWeight.w600,
                        ),
                ),),
                
             
                Spacer(),
              ]),
  ],),
  titleSpacing: 0.0,
    ),
    backgroundColor: Colors.white,
    body: AnnotatedRegion<SystemUiOverlayStyle>(
        
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
             
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 20.0,
                    vertical: 30.0,
                  ),
                  child: receive?Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: [
                      Align(
                        alignment: Alignment.center,child:
                          Text('Invest in a cow for just Rs 11,529 & earn returns of up to Rs 12,330.06.',style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          fontSize: 18.0,
                          letterSpacing: 0.1 ,
                          fontWeight: FontWeight.w400,
                        ),textAlign: TextAlign.center,),
                      ),
                      SizedBox(height:20.0),
                     breed_type('Breed', breed),
                    breed_type('Age', subDetails[0]['pDetails']['Age__c']),
                      breed_type('Price', (subDetails[0]['pDetails']['Price__c']).toString()),
                breed_type('Lifestage', subDetails[0]['pDetails']['Maturity_Date__c']),
                  breed_type('Birthdate', subDetails[0]['pDetails']['BirthDate__c']),
                   breed_type('Maturity date', subDetails[0]['pDetails']['Maturity_Date__c']),
                   breed_type('Expected ROI', (subDetails[0]['pDetails']['Expected_ROI__c']).toString()),
                    breed_type('Source of diet', subDetails[0]['pDetails']['Source_of_Diet__c']),
                      SizedBox(height:20.0),
                    Row(children: [
                      Spacer(),
                      Quantity(),
                      Spacer(),
                      _buildLoginBtn(),
                     
                     
                    ],),
                     SizedBox(height:20),
                     _investBTN(context,product_id,quantity_count),
    ]):Center(
                              child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(height: 40.0),
                                CircularProgressIndicator(),
                              ],
                            )),  ))])))):Scaffold(
                              backgroundColor: Colors.white,
                              body:Center(
                              child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(height: 40.0),
                                CircularProgressIndicator(),
                              ],
                            )));
   
  }
   Widget breed_type(String input1, String input2)
    {
   
     return  
    new Container(
  margin: const EdgeInsets.symmetric(vertical: 4.0, horizontal: 16.0),
  child: new InkWell(
    child: new Card(
      child: new SizedBox(
        height: 40.0,
        child: new Row(
        mainAxisSize: MainAxisSize.max,
        children: <Widget>[
          new Container(color: Colors.grey[100],
                alignment: Alignment.center,
                width:180,
                padding: const EdgeInsets.symmetric(horizontal: 30.0),
                child: Padding(
           padding: const EdgeInsets.only(top: 0.0, bottom: 0.0,left: 0.0,right: 0.0),
            child: new Text(input1,
               style:GoogleFonts.montserrat(
                    color: new HexColor('#7C7D7E'),
                    fontSize: 17.0,
                    fontWeight: FontWeight.w300)),
          ),),
        
        
         new Container(
                alignment: Alignment.center,
                padding: const EdgeInsets.symmetric(horizontal: 0.0),
                child: Padding(
            padding: const EdgeInsets.only(top: 0.0, bottom: 0.0,left: 30.0,right:0.0),
            child: new Text(input2,
               style:GoogleFonts.montserrat(
                    color: Colors.black,
                    fontSize: 16.0,
                    fontWeight: FontWeight.w300)),
          ),
            ),
        ],
      ),
      ),
    ),
  ),
);
    }
      
    Widget Quantity()
    {
      return Column(
        children: [
          Text('Add Quantity',style: GoogleFonts.montserrat(
                    color: new HexColor('#7C7D7E'),
                    fontSize: 20.0,
                    fontWeight: FontWeight.w400),),
          SizedBox(height:10.0),
          Row(
            children:[
              Padding(padding: EdgeInsets.only(
                left:0.0,right:5.0
              ),child:  CircleAvatar(
                radius: 15,
                backgroundColor: HexColor('#7C7D7E'),
                child: IconButton(
                  icon: Icon(
                    Icons.remove,
                    color: Colors.white,
                    size: 15.0,
                  ),
                  onPressed: () {
                    setState(() {
                      if(quantity_count > 0)
                      {
                        quantity_count --;
                      }
                    });
                  },
                ),
              ),
              ),
              Padding(padding: EdgeInsets.only(right: 15.0,left:15.0),
              child:  Text("$quantity_count",style: GoogleFonts.montserrat(
                    color: Colors.grey[800],
                    fontSize: 20.0,
                    fontWeight: FontWeight.w400),),
              ),
           
              Padding(padding: EdgeInsets.only(left:5.0,right:5.0),
              child:   CircleAvatar(
                radius: 15,
                backgroundColor: HexColor("#0071BC"),
                child: IconButton(
                  icon: Icon(
                    Icons.add,
                    color: Colors.white,
                    size: 15.0,
                  ),
                  onPressed: () {
                    setState(() {
                      quantity_count ++;
                    });
                  },
                ),
              ),
              )
        
             
            ]
          )
        ],
      );
    }
     Widget _buildLoginBtn() {
    return Container(
          width:200,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 0.0,left: 0.0,top: 15.0,bottom: 15.0),child: Text(
        "Calculate Investment",
        style: GoogleFonts.montserrat(fontSize: 15,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(HexColor('#7C7D7E')),
        shadowColor:  MaterialStateProperty.all<Color>(HexColor("#0071BC")),
        backgroundColor: MaterialStateProperty.all<Color>(Colors.white),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#0071BC"))
          )
        ),
         elevation:MaterialStateProperty.all(5.0),
      ),
     
        onPressed: () {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Calculator()),
  );
}
    ),
                  );
  }
}
Widget Loading()
{
  return Center(
                              child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(height: 40.0),
                                CircularProgressIndicator(),
                              ],
                            ));
}
  Widget _investBTN(context,String pd,int qc) {
    return Container(
          width:380,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 50.0,left: 50.0,top: 16.0,bottom: 16.0),child: Text(
        "Invest Now",
        style: GoogleFonts.montserrat(fontSize: 20,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
        onPressed: () {
           Navigator.push(context,MaterialPageRoute(builder: (context) => Checkout(result: 0,product_id: pd,count:qc ,)), );}), );}