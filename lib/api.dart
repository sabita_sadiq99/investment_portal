import 'package:http/http.dart' as http;
import 'package:investment_portal_window/passwordmodel.dart';
import 'package:investment_portal_window/uploadmodel.dart';
import 'package:investment_portal_window/userupdatemodel.dart';
import 'dart:async';
import 'dart:core';
import './usermodel.dart';
import 'dart:convert';
import './filtermodel.dart';
import 'favouritemodel.dart';
import 'ordermodel.dart';

class API {
  static API Api;
  Future<http.Response> APIuser() async {
    return http.post(
      Uri.parse(
          'https://login.salesforce.com/services/oauth2/token?grant_type=password&client_id=3MVG9sh10GGnD4DtyGw2iAfsD2EftBuRCE.z5.m.bY9GHVd0XdF6RoBtTxAcYOOd4Ow.teOXKjq_a.Yj610gI&client_secret=0FA4759AD3E0998A36E6A97141166567BE08953A0654A8090CF7055BB8E68E63&username=api.user@cloudvision.com&password=Ap!Us3R2021ngAh5jOSW0wdddDVQG4ZKuAPh&security token=2I5eW2ad9uT0R4CfpqbFNdzoa'),
      headers: <String, String>{
        'Content-Type': 'application/json; charset=UTF-8',
        'Access-Control-Allow-Headers': 'Access-Control-Allow-Origin, Accept',
      },
    );
  }
  Future<http.Response> createuser(User user, String id) async {
    return http.post(
      Uri.parse(
          'https://livestockapp-dev-ed.my.salesforce.com/services/apexrest/UserOperation/CreateNewUser'),
      headers: <String, String>{
        'Authorization': 'Bearer' + " " + id,
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: json.encode(user),
    );
  }
  Future<http.Response> check_user_status(String email, String id) async {
    print(id);
    return http.get(
      Uri.parse(
          'https://livestockapp-dev-ed.my.salesforce.com/services/apexrest/UserOperation/testportal@test.com.qa'),
      headers: <String, String>{
        'Authorization': 'Bearer' + " " + id,
        'Content-Type': 'application/json; charset=UTF-8',
      },
      
    );
  }

  Future<http.Response> getDetail(String id) async {
    return http.get(
      Uri.parse(
          'https://livestockapp-dev-ed.my.salesforce.com/services/apexrest/ProductDetails/getProductDetails'),
      headers: <String, String>{
        'Authorization': 'Bearer' + " " + id,
        'Content-Type': 'application/json; charset=UTF-8',
        'Access-Control-Allow-Headers': 'Access-Control-Allow-Origin, Accept'
      },
    );
  }
    Future<http.Response> get_Image(String id,String Url) async {
      print(Url);
    return http.get(
      Uri.parse(
          'https://livestockapp-dev-ed.my.salesforce.com' + Url),
      headers: <String, String>{
        'Authorization': 'Bearer' + " " + id,
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
  }
      Future<http.Response> get_Filter(Filter filt ,String id) async {
    return http.post(
      Uri.parse(
          'https://livestockapp-dev-ed.my.salesforce.com/services/apexrest/ProductDetails/getProductDetailsByKeyword'),
      headers: <String, String>{
        'Authorization': 'Bearer' + " " + id,
        'Content-Type': 'application/json; charset=UTF-8',
      },
        body: json.encode(filt),
    );
  }
        Future<http.Response> update_user(UserUpdate filt ,String id) async {
    return http.post(
      Uri.parse(
          'https://livestockapp-dev-ed.my.salesforce.com/services/apexrest/UpdateUsers/UpdateUserRestAPI'),
      headers: <String, String>{
        'Authorization': 'Bearer' + " " + id,
        'Content-Type': 'application/json; charset=UTF-8',
      },
        body: json.encode(filt),
    );
  }
        Future<http.Response> update_password(PassUpdate pass ,String id) async {
    return http.post(
      Uri.parse(
          'https://livestockapp-dev-ed.my.salesforce.com/services/apexrest/ChangeUserPwd/ChangePwdRestAPI'),
      headers: <String, String>{
        'Authorization': 'Bearer' + " " + id,
        'Content-Type': 'application/json; charset=UTF-8',
      },
        body: json.encode(pass),
    );
  }
  Future<http.Response> login_api(String id,String email,String password) async {
    print(email);
    print(password);
    return http.get(
      Uri.parse(
          'https://livestockapp-dev-ed.my.salesforce.com/services/apexrest/UpdateUsers?id=testportal@test.com.qa&pwd=RGNzaWFuJDk5'),
      headers: <String, String>{
        'Authorization': 'Bearer' + " " + id,
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
  }
          Future<http.Response> upload_image(UploadModal pass ,String id) async {
    return http.post(
      Uri.parse(
          'https://livestockapp-dev-ed.my.salesforce.com/services/apexrest/UploadImages/UploadImageRestAPI'),
      headers: <String, String>{
        'Authorization': 'Bearer' + " " + id,
        'Content-Type': 'application/json; charset=UTF-8',
      },
        body: json.encode(pass),
    );
  }
  Future<http.Response> submitfav(Favourite fav, String id) async {
    return http.post(
      Uri.parse(
          'https://livestockapp-dev-ed.my.salesforce.com/services/apexrest/FavouriteProducts/FavouriteProductsRestAPI'),
      headers: <String, String>{
        'Authorization': 'Bearer' + " " + id,
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: json.encode(fav),
    );
  }
    Future<http.Response> getFavs(String id) async {
    return http.get(
      Uri.parse(
          'https://livestockapp-dev-ed.my.salesforce.com/services/apexrest/FavouriteProducts/FavouriteProductsRestAPI'),
      headers: <String, String>{
        'Authorization': 'Bearer' + " " + id,
        'Content-Type': 'application/json; charset=UTF-8',
      },
    );
  }
    Future<http.Response> createorder(Order order, String id) async {
    return http.post(
      Uri.parse(
          'https://livestockapp-dev-ed.my.salesforce.com/services/apexrest/ProductOrders/ProductOrdersRestAPI'),
      headers: <String, String>{
        'Authorization': 'Bearer' + " " + id,
        'Content-Type': 'application/json; charset=UTF-8',
      },
      body: json.encode(order),
    );
  }
}
 
  


