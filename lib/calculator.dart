
import 'dart:ui';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import './setting.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import './welcome.dart';
import 'checkout.dart';
class Calculator extends StatefulWidget{
  _CalculatorState createState() => _CalculatorState();
}
class _CalculatorState extends State<Calculator>{
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  double _currentSliderValue = 20;
  double _currentSliderValue1 = 2;
  int buffalo_count = 20;
  int year_count = 2;
    void _onItemTapped(int index) {
    if (index == 0) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Welcome()),
      );
    }

    if (index == 4) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Setting()),
      );
    }
  }
 @override
  Widget build(BuildContext context) {
    return Scaffold(
       key: _scaffoldKey,
    drawer: Drawer(
        child: Container(
          color: HexColor('#1b2f35'),
          width: double.infinity,
          height: double.infinity,
          child: ListView(
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: HexColor('#1b2f35'),
              ),
              child: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft+ Alignment(0.2,0),
                    child: new Image.asset(
              'logo-white.png',
              width: 100.0,
              height: 100.0,
              fit: BoxFit.contain,
            ),
            
                  ),
                 
                  Align(
                    alignment: Alignment.bottomLeft + Alignment(0.2,1.5),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage('https://avatars0.githubusercontent.com/u/8264639?s=460&v=4'),
                      radius: 30.0,
                    ),
                  ),
                  Align(
                    alignment: Alignment(0.4, 1.5),
                    child: Text(
                      'Alec Reynolds',
                      style: TextStyle(color: Colors.white, fontSize: 20.0),
                    ),
                  ),
                  SizedBox(height: 2.0,),
                 Align(
                    alignment: Alignment(0.5, 1.8),
                    child: Text(
                      'johndoe@gmail.com',
                      style: TextStyle(color: Colors.white, fontSize: 15.0),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height:50.0),
             new Divider(
            color: Colors.white,
            thickness: 0.8,
          ),
          ListTile(
                leading: Icon(Icons.home,color: Colors.white,),
                title: Text('Home',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
               ListTile(
                leading: Icon(Icons.shopping_bag,color: Colors.white,),
                title: Text('My Orders',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
              ListTile(
                leading: Icon(Icons.account_balance_wallet,color: Colors.white,),
                title: Text('My Wallet',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
              ListTile(
                leading: Icon(Icons.settings,color: Colors.white,),
                title: Text('Account Settings',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: () {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Setting()),
  );
},
              ),
               ListTile(
                leading: Icon(Icons.power_settings_new_rounded,color: Colors.white,),
                title: Text('Sign Out',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
          ],
          
        ),
        ),
      ),
       bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag_outlined),
            label: 'My Orders',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
          ),
        ],
        iconSize: 30.0,
        selectedItemColor: HexColor('#7C7D7E'),
        unselectedItemColor: HexColor('#7C7D7E'),
        unselectedLabelStyle: TextStyle(color: Colors.black),
        onTap: _onItemTapped,
      ),
      backgroundColor: Colors.white,
            body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
             
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 20.0,
                    vertical: 60.0,
                  ),
                  child: Column(
                    
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      
                      Row(
                        children:[
                          Padding(padding: EdgeInsets.only(left:0.0),child:
                                 IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: HexColor('#0071BC'),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),),
         CircularProfileAvatar(
          'https://avatars0.githubusercontent.com/u/8264639?s=460&v=4', 
          radius: 15,
          backgroundColor: Colors.transparent, 
          elevation: 5.0, 
          cacheImage: true, 
          onTap: () => _scaffoldKey.currentState.openDrawer(),
          showInitialTextAbovePicture: true, 
          ),
          Spacer(),
         
          Column(
            children:[
                     Text(
                        'Investment',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                         
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                         Text(
                        'Calculator',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                         
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
            ]
          ),
                      Spacer(),
                      Spacer(),
                      Spacer(),]
                      ),
                      SizedBox(height:30.0),
                      Row(children: [
                 Align(
                         alignment: Alignment.topLeft,
                        child: Container(child:Padding(child: Text('Investment (What I put in)',style: GoogleFonts.montserrat(
                          color:Colors.white,fontSize: 16.0,)),padding: EdgeInsets.only(top:10.0,left:10.0,right: 15.0),),
                        height: 40,
                    
                        decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: HexColor('#1b2f35'),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.zero,
                        topRight: Radius.zero,
                        bottomLeft: Radius.zero,
                        bottomRight: Radius.circular(20.0),
                      ),
                    ),),),
                                      ],),
                      SizedBox(height:30.0),
                      Align(
                        alignment: Alignment.topLeft,child:
                          Text('How much amount would you like to invest in?',style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          fontSize: 14.0,
                          letterSpacing: 0.1 ,
                          fontWeight: FontWeight.w400,
                        ),textAlign: TextAlign.center,),
                      ),
                        Padding(
                padding: EdgeInsets.only(top:10.0,right:18.0,bottom: 15.0),
                child: Stack(
                  alignment: const Alignment(0, 0),
                  children: <Widget>[
                    Container(
                        decoration: BoxDecoration(
                          color: Colors.grey[100],
                          borderRadius: new BorderRadius.circular(10.0),
                        ),
                        child: Padding(
                            padding:
                                EdgeInsets.only(left: 70.0, right: 15, top: 0),
                            child: TextFormField(
                                obscureText: true,
                                decoration: InputDecoration(
                                  border: InputBorder.none,
                                 
                                )))),
                    Positioned(
                        left: 0,
                        child: Container(child:Padding(child: Text('PKR',style: GoogleFonts.montserrat(
                          color:Colors.white,)),padding: EdgeInsets.only(top:15.0,left:10.0,),),
                        height: 50,
                        width: 50,
                           decoration: new BoxDecoration (
          color: HexColor('#0071BC'),
           borderRadius: new BorderRadius.only(
             topLeft: Radius.circular(10.0),
             bottomLeft: Radius.circular(10.0)
           ),
        ),),)
                  ],
                ),
              ),
              SizedBox(height:10.0),
                      Align(
                        alignment: Alignment.topLeft,child:
                          Text('How many buffalo would you like to start with?',style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          fontSize: 14.0,
                          letterSpacing: 0.1 ,
                          fontWeight: FontWeight.w400,
                        ),textAlign: TextAlign.center,),
                      ),
                       SizedBox(height:20.0),
                       Align(
                         alignment: Alignment.topLeft,child:  new Text("$buffalo_count buffalos",
                    style:GoogleFonts.montserrat(
                    color: Colors.black,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w400),textAlign: TextAlign.left,), 
                       ),
                     SizedBox(height:10.0),
                      Slider(
                       value: _currentSliderValue,
                        min: 0,
                        max: 100,
                        divisions: 100,
                        activeColor: HexColor('#00A14B'),
                        inactiveColor: HexColor('#7C7D7E'),
                        label: _currentSliderValue.round().toString(),
                        onChanged: (double value) {
                          setState(() {
                            _currentSliderValue = value;
                            buffalo_count = _currentSliderValue.toInt();
                          });
                        },
                      ),
                       SizedBox(height:10.0),
                            Align(
                        alignment: Alignment.topLeft,child:
                          Text('How many buffalo would you like to start with?',style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          fontSize: 14.0,
                          letterSpacing: 0.1 ,
                          fontWeight: FontWeight.w400,
                        ),textAlign: TextAlign.center,),
                      ),
                         SizedBox(height:20.0),
                       Align(
                         alignment: Alignment.topLeft,child:  new Text("$year_count years",
                    style:GoogleFonts.montserrat(
                    color: Colors.black,
                    fontSize: 18.0,
                    fontWeight: FontWeight.w400),textAlign: TextAlign.left,), 
                       ),
                          SizedBox(height:10.0),
                          Slider(
                       value: _currentSliderValue1,
                        min: 0,
                        max: 100,
                        divisions: 100,
                        activeColor: HexColor('#00A14B'),
                        inactiveColor: HexColor('#7C7D7E'),
                        label: _currentSliderValue1.round().toString(),
                        onChanged: (double value) {
                          setState(() {
                            _currentSliderValue1 = value;
                            year_count = _currentSliderValue1.toInt();
                          });
                        },
                      ),
                          Align(
                         alignment: Alignment.topLeft,
                        child: Container(child:Padding(child: Text('Return (What I get back)',style: GoogleFonts.montserrat(
                          color:Colors.white,fontSize: 16.0,)),padding: EdgeInsets.only(top:10.0,left:10.0,right: 15.0),),
                        height: 40,
                    
                        decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: HexColor('#1b2f35'),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.zero,
                        topRight: Radius.zero,
                        bottomLeft: Radius.zero,
                        bottomRight: Radius.circular(20.0),
                      ),
                    ),),),
                    SizedBox(height: 20.0,),
                    message(context),
                   ])))]))),
    );
  }
}
 Widget message(context)
  {
    return Container(
      width:400,
      child:Column(children: [

        Row(children: [
          Padding(padding: EdgeInsets.only(left:10.0,bottom: 10.0,top:10.0),child:    Text('Total Investment',style: GoogleFonts.montserrat(fontSize: 14,fontWeight: FontWeight.w400,color: Colors.black )),),
       
          Spacer(),
          Text('11,23500',style: GoogleFonts.montserrat(fontSize: 14,fontWeight: FontWeight.w400,color: Colors.black)),
          Spacer(),
          Spacer(),
        ],),
          Row(children: [
          Padding(padding: EdgeInsets.only(left:10.0,bottom: 10.0),child:    Text('Total Years',style: GoogleFonts.montserrat(fontSize: 14,fontWeight: FontWeight.w400,color: Colors.black )),),
       
          Spacer(),
          Text('           '),
          Text('12 Years',style: GoogleFonts.montserrat(fontSize: 14,fontWeight: FontWeight.w400,color: Colors.black)),
          Spacer(),
          Spacer(),
        ],),
          Row(children: [
          Padding(padding: EdgeInsets.only(left:10.0,bottom: 10.0),child:    Text('Profit %',style: GoogleFonts.montserrat(fontSize: 14,fontWeight: FontWeight.w400,color: Colors.black )),),
       
          Spacer(),
          Text('              '),
          Text('8.5%',style: GoogleFonts.montserrat(fontSize: 14,fontWeight: FontWeight.w400,color: Colors.black)),
          Spacer(),
          Spacer(),
        ],),
        SizedBox(height: 20.0,),
        Row(children: [
          Spacer(),
          Align(
                         alignment: Alignment.topRight,
                        child: Container(child:Padding(child: Text('Expected ROI            11,24320',style: GoogleFonts.montserrat(
                          color:Colors.white,fontSize: 16.0,)),padding: EdgeInsets.only(top:10.0,left:10.0,right: 15.0),),
                        height: 40,
                        decoration: BoxDecoration(
                      shape: BoxShape.rectangle,
                      color: HexColor('#0071BC'),
                      borderRadius: BorderRadius.only(
                        topLeft: Radius.circular(20.0),
                        topRight: Radius.zero,
                        bottomLeft: Radius.zero,
                        bottomRight: Radius.zero,
                      ),
                    ),),),
        ],),
        SizedBox(height: 30.0,),
        _investBTN(context),
      ],),
     decoration: BoxDecoration(
       border:Border.all(
       color:Colors.white,
     ),
     borderRadius: BorderRadius.circular(8.0),
        color: Colors.grey[100],),);}
      Widget _investBTN(context) {
    return Container(
          width:380,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 50.0,left: 50.0,top: 16.0,bottom: 16.0),child: Text(
        "Invest Now",
        style: GoogleFonts.montserrat(fontSize: 20,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
        onPressed: () {
           Navigator.push(context,MaterialPageRoute(builder: (context) => Checkout(result: 0,)), );}), );}
           