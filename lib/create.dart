import 'dart:convert';
import 'dart:core';
import 'dart:io';
import 'package:flutter/material.dart';
import 'package:investment_portal_window/sign_in.dart';
import 'package:investment_portal_window/welcome.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import './api.dart';
import 'dart:math';
import './usermodel.dart';
import './login.dart';
import 'dart:ui';
import 'package:flutter/gestures.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:validators/validators.dart';
import 'package:sign_button/sign_button.dart';
import 'package:intl_phone_number_input/intl_phone_number_input.dart';
import 'package:image_picker/image_picker.dart';
import 'package:random_string/random_string.dart';
class CreateAccount extends StatefulWidget{
  _CreateAccountState createState() => _CreateAccountState();
}
class _CreateAccountState extends State<CreateAccount>
{
   final _formKey = GlobalKey<FormState>();
    Future<FormDependency> _dependencies;
    Model _model;
    List<Country_State> _stateList;
    Country_State _currentstate;
    List<Country_State> _currentstateList;
    List<City> _cityList;
    City _currentcity;
    File _image;
    List<City> _currentcityList;
    final _username = TextEditingController();
    final _fname = TextEditingController();
    final _lname = TextEditingController();
    String _pcity = "";
    String _pstreet = "";
    String _pcountry = "";
    String _pstate = "";
    final _postal =TextEditingController();
    final password = TextEditingController();
    final confirm = TextEditingController();
    final _email = TextEditingController();
    final _address = TextEditingController();
    final _cnic = TextEditingController();
    String _phone;
    String session_id ;
    bool _passwordVisible = false;
    var items = ['item1', 'item2', 'item3', 'item4'];
    String formattedDate="";
    final TextEditingController controller = TextEditingController();
    String initialCountry = 'PK';
    PhoneNumber number = PhoneNumber(isoCode: 'PK');
  void initState() {
    API.Api.APIuser().then((value) {
  var data = json.decode(value.body);
 setState(() {
   session_id =  data['access_token'];
   print(session_id);
 });
});
    super.initState();
    _model = Model(country:null,state: null, city: null);
    _stateList = null;
    _currentstateList = null;
    _currentstate = null;
    _passwordVisible = false;
     _dependencies = _getFormDependency();
  }
    Future<FormDependency> _getFormDependency() async {
    var pakistan = Country(id:1, name: 'Pakistan');
    var uk = Country (id:2,name:'UK');
    var azad_jammu = Country_State(id: 1, name: 'Azad Jammu and Kashmir',country: pakistan);
    var balochistan = Country_State(id: 2, name: 'Balochistan',country: uk);
    var gilgit = Country_State(id: 3, name: 'Gilgit-Baltistan',country: pakistan);
    var khyber = Country_State(id: 4, name: 'Khyber Pakhtunkhwa',country: pakistan);
    var sindh = Country_State(id: 5, name: 'Sindh',country: pakistan);
    var punjab = Country_State(id: 6, name: 'Punjab',country: pakistan);
    var eng = Country_State(id: 7, name: 'England',country: uk);
    var statelist = [azad_jammu, balochistan, gilgit,khyber,sindh,punjab,eng];
    var countryList =[pakistan,uk];
    var citiesList =[
      City(id: 1,name: 'Karachi',state: sindh),
      City(id:2, name:'Islamabad',state:punjab),
      City(id:3, name:'Chilas',state:gilgit),
      City(id:4, name:'Quetta',state:balochistan),
      City(id:5, name:'Muzaffarabad',state:azad_jammu),
      City(id:6, name:'Peshawar',state:khyber),

    ];
    _stateList = statelist;
    _currentstate = statelist[0];
    _currentstateList = _stateList.where((element) => element.country.id == countryList[0].id).toList();
    _cityList = citiesList;
    _currentcity = citiesList[0];
    _pcity = citiesList[0].name;
    _pstate = statelist[0].name;
    _pcountry = countryList[0].name;
    _currentcityList = _cityList.where((element) => element.state.id == statelist[0].id).toList();
    return FormDependency(countries :countryList,categories: statelist,cities: citiesList);
  }
  Widget build(BuildContext context) {
    
    return Scaffold(body: FutureBuilder<FormDependency>(
      future: _dependencies,
      builder: (context,snapshot){
        if(snapshot.connectionState == ConnectionState.done){
          return Container(
      height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal:20.0,
                    vertical:80.0,
                  ),
           child: Form(
        key: _formKey,
        child: Column(
          mainAxisAlignment: MainAxisAlignment.center,
          children: [
             Text(
                        'Sign Up',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                       SizedBox(height:12.0),
                      Text('Add your details to sign up',style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          fontSize: 18.0,
                          letterSpacing: 0.2 ,
                          fontWeight: FontWeight.w400,
                        ),textAlign: TextAlign.center,),
             SizedBox(height:10.0),
                      ImageProfile(),
          SizedBox(height : 16),
          FnameInput(),
          SizedBox(height : 7),
          LnameInput(),
          SizedBox(height : 7),
          EmailInput(),
          SizedBox(height : 7),
          CNIC(),
          SizedBox(height : 7),
          PhoneInput(),
          SizedBox(height : 7),
              _buildForm(context, snapshot.data),
              SizedBox(height : 7),
              _buildForm1(context,snapshot.data),
             SizedBox(height : 7),
             Row(
            children: [
              Flexible(child:_buildForm2(context, snapshot.data),),
              Flexible(child:AddressInput(),),
            ],
          ),
           SizedBox(height : 7),
           PasswordInput(),
           SizedBox(height : 7),
           ConfirmPassInput(),
            SizedBox(height : 10),
          _buildLoginBtn(),
           SizedBox(height:10.0),
                          Row(
                            children:[
                              Spacer(),
                              RichText(text: TextSpan(text: "Already have an Account?",style: TextStyle(fontSize:18.0, color: HexColor('#7C7D7E')),
                              children: <TextSpan>[
                                TextSpan(
                                  text:' Login',
                                   recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      Navigator.of(context).push( MaterialPageRoute(builder: (context,) {return LoginScreen(); },),);
                    },style: TextStyle(fontSize:18.0,color:HexColor('#0071BC')))])),
                          Spacer(),
                            ]),
                               SizedBox(height:20.0),
                               Text("or sign up with", style: TextStyle(fontSize:18.0, color: HexColor('#7C7D7E'),fontWeight: FontWeight.bold) ,),
                               SizedBox(height:20.0),
                               Row(children: [
                                 Spacer(),
                                   SignInButton.mini(
                            buttonType: ButtonType.google,
                              buttonSize: ButtonSize.large,
                            onPressed: () {},
                            ),
                              SignInButton.mini(
                            buttonType: ButtonType.facebook,
                              buttonSize: ButtonSize.large,
                            onPressed: () {},
                            ),
                              SignInButton.mini(
                            buttonType: ButtonType.linkedin,
                            buttonSize: ButtonSize.large,
                            onPressed: () {},
                            ),
                            Spacer(),
                               ],)],),)));}
        else
        {
          return Center(child: CircularProgressIndicator());
        }
      },));
       
      }
  Widget PasswordInput()
  {
    return  TextFormField(
   keyboardType: TextInputType.text,
   controller: password,
   obscureText:  !_passwordVisible,//This will obscure text dynamically
   decoration: InputDecoration(
    hintText: 'Password', contentPadding: const EdgeInsets.only(left:20.0,right:25.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
     errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
       // Here is key idea
       suffixIcon: IconButton(
            icon: Icon(
              // Based on passwordVisible state choose the icon
               _passwordVisible
               ? Icons.visibility
               : Icons.visibility_off,
               color: Theme.of(context).primaryColorDark,
               ),
            onPressed: () {
               // Update the state i.e. toogle the state of passwordVisible variable
               setState(() {
                   _passwordVisible = !_passwordVisible;
               });
             },
            ),
          ),
             validator: (value) {
        if (value.length == 0)
          return "Please fill this field";
        else
          return null;
      },
        );
  }
    Widget ConfirmPassInput()
  {
    return  TextFormField(
   keyboardType: TextInputType.text,
   controller: confirm,
   obscureText: !_passwordVisible,//This will obscure text dynamically
   decoration: InputDecoration(
    hintText: 'Confirm Password', contentPadding: const EdgeInsets.only(left:20.0,right:25.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
      errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
       suffixIcon: IconButton(
            icon: Icon(
               _passwordVisible
               ? Icons.visibility
               : Icons.visibility_off,
               color: Theme.of(context).primaryColorDark,
               ),
            onPressed: () {
               setState(() {
                   _passwordVisible = !_passwordVisible;
               });
             },
            ),
          ),
             validator: (value) {
        if (value != password.text)
          return "Passwords doesn't match";
        else if (value == "") {
          return 'Please fill this field';
        } 
        else
          return null;
      },
        );
  }
     Widget FnameInput() {
    return TextFormField(
      controller: _fname,
    	decoration: InputDecoration(
    hintText: 'First Name', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	),
      keyboardType: TextInputType.text,
      validator: (_fname) {
        if (_fname.length < 3 && _fname.length >0)
          return 'Name must be more than 2 charater';
        else if (_fname == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_fname))
        {
          return 'Invalid ';
        }
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
   Widget LnameInput() {
    return TextFormField(
      controller: _lname,
    	decoration: InputDecoration(
    hintText: 'Last Name', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	),
      keyboardType: TextInputType.text,
      validator: (_lname) {
        if (_lname.length < 3 && _lname.length >0)
          return 'Name must be more than 2 charater';
        else if (_lname == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_lname))
        {
          return 'Invalid';
        }
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
     Widget EmailInput() {
    return TextFormField(
      controller: _email,
    	decoration: InputDecoration(
    hintText: 'Email', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	),
      keyboardType: TextInputType.text,
      validator: (_email) {
        if (!(_email.contains('@')) && _email.length >0)
          return 'Invalid';
        else if (_email == "") {
          return 'Please fill this field';
        } 
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
   Widget AddressInput() {
    return TextFormField(
      controller: _address,
    	decoration: InputDecoration(
    hintText: 'Street', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
       errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	),
      keyboardType: TextInputType.text,
      validator: (_address) {
      if (_address == "") {
          return 'Please fill this field';
        } 
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
   Widget CNIC() {
    return TextFormField(
      controller: _cnic,
    	decoration: InputDecoration(
    hintText: 'CNIC', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),),
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  ),

      keyboardType: TextInputType.text,
      validator: (_cnic) {
        if (_cnic.length < 15 && _cnic.length >0)
          return 'Invalid Cnic';
        else if (_cnic == "") {
          return 'Please fill this field';
        } 
        else if(isAlphanumeric(_cnic))
        {
          return 'Invalid Cnic';
        }
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
    Widget PostalInput() {
    return TextFormField(
      controller: _postal,
       keyboardType:
                  TextInputType.numberWithOptions(signed: true, decimal: true),
    	decoration: InputDecoration(
    hintText: 'Postal Code', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
    
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	),
    
      validator: (_postal) {
        if (_postal.length < 3 && _postal.length > 0)
          return 'Invalid';
        else if (_postal == "") {
          return 'Please fill this field';
        } 
        else if(!isNumeric(_postal))
        {
          return 'Invalid';
        }
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
  Widget PhoneInput()
  {
      return InternationalPhoneNumberInput(
              onInputChanged: (PhoneNumber number) {
                //print(number.phoneNumber);
              },
              onInputValidated: (bool value) {
                if(value == true)
                {
                   print(value);
                   
                }
               
              },
              selectorConfig: SelectorConfig(
                selectorType: PhoneInputSelectorType.BOTTOM_SHEET,
              ),
              inputDecoration: InputDecoration(
    hintText: 'Phone', contentPadding: const EdgeInsets.only(left:30.0,right:15.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	),
              ignoreBlank: false,
              autoValidateMode: AutovalidateMode.disabled,
              selectorTextStyle: TextStyle(color: Colors.black),
              initialValue: number,
              formatInput: false,
              keyboardType:
                  TextInputType.numberWithOptions(signed: true, decimal: true),
              inputBorder: OutlineInputBorder(),
              onSaved: (PhoneNumber number) {
                print('On Saved: $number');
                _phone = number.toString();
              },
            );
  }
    
  Widget _buildLoginBtn() {
    FocusManager.instance.primaryFocus.unfocus();
    return Container(
          width:400,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 100.0,left: 90.0,top: 15.0,bottom: 15.0),child: Text(
        "Sign Up",
        style: GoogleFonts.montserrat(fontSize: 20,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
      onPressed:(){
          if (_formKey.currentState.validate()) {
             _formKey.currentState.save();
             User user = new User.novalue();
              user.username = _email.text;
              user.firstname = _fname.text;
              user.lastname = _lname.text;
              user.email = _email.text;
              user.street = _address.text;
              user.short_name = _fname.text + randomString(1);
              user.timezone = "Asia/Karachi";
              user.localeSid ="en_US";
              user.emailencoding = "ISO-8859-1";
              user.language="en_US";
              user.city = _pcity;
              user.country = _pcountry;
              user.state = _pstate;
              user.cnic = _cnic.text;
              user.phone = _phone;
              user.password = base64.encode(utf8.encode(password.text));
              user.file = "CNIC.pdf";
              user.blob = "aGVsbG8gd29ybGQ=";
               API.Api.check_user_status(_email.text, session_id).then((value) {
                  if(value.body == "")
                  {
                    API.Api.createuser(user,session_id).then((value) {
                if(value.statusCode == 200)
                {
                   Navigator.push( context, MaterialPageRoute(builder: (context) => Welcome()), );
                }
              });
                  }
                  else
                  {
                    _onUserSuccessfullyCreated(context);
                  }
              });
            
      }
      }),);}
      String getrandom()
      {
        var rndnumber="";
  var rnd= new Random();
  for (var i = 0; i < 3; i++) {
  rndnumber = rndnumber + rnd.nextInt(9).toString();}
    return rndnumber; }
    Widget _buildForm(BuildContext context, FormDependency data) {
   // List<Widget> form = [];
return CustomDropDownInput<Country>(
      labelText: 'country',
      optional: false,
      enabled: true,
      initialValue: data.countries[0],
      itemList: data.countries.map((Country country) {
        return DropdownMenuItem<Country>(
          value: country,
          child: Text(country.name),
        );
      }).toList(),
      onChanged: (newValue) {
        print(newValue.id);
        _model.country = newValue;

        setState(() {
          _pcountry = newValue.name;
          _currentstate =
              null; //null is passed to the dropdown build, but the internal valu of dropdown does not set the null value
          _currentstateList = _stateList
              .where((x) => x.country.id == newValue.id)
              .toList();
              print(_currentstateList);

        });
      },
    );}
 Widget _buildForm1(BuildContext context, FormDependency data) {
   // List<Widget> form = [];
return CustomDropDownInput<Country_State>(
      labelText: 'State',
      optional: false,
      enabled: true,
      initialValue: _currentstateList[0],
      itemList: (_currentstateList != null)
          ? _currentstateList.map((Country_State cat) {
              return DropdownMenuItem<Country_State>(
                value: cat,
                child: Text(cat.name),
              );
            }).toList()
          : null,
      onChanged: (newValue) {
        _model.state = newValue;
        setState(() {
          _pstate = newValue.name;
          _currentcity =
              null; //null is passed to the dropdown build, but the internal valu of dropdown does not set the null value
          _currentcityList = _cityList
              .where((x) => x.state.id == newValue.id)
              .toList();
            // print(_currentcityList);
        });
      },
    );
 }
 Widget _buildForm2(BuildContext context, FormDependency data) {
   // List<Widget> form = [];
return CustomDropDownInput<City>(
      labelText: 'City',
      optional: false,
      enabled: true,
      initialValue: _currentcityList[0],
      itemList: (_currentcityList != null)
          ? _currentcityList.map((City cat) {
              return DropdownMenuItem<City>(
                value: cat,
                child: Text(cat.name),
              );
            }).toList()
          : null,
      onChanged: (newValue) {
       _model.city = newValue;
        setState(() {
          _pcity = newValue.name;
        _currentcity = newValue;
        print(_currentcity);
        });
      },
    );
 }
 void _showPicker(context) {
  showModalBottomSheet(
      context: context,
      builder: (BuildContext bc) {
        return SafeArea(
          child: Container(
            child: new Wrap(
              children: <Widget>[
                new ListTile(
                    leading: new Icon(Icons.photo_library),
                    title: new Text('Photo Library'),
                    onTap: () {
                      _imgFromGallery();
                      Navigator.of(context).pop();
                    }), ],),),);});
}
_imgFromGallery() async {
       var image = await ImagePicker().getImage(
      source: ImageSource.gallery, imageQuality: 50
  );
  if(image != null)
  {
 final bytes = File(image.path).readAsBytesSync();
   String img64 = base64Encode(bytes);
  print(img64);
  }
}
  Widget ImageProfile()
  {
    return Center(
          child: GestureDetector(
            onTap: () {
              _showPicker(context);
            },
            child: CircleAvatar(
              radius: 55,
              backgroundColor: Colors.grey[200],
              child: _image != null
                  ? ClipRRect(
                      borderRadius: BorderRadius.circular(50),
                      child: Image.file(
                        _image,
                        width: 100,
                        height: 100,
                        fit: BoxFit.fitHeight,
                      ),
                    )
                  : Container(
                      decoration: BoxDecoration(
                          color: Colors.grey[200],
                          borderRadius: BorderRadius.circular(50)),
                      width: 100,
                      height: 100,
                      child: Icon(
                        Icons.camera_alt,
                        color: Colors.grey[800],
                      ),),),),); }
  }
class Country {
  final id;
  final name;
   Country({@required this.id, @required this.name});
}
class Country_State {
  final id;
  final name;
  final Country country;
  Country_State({@required this.id, @required this.name,@required this.country});
}
class City {
  final id;
  final name;
  final Country_State state;

  City({@required this.id, @required this.name, @required this.state});
}
class FormDependency {
  final List<Country> countries;
  final List<Country_State> categories;
 final List<City> cities;

  FormDependency({@required this.countries,@required this.categories,@required this.cities});
}

class Model {
  Country country;
  Country_State state;
  City city;

  Model({@required this.country,@required this.state, @required this.city});
}
class CustomDropDownInput<T> extends StatelessWidget {
  const CustomDropDownInput({
    Key key,
    @required this.labelText,
    @required this.itemList,
    @required this.onChanged,
    this.initialValue,
    this.show,
    this.enabled,
    this.optional,
  }) : super(key: key);

  final String labelText;
  final List<DropdownMenuItem<T>> itemList;
  final ValueChanged<T> onChanged;
  final T initialValue;
  final bool show;
  final bool enabled;
  final bool optional;

  String _validateOptional(T value) {
    if (value == null) return "Campo obrigatório";

    return null;
  }

  void _handleOnChanged(T newValue) {
    if (newValue != null) {
      // Call onChanged callback if registered
      if (onChanged != null) onChanged(newValue);
    }
  }

  @override
  Widget build(BuildContext context) {
    return (show == null || show == true)
        ? Column(
            children: <Widget>[
              DropdownButtonFormField(
                icon: Icon(Icons.arrow_drop_down),
                isDense: true,
                decoration: InputDecoration(
                  hintText: labelText,
                  hintStyle:  TextStyle(color: Colors.grey),
                  filled: true,
                  fillColor: Colors.grey[80],
                   enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
                ),
                validator: (optional != null && optional == false)
                    ? _validateOptional
                    : null,
                disabledHint: (enabled != null &&
                        enabled == false &&
                        itemList != null &&
                        initialValue != null)
                    ? itemList.firstWhere((x) => x.value == initialValue).child
                    : null,
                items: (enabled != null && enabled == false) ? null : itemList,
                onChanged: _handleOnChanged,
                value: initialValue,
              ), ],)
        : Container();
  }
}
 _onUserSuccessfullyCreated(context){
 Alert(
      context: context,
      title: "Duplicate User",
      type: AlertType.info,
      style: AlertStyle(
        isCloseButton: false,
        isOverlayTapDismiss: false,
          titleStyle: GoogleFonts.montserrat(
              color: HexColor('#4A4B4D'),
              fontSize: 18,
              fontWeight: FontWeight.w700),
          descStyle: GoogleFonts.montserrat(
              color: HexColor('#4A4B4D'),
              fontSize: 17,
              fontWeight: FontWeight.w400)),
      desc: "The user you are trying to create already exists.",
      buttons: [
                DialogButton(
            child: Text(
              "Cancel",
              style: GoogleFonts.montserrat(
                  color: HexColor('#4A4B4D'),
                  fontSize: 18,
                  fontWeight: FontWeight.w700),
            ),
            onPressed: () => Navigator.pop(context),
            color: Colors.white,
            border: Border.fromBorderSide(BorderSide(
                color: Colors.black, width: 0.8, style: BorderStyle.solid)),
            radius: const BorderRadius.all(Radius.circular(50))),
        DialogButton(
            child: Text(
              "Sign In",
              style: GoogleFonts.montserrat(
                  color: Colors.white,
                  fontSize: 18,
                  fontWeight: FontWeight.w700),
            ),
            onPressed: () => Navigator.pop(context),
            color: HexColor('#00A14B'),
            border: Border.fromBorderSide(BorderSide(
                color: HexColor('#00A14B'),
                width: 2,
                style: BorderStyle.solid)),
            radius: const BorderRadius.all(Radius.circular(50))),
      ],
    ).show();
  }
