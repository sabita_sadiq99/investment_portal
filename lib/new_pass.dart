import 'dart:convert';
import 'dart:ui';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:investment_portal_window/forgot_password.dart';
import 'package:investment_portal_window/login.dart';
import 'package:investment_portal_window/passwordmodel.dart';
import 'package:investment_portal_window/try2.dart';
import 'package:validators/validators.dart';

import 'account_menu.dart';
import 'api.dart';
class NewPass extends StatefulWidget{
  _NewPassstate createState() => _NewPassstate();
}
class _NewPassstate extends State<NewPass>{
final _new = TextEditingController();
final _confirm = TextEditingController();
final _formKey = GlobalKey<FormState>();
bool _passwordVisible = false;
bool _passwordVisible1 = false;
void initState(){
  super.initState();
  _passwordVisible = false;
  _passwordVisible1 = false;
}
Widget _buildNewPassTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 10.0),
        Container(
  alignment: Alignment.centerLeft,
  height: 80.0,
  child: new TextFormField(
  obscureText:  !_passwordVisible,
  controller: _new,
	autocorrect: true,
	decoration: InputDecoration(
    hintText: 'New Password', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
       suffixIcon: IconButton(
            icon: Icon(
               _passwordVisible
               ? Icons.visibility
               : Icons.visibility_off,
               color: Theme.of(context).primaryColorDark,
               ),
            onPressed: () {
               setState(() {
                   _passwordVisible = !_passwordVisible;
               });
             },
            ),
    ),
     validator: (_new){
          if(_new.length <8 || isAlpha(_new) || isNumeric(_new))
          {
              return "Invalid Password";
          }
          else
          {
            return null;
          }
      },
    ), ),],); }
  Widget _buildConfirmPassTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        Container(
          alignment: Alignment.centerLeft,
          height: 80.0,
          child: new TextFormField(
          obscureText:  !_passwordVisible1,
          controller: _confirm, 
          autocorrect: true,
          decoration: InputDecoration(
    hintText: 'Confirm Password', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
     suffixIcon: IconButton(
            icon: Icon(
               _passwordVisible
               ? Icons.visibility
               : Icons.visibility_off,
               color: Theme.of(context).primaryColorDark,
               ),
            onPressed: () {
               setState(() {
                   _passwordVisible1 = !_passwordVisible1;
               });
             },
            ),
	),
        validator: (_confirm){
          if(_confirm.length ==0)
          {return "Please re-enter password";}    
          else if(_confirm != _new.text)
          { return "Passwords doesnot match"; }
          else
          {return null; }
      },
  ),
        ),
      ],
    );
  }
    Widget _buildLoginBtn() {
    return Container(
          width:350,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 90.0,left: 90.0,top: 15.0,bottom: 15.0),child: Text(
        "Update",
        style: GoogleFonts.montserrat(fontSize: 20,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
       onPressed: (){
         FocusManager.instance.primaryFocus.unfocus();
         validateInputs();},),);}
    validateInputs()
  {
    if (_formKey.currentState.validate()) {
       _formKey.currentState.save();
       PassUpdate pass_user = new PassUpdate.novalue();
       pass_user.username = forgot_email;
       pass_user.newpass = base64.encode(utf8.encode(_new.text));
       API.Api.update_password(pass_user, session_id).then((value) {
         if(value.statusCode == 200)
         {
           Navigator.push(context,MaterialPageRoute(builder: (context) => LoginPage()),);
         }
       });
  }}
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
             
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 30.0,
                    vertical: 100.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'New Password',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                         
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height:10.0),
                      Text('Please enter your new Password and confirm that password too',style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          
                          fontSize: 18.0,
                          letterSpacing: 0.2 ,
                          fontWeight: FontWeight.w400,
                         
                        ),textAlign: TextAlign.center,),
                      Form(key: _formKey,
                      child:Column(children:[
                         SizedBox(height: 20.0),
                      _buildNewPassTF(),
                     
                      _buildConfirmPassTF(),
                       SizedBox(height: 20.0),
                       _buildLoginBtn(),
                      ])
                      ),
                     
                    ])))]))));                      
  }
}