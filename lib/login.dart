import 'dart:ui';
import 'dart:convert';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:investment_portal_window/userimage.dart';
import 'package:sign_button/sign_button.dart';
import './forgot_password.dart';
import './sign_in.dart';
import './welcome.dart';
import './api.dart';
import './create.dart';
import './Authentication.dart';
import 'package:linkedin_login/linkedin_login.dart';
import 'package:flutter/gestures.dart';
import 'userimage.dart';

import 'account_menu.dart';
const String redirectUrl = 'https://www.youtube.com/callback';
const String clientId = '86zlw59z8a60t6';
const String clientSecret = "77hpohRKYdmIu897";

class LoginScreen extends StatefulWidget {
  @override
  _LoginScreenState createState() => _LoginScreenState();
}

class _LoginScreenState extends State<LoginScreen> {
    UserObject user;
  bool _rememberMe = false;
  String email="";
  String password="";
  Widget _buildEmailTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 10.0),
        Container(
          alignment: Alignment.centerLeft,
          height: 80.0,
          child: new TextField(
            autocorrect: true,
            decoration: InputDecoration(
              hintText: 'Email',
              contentPadding:
                  const EdgeInsets.only(left: 30.0, bottom: 35.0, top: 15.0),
              hintStyle: TextStyle(color: Colors.grey),
              filled: true,
              fillColor: Colors.grey[80],
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
                borderSide: BorderSide(color: Colors.white, width: 2),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
                borderSide: BorderSide(color: Colors.white),
              ),
            ),
            onChanged: (value){
              setState(() {
                email = value;
              });
            },
          ),
        ),
      ],
    );
  }

  Widget _buildPasswordTF() {
    return Column(
      crossAxisAlignment: CrossAxisAlignment.start,
      children: <Widget>[
        SizedBox(height: 0.0),
        Container(
          alignment: Alignment.centerLeft,
          height: 80.0,
          child: TextField(
            autocorrect: true,
            obscureText: true,
            decoration: InputDecoration(
              hintText: 'Password',
              contentPadding:
                  const EdgeInsets.only(left: 30.0, bottom: 35.0, top: 15.0),
              hintStyle: TextStyle(color: Colors.grey),
              filled: true,
              fillColor: Colors.grey[80],
              enabledBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
                borderSide: BorderSide(color: Colors.white, width: 2),
              ),
              focusedBorder: OutlineInputBorder(
                borderRadius: BorderRadius.all(Radius.circular(50.0)),
                borderSide: BorderSide(color: Colors.white),
              ),
            ),
            onChanged: (value){
              setState(() {
                password = value;
              });
            },
          ),
        ),
      ],
    );
  }

  Widget _buildForgotPasswordBtn() {
    return Container(
      alignment: Alignment.centerRight,
      child: FlatButton(
        onPressed: () {
          Navigator.push(
            context,
            MaterialPageRoute(builder: (context) => ForgotPass()),
          );
        },
        padding: EdgeInsets.only(right: 0.0),
        child: Text(
          'Forgot Password?',
          style: GoogleFonts.montserrat(
            color: HexColor('#0071BC'),
            fontWeight: FontWeight.w500,
            fontSize: 16.0,
          ),
        ),
      ),
    );
  }

  Widget _buildRememberMeCheckbox() {
    return Container(
      height: 20.0,
      child: Row(
        children: <Widget>[
          Theme(
            data: ThemeData(
              unselectedWidgetColor: Colors.grey,
            ),
            child: Checkbox(
              value: _rememberMe,
              checkColor: Colors.green,
              activeColor: Colors.white,
              onChanged: (value) {
                setState(() {
                  _rememberMe = value;
                });
              },
            ),
          ),
          Text('Keep me logged in',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
              )),
        ],
      ),
    );
  }

  Widget _buildLoginBtn() {
    return Container(
      width: 350,
      child: ElevatedButton(
          child: Padding(
            padding: EdgeInsets.only(
                right: 90.0, left: 90.0, top: 15.0, bottom: 15.0),
            child: Text("Sign In",
                style: GoogleFonts.montserrat(
                    fontSize: 20, fontWeight: FontWeight.w600)),
          ),
          style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              backgroundColor:
                  MaterialStateProperty.all<Color>(HexColor("#00A14B")),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                      side: BorderSide(color: HexColor("#00A14B"))))),
          onPressed: () {
            API.Api.login_api(session_id, email,base64.encode(utf8.encode(password)) ).then((value) {
              print(value.body);
                  if(value.body != "")
                  {
                    var data = json.decode(value.body);
                    username_for_app(data['Name']);
                    useremail_for_app(data['Email']);
                    Navigator.push( context, MaterialPageRoute(builder: (context) => Welcome()), );
                  } });
          }),
    );
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.white,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 10.0,
                    vertical: 100.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Text(
                        'Login',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      SizedBox(height: 10.0),
                      Text(
                        'Please login to continue',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          fontSize: 18.0,
                          letterSpacing: 0.2,
                          fontWeight: FontWeight.w500,
                        ),
                      ),
                      SizedBox(height: 30.0),
                      _buildEmailTF(),

                      _buildPasswordTF(),
                      SizedBox(height: 20.0),

                      _buildLoginBtn(),
                      Row(children: <Widget>[
                        _buildRememberMeCheckbox(),
                        Spacer(),
                        _buildForgotPasswordBtn(),
                      ]),

                      // _buildSignInWithText(),
                      // _buildSocialBtnRow(),
                      Row(children: <Widget>[
                        Expanded(
                          child: new Container(
                              margin: const EdgeInsets.only(
                                  left: 10.0, right: 15.0),
                              child: Divider(
                                color: Colors.black,
                                thickness: 1,
                                height: 50,
                              )),
                        ),
                        Text("OR"),
                        Expanded(
                          child: new Container(
                              margin: const EdgeInsets.only(
                                  left: 15.0, right: 10.0),
                              child: Divider(
                                color: Colors.black,
                                thickness: 1,
                                height: 30,
                              )),
                        ),
                      ]),
                      SizedBox(height: 15.0),
                      // RaisedButton(
                      // onPressed: () {
                      //   Authenticate auth = Authenticate();
                      // auth.signInFB().then((onComplete) {
                      //       Navigator.of(context).push(
                      //           MaterialPageRoute(
                      //             builder: (
                      //               context,
                      //             ) {
                      //               // _showLoadingDialog();
                      //               return ForgotPass();
                      //             },
                      //           ),
                      //         );
                      //  },);}),
                   
                      
                      SignInButton(
                          buttonType: ButtonType.facebook,
                          buttonSize: ButtonSize.large,
                          // small(default), medium, large
                          onPressed: () {
                            print('click');
                          }),
                      SizedBox(height: 15.0),
                         SignInButton(
                          buttonType: ButtonType.google,
                          buttonSize: ButtonSize.large,
                          onPressed: () {
                            signInWithGoogle().then((result) {
                              Navigator.of(context).push( MaterialPageRoute( builder: ( context,) {return Welcome();}, ), );
                            });
                          }),
                          SizedBox(height: 15.0),
                      SignInButton(
                          buttonType: ButtonType.linkedin,
                          buttonSize:
                              ButtonSize.large, // small(default), medium, large
                          onPressed: () {
                             FocusScope.of(context).unfocus();
                                 Navigator.push(
                context,
                MaterialPageRoute(
                  builder: (BuildContext context) => LinkedInUserWidget(
                    redirectUrl: redirectUrl,
                    clientId: clientId,
                    clientSecret: clientSecret,
                    projection: [
                      ProjectionParameters.id,
                      ProjectionParameters.localizedFirstName,
                      ProjectionParameters.localizedLastName,
                      ProjectionParameters.firstName,
                      ProjectionParameters.lastName,
                      ProjectionParameters.profilePicture,
                    ],
                    onError: (UserFailedAction e) {
                      print('Error: ${e.toString()}');
                      print('Error: ${e.stackTrace.toString()}');
                    },
                    onGetUserProfile: (UserSucceededAction linkedInUser) {
                      user = UserObject(
                        firstName:
                            linkedInUser?.user?.firstName?.localized?.label,
                        lastName:
                            linkedInUser?.user?.lastName?.localized?.label,
                        email: linkedInUser?.user?.email?.elements[0]
                            ?.handleDeep?.emailAddress,
                        profileImageUrl: linkedInUser
                            ?.user
                            ?.profilePicture
                            ?.displayImageContent
                            ?.elements[0]
                            ?.identifiers[0]
                            ?.identifier,
                      );
                        userimage_for_app(user?.profileImageUrl);
                        username_for_app(user?.firstName +' '+user?.lastName);
                        useremail_for_app(user?.email);
                         Navigator.push( context,MaterialPageRoute(builder: (context) => Welcome()),);},),
                  fullscreenDialog: true,
                ), ); }),
                      SizedBox(height: 10.0),
                      Row(children: [
                        Spacer(),
                        RichText(
                            text: TextSpan(
                                text: "Don't have an Account?",
                                style: TextStyle(
                                    fontSize: 18.0, color: HexColor('#7C7D7E')),
                                children: <TextSpan>[
                              TextSpan(
                                  text: ' Sign Up',
                                  recognizer: TapGestureRecognizer()
                                    ..onTap = () {
                                      Navigator.of(context).push(
                                        MaterialPageRoute(
                                          builder: (
                                            context,
                                          ) {
                                            return CreateAccount();
                                          },
                                        ),
                                      );
                                    },
                                  style: TextStyle(
                                      fontSize: 18.0,
                                      color: HexColor('#0071BC')))
                            ])),
                        Spacer(),]),], ), ),)],), ),),);}}

class UserObject {
  UserObject({this.firstName, this.lastName, this.email, this.profileImageUrl});

  String firstName, lastName, email, profileImageUrl;
}