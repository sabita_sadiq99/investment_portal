import 'dart:convert';
import 'dart:ui';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/gestures.dart';
import 'package:validators/validators.dart';
import './setting.dart';
import 'package:flutter/material.dart';
import 'package:flutter_datetime_picker/flutter_datetime_picker.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import './welcome.dart';
import 'api.dart';
import 'favourites.dart';
import 'myorders.dart';
import 'wallet.dart';
import './userimage.dart';
import 'package:investment_portal_window/sign_in.dart';
import 'package:investment_portal_window/account_menu.dart';
import 'package:rflutter_alert/rflutter_alert.dart';

class Payment extends StatefulWidget{
  _PaymentState createState() => _PaymentState();
}

class _PaymentState extends State<Payment>{
  int _selectedIndex = 1;
  bool country =false;
    bool _rememberMe = false;
  List<String> _pakistan = ['Jazzcash','Easypaisa'];
  List<String> UK = ['Credit Card','Debit Card'];
  final _account = TextEditingController();
  final _accountnumber = TextEditingController();
  final _mobile = TextEditingController();
  final _formKey = GlobalKey<FormState>();
  DateTime _date = null;
  final _cvv = TextEditingController();
  var resultsList = new List.filled(2, '');
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
       void _onItemTapped(int index) {
    if (index == 0) {
      Navigator.push(context,MaterialPageRoute(builder: (context) => Welcome()),);
    }
     if (index == 1) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Favourites()),
      );
    }
    if (index == 2) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Orders()),
      );
    }

    if (index == 4) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Setting()),
      );
    }
  }
  @override
  void initState()
  {
    resultsList[0] = 'Jazzcash';
    resultsList[1] = 'Credit Card';
  }
    _fieldDropDown(List theList, int resultPosition, var dbField) {
    return new FormField(
      builder: (FormFieldState state) {
        return InputDecorator(
           decoration: InputDecoration(
    contentPadding: EdgeInsets.only(left:20.0,top:20.0,right:20.0,bottom: 20.0),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
      errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
          ),
          child: new DropdownButtonHideUnderline(
            child: new DropdownButton(
              value: this.resultsList[resultPosition],
              style: GoogleFonts.montserrat(color:HexColor("#7C7D7E"),fontSize:16),
              isDense: true,
              onChanged: ( newValue) {
                setState(() {
                  this.resultsList[resultPosition] = newValue;
                  state.didChange(newValue);
                  print(
                      'The List result = ' + this.resultsList[resultPosition]);
                });
              },
              items: theList.map((value) {
                return new DropdownMenuItem(
                  value: value,
                child: new Text(value),
                );
              }).toList(),
            ),
          ),
        );
      },
    );

  }
   Widget build(BuildContext context) {
    return Scaffold(
       key: _scaffoldKey,
           drawer:Drawer(
        child: Container(
          //child: Your widget,
          color: HexColor('#1b2f35'),
          width: double.infinity,
          height: double.infinity,
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: HexColor('#1b2f35'),
                ),
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft + Alignment(0.2, 0),
                      child: new Image.asset(
                        'logo-white.png',
                        width: 100.0,
                        height: 100.0,
                        fit: BoxFit.contain,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomLeft + Alignment(0.2, 1.5),
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                           get_image()),
                        radius: 30.0,
                      ),
                    ),
                    Align(
                      alignment: Alignment(0.3, 1.5),
                      child: Text(
                        get_name(),
                        style: TextStyle(color: Colors.white, fontSize: 20.0),
                      ),
                    ),
                    SizedBox(
                      height: 2.0,
                    ),
                    Align(
                      alignment: Alignment(0.6, 1.8),
                      child: Text(
                        get_email(),
                        style: TextStyle(color: Colors.white, fontSize: 15.0),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 50.0),
              new Divider(
                color: Colors.white,
                thickness: 0.8,
              ),
              ListTile(
                leading: Icon(
                  Icons.home,
                  color: Colors.white,
                ),
                title: Text(
                  'Home',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: null,
              ),
              ListTile(
                leading: Icon(
                  Icons.shopping_bag,
                  color: Colors.white,
                ),
                title: Text(
                  'My Orders',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: (){
                   Navigator.push(context,MaterialPageRoute(builder: (context) => Orders()));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.account_balance_wallet,
                  color: Colors.white,
                ),
                title: Text(
                  'My Wallet',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
               onTap: (){
                   Navigator.push(context,MaterialPageRoute(builder: (context) => Wallet()));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.settings,
                  color: Colors.white,
                ),
                title: Text(
                  'Account Settings',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Setting()),
                  );
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.power_settings_new_rounded,
                  color: Colors.white,
                ),
                title: Text(
                  'Sign Out',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: () => _onBasicAlertPressed(context),),],),),),
                      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag_outlined),
            label: 'My Orders',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
          ),
        ],
        iconSize: 30.0,
      selectedItemColor: HexColor('#7C7D7E'),
         currentIndex: _selectedIndex,
        unselectedItemColor: HexColor('#7C7D7E'),
        unselectedLabelStyle: TextStyle(color: Colors.black),
        onTap: _onItemTapped,
        showUnselectedLabels: true,
      ),
       backgroundColor: Colors.white,
            body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 16.0,
                    vertical: 60.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children:[
                          Padding(padding: EdgeInsets.only(left:0.0),child:
                   IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: HexColor('#0071BC'),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),),
                CircularProfileAvatar(
                get_image(), 
                  radius: 25,
                  backgroundColor: Colors.transparent, 
                  elevation: 5.0, 
                  cacheImage: true, 
                  onTap: () => _scaffoldKey.currentState.openDrawer(),
                  showInitialTextAbovePicture: true, 
                  ),
                  Padding(padding:EdgeInsets.only(left: MediaQuery.of(context).size.width / 10,top:10.0) ,child:Text(
                          'Payment',
                          style: GoogleFonts.montserrat(
                            color: HexColor('#0071BC'),
                            fontSize: 25.0,    
                            fontWeight: FontWeight.bold,
                          ),
                        ),),
            
                        Spacer(),
                        Spacer(),
                      ]),
                      SizedBox(height: 20.0),
                      Padding(padding:EdgeInsets.only(left:14.0) ,child: Text(
                        'Add new payment card',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          fontSize: 18.0,
                          letterSpacing: 0.1,
                          fontWeight: FontWeight.w500,
                        ),
                        textAlign: TextAlign.center,
                        ),),
                      SizedBox(
                        height: 20.0,
                      ),
                          SizedBox(height:15.0),
                          Row(children: [
                            Padding(padding: EdgeInsets.only(left:10.0),child: Text('Select Region:',style: GoogleFonts.montserrat(
                          color: HexColor("#4A4B4D"),
                          fontSize: 16.0,
                          fontWeight: FontWeight.w300
                        ),),),
                        Spacer(),
                              RichText(text:  TextSpan(
                                  text:'Pakistan ',
                                   recognizer: TapGestureRecognizer()
                    ..onTap = () {
                        setState(() {
                          country =false;
                        });
                    },style: TextStyle(fontSize:15.0,  color:country?HexColor("#B6B7B7"):HexColor('#0071BC'),
                                )
                              )),
                              Text("|",style: TextStyle(color: Colors.black),),
                               RichText(text:  TextSpan(
                                  text:' United Kingdom',
                                   recognizer: TapGestureRecognizer()
                    ..onTap = () {
                        setState(() {
                          country =true;
                        });
                    },style: TextStyle(fontSize:15.0,
                    color:country?HexColor('#0071BC'):HexColor("#B6B7B7"),
                                )
                              )),
                        ],),
                        SizedBox(height:15.0),
                      country?_fieldDropDown(UK, 1, 'colorDBfield'):_fieldDropDown(_pakistan, 0, 'colorDBfield'),
                        SizedBox(height:20.0),
                        country?Row(children: [
                            Padding(padding: EdgeInsets.only(left:10.0),child: Text('We Accept',style: GoogleFonts.montserrat(
                          color: Colors.black,
                          fontSize: 18.0,
                          letterSpacing: 0.1,
                          fontWeight: FontWeight.w400,
                        ),),),
                        Padding(padding: EdgeInsets.only(left:10.0,right:10.0),child:Image(image: AssetImage('assets/visa.png')),),
                        Padding(padding: EdgeInsets.only(right: 10.0),child:Image(image: AssetImage('assets/mastercard.png')),),
                        Padding(padding: EdgeInsets.only(right: 10.0),child:Image(image: AssetImage('assets/americanexpress-color-large.png')),),
                        Padding(padding: EdgeInsets.only(right: 10.0),child:Image(image: AssetImage('assets/discover-light-large.png')),),
                        ],):Row(children: [
                            Padding(padding: EdgeInsets.only(left:10.0),child: Text('We Accept',style: GoogleFonts.montserrat(
                          color: Colors.black,
                          fontSize: 18.0,
                          letterSpacing: 0.1,
                          fontWeight: FontWeight.w400,
                        ),),),
                        Padding(padding: EdgeInsets.only(left:10.0,right:10.0),child:Image(image: AssetImage('assets/jazzcash.png'),height: 33,width: 50,),),
                        Padding(padding: EdgeInsets.only(right: 10.0),child:Image(image: AssetImage('assets/easypaisa.png'),height:33,width: 50,),),
                        ],),
                        SizedBox(height:30.0),
                          Row(children: [
                            Padding(padding: EdgeInsets.only(left:10.0),child: Text('Card Details',style: GoogleFonts.montserrat(
                          color: Colors.black,
                          fontSize: 18.0,
                          letterSpacing: 0.1,
                          fontWeight: FontWeight.w600,
                        ),),)],),
                        SizedBox(height:20),
                        Form(
                        key: _formKey,
                        child: Column(children: [
                          AccountInput(),
                          SizedBox(height:5),
                          AccountNumber(),
                          SizedBox(height:5),
                          MobileNumber(),
                          SizedBox(height:5),
                            Row(
                          children: [
                            Flexible(child:CVV(),),
                          
                          ],
                        ),
                        ],),),
                        SizedBox(height:5),
                          Row(children: <Widget>[
                        _buildRememberMeCheckbox(),
                        Spacer(),
                       
                      ]),
                      SizedBox(height:20.0),
                      _buildLoginBtn(context),
              
              SizedBox(height:30.0),
            
              ])
                ))]))));}
                                  _onBasicAlertPressed(context) {
                 Alert(
                 context: context,
                 title: "Are you sure you want to logout?",
                style: AlertStyle(
                titleStyle: GoogleFonts.montserrat(
                color: HexColor('#4A4B4D'),
                 fontSize: 18,
                fontWeight: FontWeight.w700),
                descStyle: GoogleFonts.montserrat(
                color: HexColor('#4A4B4D'),
                fontSize: 17,
                fontWeight: FontWeight.w400)),
                desc: "We can't notify you of new stocks if you do.",
                buttons: [
                DialogButton(
                child: Text(
                "Cancel",
                 style: GoogleFonts.montserrat(
                 color: HexColor('#4A4B4D'),
                fontSize: 18,
                 fontWeight: FontWeight.w700),
                 ),
                onPressed: () => Navigator.pop(context),
                color: Colors.white,
                border: Border.fromBorderSide(BorderSide(
                color: Colors.black, width: 0.8, style: BorderStyle.solid)),
                 radius: const BorderRadius.all(Radius.circular(50))),
                 DialogButton(
                child: Text(
                 "Logout",
                style: GoogleFonts.montserrat(
                 color: Colors.white,
                 fontSize: 18,
                fontWeight: FontWeight.w700),
                ),
                onPressed: () =>{
                if(get_login_method() == 1)
                 {
                signOutGoogle(),
                session_id = null,
                }
                else if(get_login_method() == 2)
                {

                },
                Navigator.push(context, MaterialPageRoute(builder: (context) => Account_select()),),
                },
                color: HexColor('#00A14B'),
                 border: Border.fromBorderSide(BorderSide(color: HexColor('#00A14B'), width: 2,style: BorderStyle.solid)),radius: const BorderRadius.all(Radius.circular(50))),], ).show();
      }
       Widget AccountInput() {
    return TextFormField(
      controller: _account,
    	decoration: InputDecoration(
    hintText: 'Account Title', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: GoogleFonts.montserrat(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	),
      keyboardType: TextInputType.text,
      validator: (_account) {
       if (_account == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_account))
        {
          return 'Invalid ';
        }
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
        Widget AccountNumber() {
    return TextFormField(
      controller: _accountnumber,
    	decoration: InputDecoration(
    hintText: 'Account Number', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: GoogleFonts.montserrat(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	),
      keyboardType: TextInputType.text,
      validator: (_accountno) {
       if (_accountno == "") {
          return 'Please fill this field';
        } 
        else if(_accountno.length !=12 || _accountno.length!=16)
        {
          return 'Invalid ';
        }
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
    Widget MobileNumber() {
    return TextFormField(
      controller: _mobile,
    	decoration: InputDecoration(
    hintText: 'Mobile Number', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: GoogleFonts.montserrat(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	),
      keyboardType: TextInputType.phone,
      validator: (_mobile) {
       if ((_mobile.length < 11 || _mobile.length >11 )&& _mobile.length >0){ 
       }
       else if(_mobile.length==0)
          {
          return 'Please fill this field';
        } 
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
    Widget _buildRememberMeCheckbox() {
    return Container(
      height: 20.0,
      child: Row(
        children: <Widget>[
          Theme(
            data: ThemeData(
              unselectedWidgetColor: Colors.grey,
            ),
            child: Checkbox(
              value: _rememberMe,
              checkColor: Colors.white,
              activeColor: Colors.blue,
              
              onChanged: (value) {
                setState(() {
                  _rememberMe = value;
                });
              },
            ),
          ),
          Text('Set as Default',
              style: GoogleFonts.montserrat(
                color: HexColor('#7C7D7E'),
                fontSize: 16
              )),
        ],
      ),
    );
  }
      Widget CVV() {
    return TextFormField(
      controller: _cvv,
    	decoration: InputDecoration(
    hintText: 'CVV', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: GoogleFonts.montserrat(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	),
      keyboardType: TextInputType.number,
      validator: (_cvv) {
        if(_cvv== "")
          {
          return 'Please fill this field';
        } 
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
}
 Widget _buildLoginBtn(context) {
    return Container(
      width: 350,
      child: ElevatedButton(
          child: Padding(
            padding: EdgeInsets.only(
                right: 90.0, left: 90.0, top: 15.0, bottom: 15.0),
            child: Text("Add Card",
                style: GoogleFonts.montserrat(
                    fontSize: 20, fontWeight: FontWeight.w600)),
          ),
          style: ButtonStyle(
              foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
              backgroundColor:
                  MaterialStateProperty.all<Color>(HexColor("#00A14B")),
              shape: MaterialStateProperty.all<RoundedRectangleBorder>(
                  RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(50),
                      side: BorderSide(color: HexColor("#00A14B"))))),
          onPressed: () {
           
          }),
    );
  }
    