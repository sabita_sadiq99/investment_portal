class UpdateOrder{
  String orderid;
  String status;
  factory UpdateOrder.fromJson(Map<String,dynamic> json)
  {
    return UpdateOrder(
      json['p_OrderId'],
      json['p_PaymentStatus'],
    );
  }
  UpdateOrder.novalue();
  Map<String,dynamic> toJson()=>{
    'p_OrderId' : orderid,
    'p_PaymentStatus' : status,
  };
  UpdateOrder(
    this.orderid,
    this.status,
  );
}