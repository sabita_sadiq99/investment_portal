import 'dart:convert';
import 'dart:ui';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import './setting.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import './welcome.dart';
import 'api.dart';
import 'detail.dart';
import 'myorders.dart';
import 'wallet.dart';
import './userimage.dart';
import 'package:investment_portal_window/sign_in.dart';
import 'package:investment_portal_window/account_menu.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
class Favourites extends StatefulWidget{
  _FavouritesState createState() => _FavouritesState();
}
class _FavouritesState extends State<Favourites>{
final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
int _selectedIndex = 1;
bool receive = false;
List<dynamic> productDetails = List<dynamic>();
       void _onItemTapped(int index) {
    if (index == 0) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Welcome()),
      );
    }
     if (index == 1) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Favourites()),
      );
    }
    if (index == 2) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Orders()),
      );
    }

    if (index == 4) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Setting()),
      );
    }
  }
void initState() {
 API.Api.getFavs(session_id).then((value) {
 var data = json.decode(value.body);
 var res = data as List;
 for (int i = 0; i < res.length; i++) {
 print(res[i]);
 productDetails.add(res[i]);
 }
 setState(() {
 receive = true;
 });
 });
 super.initState();
 }
  @override
  Widget build(BuildContext context) {
    return Scaffold(
       key: _scaffoldKey,
           drawer:Drawer(
        child: Container(
          //child: Your widget,
          color: HexColor('#1b2f35'),
          width: double.infinity,
          height: double.infinity,
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: HexColor('#1b2f35'),
                ),
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft + Alignment(0.2, 0),
                      child: new Image.asset(
                        'logo-white.png',
                        width: 100.0,
                        height: 100.0,
                        fit: BoxFit.contain,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomLeft + Alignment(0.2, 1.5),
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                           get_image()),
                        radius: 30.0,
                      ),
                    ),
                    Align(
                      alignment: Alignment(0.3, 1.5),
                      child: Text(
                        get_name(),
                        style: TextStyle(color: Colors.white, fontSize: 20.0),
                      ),
                    ),
                    SizedBox(
                      height: 2.0,
                    ),
                    Align(
                      alignment: Alignment(0.6, 1.8),
                      child: Text(
                        get_email(),
                        style: TextStyle(color: Colors.white, fontSize: 15.0),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 50.0),
              new Divider(
                color: Colors.white,
                thickness: 0.8,
              ),
              ListTile(
                leading: Icon(
                  Icons.home,
                  color: Colors.white,
                ),
                title: Text(
                  'Home',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: null,
              ),
              ListTile(
                leading: Icon(
                  Icons.shopping_bag,
                  color: Colors.white,
                ),
                title: Text(
                  'My Orders',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: (){
                   Navigator.push(context,MaterialPageRoute(builder: (context) => Orders()));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.account_balance_wallet,
                  color: Colors.white,
                ),
                title: Text(
                  'My Wallet',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
               onTap: (){
                   Navigator.push(context,MaterialPageRoute(builder: (context) => Wallet()));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.settings,
                  color: Colors.white,
                ),
                title: Text(
                  'Account Settings',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Setting()),
                  );
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.power_settings_new_rounded,
                  color: Colors.white,
                ),
                title: Text(
                  'Sign Out',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: () => _onBasicAlertPressed(context),),],),),),
                      bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag_outlined),
            label: 'My Orders',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
          ),
        ],
        iconSize: 30.0,
      selectedItemColor: HexColor('#0071BC'),
         currentIndex: _selectedIndex,
        unselectedItemColor: HexColor('#7C7D7E'),
        unselectedLabelStyle: TextStyle(color: Colors.black),
        onTap: _onItemTapped,
        showUnselectedLabels: true,
      ),
       backgroundColor: Colors.white,
            body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 20.0,
                    vertical: 60.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children:[
                          Padding(padding: EdgeInsets.only(left:0.0),child:
                   IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: HexColor('#0071BC'),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),),
                CircularProfileAvatar(
                get_image(), 
                  radius: 25,
                  backgroundColor: Colors.transparent, 
                  elevation: 5.0, 
                  cacheImage: true, 
                  onTap: () => _scaffoldKey.currentState.openDrawer(),
                  showInitialTextAbovePicture: true, 
                  ),
                  
                  Padding(padding:EdgeInsets.only(left:35.0,top:0.0) ,child:Text(
                          'Favourites',
                          style: GoogleFonts.montserrat(
                            color: HexColor('#0071BC'),
                            fontSize: 25.0,
                            fontWeight: FontWeight.bold,
                          ),
                        ),),
            
                        Spacer(),
                        Spacer(),
                      ]),
                      SizedBox(height: 20.0),
                      Padding(padding:EdgeInsets.only(left:10.0) ,child: Text(
                        'Here are your fav items',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          fontSize: 18.0,
                          letterSpacing: 0.1,
                          fontWeight: FontWeight.w500,
                        ), ) ,),
                      SizedBox(
                        height: 20.0,
                      ),
                                   Container(
                        height: 45,
                        width: MediaQuery.of(context).size.width / 1.1,
                        child: Row(
                          children: [
                            Expanded(
                              child: new Theme(
                                data: new ThemeData(
                                  primaryColor:Colors.white,
                                  primaryColorDark: HexColor('#0071BC'),
                                ),
                                child: TextField(
                                  decoration: InputDecoration(
                                    hintText: 'Search Here',
                                    hintStyle: GoogleFonts.montserrat(
                                      color: HexColor('#7C7D7E'),
                                      fontSize: 14.0,
                                      letterSpacing: 0.1,
                                      fontWeight: FontWeight.w300,
                                    ),
                                    contentPadding:
                                        EdgeInsets.only(top: 5.0, left: 25.0),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(50.0)),
                                      borderSide: BorderSide(
                                        color:  HexColor('#0071BC')
                                      ),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(50.0)),
                                      borderSide: BorderSide(
                                        color: HexColor('#0071BC'),
                                      ),
                                    ),
                                    fillColor: Colors.white,
                                    filled: true,
                                       suffixIcon: Material(
                            elevation: 5.0,
                            color:  HexColor('#0071BC'),
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(30.0),
                              bottomRight: Radius.circular(30.0),
                            ),
                            child: Icon(Icons.search, color: Colors.white),
                          ),),),),),
                            SizedBox(width: 10),
                          ],),),
                          SizedBox(height:20.0),
                         receive
 ? Container(
 child: ListView.builder(
 scrollDirection: Axis.vertical,
 shrinkWrap: true,
 itemExtent: 100.0,
 itemCount: productDetails.length,
 padding: EdgeInsets.only(top:10.0),
 itemBuilder: (BuildContext context, int index) {
 
 return Card(
 elevation: 8.0,
 child: Container(
 decoration:
 BoxDecoration(color: Colors.white),
 child: ListTile(
 leading: Wrap(children: [
 Container(
 width: 80.0,
 height: 60.0,
 decoration: BoxDecoration(
 image: DecorationImage(
 fit: BoxFit.cover,
 image: MemoryImage(base64Decode(productDetails[index]['blobImageString']))),
 borderRadius: BorderRadius.all(
 Radius.circular(8.0)),
 ),
 ),
 Container(
 height: 60,
 child: VerticalDivider(
 color: Colors.grey[500])),
 ]),
 title: Padding(
 padding: EdgeInsets.only(
 top: 5.0, left: 0.0),
 child: Text(
 productDetails[index]['ProducName'],
 style: GoogleFonts.montserrat(
 color: HexColor('#4A4B4D'),
 fontSize: 20.0,
 letterSpacing: 0.1,
 fontWeight: FontWeight.w600,
 ),
 ),
 ),
 subtitle: Column(
 crossAxisAlignment:
 CrossAxisAlignment.start,
 children: [
 SizedBox(height: 5.0),
 RichText(
 text: TextSpan(
 text: 'Price',
 style: GoogleFonts.montserrat(
 color: Colors.black,
 fontSize: 15.0),
 children: <TextSpan>[
 TextSpan(
 text: '  ${productDetails[index]['Price']}',
 style: TextStyle(
 color: HexColor(
 '#7C7D7E'),
 fontSize: 15.0))
 ],
 ),
 ),
 RichText(
 text: TextSpan(
 text: 'Mts',
 style: GoogleFonts.montserrat(
 color: Colors.black,
 fontSize: 15.0),
 children: <TextSpan>[
 TextSpan(
 text: '  ${productDetails[index]['Age']}',
 style: TextStyle(
 color: HexColor(
 '#7C7D7E'),
 fontSize: 15.0))
 ],
 ),
 )
 ],
 ),
 trailing: Column(children: [
   Icon(Icons.favorite,color: Colors.red,),
 SizedBox(height: 14.0),
 InkWell(
 child: Icon(
 Icons.arrow_forward_ios,
 size: 18.0,
 color: HexColor('#00A14B'),
 ),
 onTap: () {
 Navigator.push(
 context,
 MaterialPageRoute(
 builder: (context) =>
 Detail(product_id: productDetails[index]['PDID'] ,)),
 );
 },
 ),
 ]),
 onTap: () {
 
 },
 )));
 },
 )):Center(
 child: Column(
 mainAxisAlignment: MainAxisAlignment.center,
 children: <Widget>[
 SizedBox(height: 40.0),
 CircularProgressIndicator(),
 ],
 )),
                    ])
                ))]))));}
                 _onBasicAlertPressed(context) {
                 Alert(
                 context: context,
                 title: "Are you sure you want to logout?",
                style: AlertStyle(
                titleStyle: GoogleFonts.montserrat(
                color: HexColor('#4A4B4D'),
                 fontSize: 18,
                fontWeight: FontWeight.w700),
                descStyle: GoogleFonts.montserrat(
                color: HexColor('#4A4B4D'),
                fontSize: 17,
                fontWeight: FontWeight.w400)),
                desc: "We can't notify you of new stocks if you do.",
                buttons: [
                DialogButton(
                child: Text(
                "Cancel",
                 style: GoogleFonts.montserrat(
                 color: HexColor('#4A4B4D'),
                fontSize: 18,
                 fontWeight: FontWeight.w700),
                 ),
                onPressed: () => Navigator.pop(context),
                color: Colors.white,
                border: Border.fromBorderSide(BorderSide(
                color: Colors.black, width: 0.8, style: BorderStyle.solid)),
                 radius: const BorderRadius.all(Radius.circular(50))),
                 DialogButton(
                child: Text(
                 "Logout",
                style: GoogleFonts.montserrat(
                 color: Colors.white,
                 fontSize: 18,
                fontWeight: FontWeight.w700),
                ),
                onPressed: () =>{
                if(get_login_method() == 1)
                 {
                signOutGoogle(),
                session_id = null,
                }
                else if(get_login_method() == 2)
                {

                },
                Navigator.push(context, MaterialPageRoute(builder: (context) => Account_select()),),
                },
                color: HexColor('#00A14B'),
                 border: Border.fromBorderSide(BorderSide(color: HexColor('#00A14B'), width: 2,style: BorderStyle.solid)),radius: const BorderRadius.all(Radius.circular(50))),], ).show();
      }

}