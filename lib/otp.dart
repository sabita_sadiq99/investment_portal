
import 'package:flutter/material.dart';
import 'package:email_auth/email_auth.dart';
import 'package:flutter_otp/flutter_otp.dart';

void main() => runApp(MyApp());

class MyApp extends StatelessWidget {
  // This widget is the root of your application.
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      title: 'Flutter LinkedIn login',
      theme: ThemeData(
        primarySwatch: Colors.blue,
      ),
      home: MyHomePage(title: 'Flutter LinkedIn login'),
    );
  }
}

class MyHomePage extends StatefulWidget {
  MyHomePage({Key key, this.title}) : super(key: key);

  final String title;

  @override
  _MyHomePageState createState() => _MyHomePageState();
}

class _MyHomePageState extends State<MyHomePage> {
  final TextEditingController _optcontroller =  TextEditingController();
  void SendOTP() async {
    EmailAuth.sessionName = "Test Session";
    var res = await EmailAuth.sendOtp(receiverMail: 'k173659@nu.edu.pk');
    if(res){
      print('OPT Verified');
    }
    else
    {
      print('Invalid OTP');
    }

  }
   void verifyOTP() async {
     var res = EmailAuth.validate(receiverMail: 'k173659@nu.edu.pk', userOTP: _optcontroller.text);
     if(res){
      print('OPT Verified');
    }
    else
    {
      print('Invalid OTP');
    }
   }
  @override
  Widget build(BuildContext context) {
    return Scaffold(

      appBar: AppBar(
        title: Text(widget.title),
      ),
      body: Center(
        child: Column(children: [
          ElevatedButton(onPressed: (){
            SendOTP();
          }, child: Text('Send OPT')),
            CVVInput(),
              ElevatedButton(onPressed: (){
            verifyOTP();
          }, child: Text('Validate OPT')),
        ],
      ),
    ),);
  }
   Widget CVVInput() {
    return TextFormField(
      controller: _optcontroller,
    	decoration: InputDecoration(
    hintText: 'CVV', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.grey),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	),
      keyboardType: TextInputType.text,
      validator: (_cvv) {
        if (_cvv.length < 3 && _cvv.length >0)
          return 'Invalid';
        else if (_cvv == "") {
          return 'Please fill this field';
        } 
        else
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }

}
