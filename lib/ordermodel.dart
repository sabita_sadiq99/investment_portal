
class Order{
  String productid;
  String quantity;
  String price;
  String userid;
  factory Order.fromJson(Map<String, dynamic> json){
  return Order(
    json['p_ProductId'],
    json['p_Quantity'],
    json['p_UnitPrice'],
    json['UserId'],
  );
}
Order.novalue();
Map<String,dynamic> toJson()=>{
  'p_ProductId' : productid,
  'p_Quantity' : quantity,
  'p_UnitPrice' : price,
  'UserId' : userid,
};
Order(
  this.productid,
  this.quantity,
  this.price,
  this.userid,
);
}