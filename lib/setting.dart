import 'dart:convert';
import 'dart:ui';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/gestures.dart';
import 'package:investment_portal_window/api.dart';
import 'package:investment_portal_window/login.dart';
import 'package:investment_portal_window/userimage.dart';
import 'package:investment_portal_window/userupdatemodel.dart';
import './change_pass.dart';
import 'package:step_progress_indicator/step_progress_indicator.dart'; 
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:validators/validators.dart';
import './welcome.dart';
import './uploadcnic.dart';
import 'account_menu.dart';
class Setting extends StatefulWidget{
  _SettingState createState() => _SettingState();
}
class _SettingState extends State<Setting>{
final _formKey = GlobalKey<FormState>();
final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
@override
int _selectedIndex = 4;
int currentstate =0 ;
bool check= true;
int formstate=0;
List<dynamic> userdetail = new List<dynamic>();
final _username = TextEditingController();
final _lastname = TextEditingController();
final _email = TextEditingController();
final _phone = TextEditingController();
final _address = TextEditingController();
final _address2 = TextEditingController();
final _cnic = TextEditingController();
final _city = TextEditingController();
final _country = TextEditingController();
bool receive = false;
void initState(){
  API.Api.check_user_status('admin@cloudvision.com', session_id).then((value) {
    var data = json.decode(value.body);
      setState(() {
    userdetail.add(data['FirstName']);
    userdetail.add(data['Username']);
    userdetail.add(data['Email']);
    userdetail.add(data['Street']);
    userdetail.add(data['State']);
    userdetail.add(data['City']);
    userdetail.add(data['Country']);
    userdetail.add(data['Phone']);
    userdetail.add(data['CNIC_NICOP__c']);
    userdetail.add(data['LastName']);
    receive = true;
  });
    
  });

 super.initState();
}
void _onItemTapped(int index) {
if(index==0)
{
 Navigator.push(
context,
    MaterialPageRoute(builder: (context) => Welcome()),
  );
    }

   if(index == 4)
   {
      Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Setting()),
  );
   }
  }
    Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 20.0,
                    vertical: 60.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children:[Spacer(),
                             IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: HexColor('#0071BC'),
                  ),
                  onPressed: () {
                    Navigator.pop(context); },),
          Spacer(),
              Text('Account Settings',style: GoogleFonts.montserrat( color: HexColor('#0071BC'),fontSize: 22.0,fontWeight: FontWeight.bold,
                        ),
                      ),
                      Spacer(),
                    Spacer(),
                    Spacer(),
                        ]
                      ),
                      SizedBox(height:10.0),
                      ImageProfile(),
                        SizedBox(height:30.0),
                        Padding(padding: EdgeInsets.only(left:10.0,right:10.0),
                        child:check? StepProgressIndicator(
    totalSteps: 5,
    currentStep: currentstate,
    size: 12,
    padding: 5.0,
    unselectedColor: Colors.grey[350],
    selectedColor: Colors.green,
):
SizedBox(height:0.0)),
 receive?Column(children: [
                          Form(
                          key:_formKey,
                          child:Column(children: [
                             SizedBox(height:20.0),
                             NameInput(),
                             SizedBox(height:10.0),
                             LastNameInput(),
                            SizedBox(height:10.0),
                             EmailInput(),
                              SizedBox(height:10.0),
                            Phone(),
                            SizedBox(height:10.0),
                            CNIC(),
                            SizedBox(height:10.0),
                            Street(),
                            SizedBox(height:10.0),
                            state(),
                            SizedBox(height:10.0),
                            City(),
                            SizedBox(height:10.0),
                            Country(),
                            SizedBox(height:10.0),
                            Align(
                              alignment: Alignment.bottomLeft+ Alignment(0.1,0)
                              ,child:
                               RichText(text: TextSpan(text:'Change Password',recognizer: TapGestureRecognizer()..onTap= (){
                                 Navigator.push( context,MaterialPageRoute(builder: (context) => ChangePass()),);

                            },style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                          fontSize: 15.0,
                          decoration: TextDecoration.underline, ), ))),SizedBox(height:10.0),
                            Align(
                              alignment: Alignment.bottomLeft+ Alignment(0.1,0)
                              ,child:
                               RichText(text: TextSpan(text:'Upload CNIC Image',recognizer: TapGestureRecognizer()..onTap= (){
                                  Navigator.push(context,MaterialPageRoute(builder: (context) => UploadCnic()),);
                            },style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                          fontSize: 15.0,
                           ), )) )
                           ],)
                        ),
                        SizedBox(height:15.0),
                          Container(
          width:350,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 90.0,left: 90.0,top: 15.0,bottom: 15.0),child: Text(
        "Update",
        style: GoogleFonts.montserrat(fontSize: 20,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
       onPressed: (){
               _formKey.currentState.save();
              UserUpdate user = new UserUpdate.novalue();
              user.username = userdetail[1];
              if(_lastname.text == "")
                user.lastname = userdetail[9];
              else
              {
                 user.lastname = _lastname.text;
              }  
              if(_username.text == "")
                user.firstname = userdetail[0];
              else
              {
                user.firstname = _username.text;
              }
                
              if(_address.text == "")
                user.street = userdetail[3];
              else
              {
                user.street = _address.text;
              }
                
              if(_address2.text == "")
                user.state = userdetail[4];
              else
              {
                  user.state = _address2.text;
              }  
              if(_city.text == "")
                user.city = userdetail[5];
              else
              {
                 user.city = _city.text;
              }
               
              if(_country.text == "")
                user.country = userdetail[6];
              else
              {
                user.country = _country.text;
              }
              if(_phone.text == "")
                user.phone = userdetail[7];
              else
              {
                user.phone = _phone.text;
              } 
              if(_cnic.text == "")
                user.cnic = userdetail[8];
              else
              {
                 user.cnic = _cnic.text;
              }
              
              API.Api.update_user(user, session_id).then((value) {
                  print(value.statusCode);
                });
       },
    ), ), ],):     Center(
                              child: Column(
                              mainAxisAlignment: MainAxisAlignment.center,
                              children: <Widget>[
                                SizedBox(height: 40.0),
                                CircularProgressIndicator(),
                              ],
                            )),
                      
                      ],),
                ),
              ),
             
            ],
          ),
        ),
      ),
      bottomNavigationBar:BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag_outlined),
            label: 'My Orders',
          ),
            BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
            
          ),
       
          
        ],
        iconSize: 30.0,
        currentIndex: _selectedIndex,
        selectedItemColor: HexColor('#0071BC'),
        unselectedItemColor: HexColor('#7C7D7E'),
        unselectedLabelStyle: TextStyle(color:Colors.black),
        onTap: _onItemTapped,
      ),
    );
  }
  Widget ImageProfile()
  {
    return Center(
      child:Stack(children: [
        CircleAvatar(radius:60.0,backgroundImage: NetworkImage(get_image()),),
        Positioned(
          bottom: 10.0,
          left: 95.0,
          right: 0.0,
        child: InkWell(
          onTap: (){},
          child: Icon(Icons.camera_alt,
          color: Colors.black,size: 28.0,),
        ))
      ],)
    );
  }
     Widget NameInput() {
    return TextFormField(
      readOnly: true,
      controller: _username,
    	decoration: InputDecoration(
    hintText: userdetail[0], contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0), 
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_username) {
        if (_username.length < 3)
          return 'Name must be more than 2 character';
        else if (_username == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_username))
        {
          return 'Invalid Name';
        }
        else
        {
          setState(() {
            formstate = formstate +1;
          });
          return null;} },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
      Widget LastNameInput() {
    return TextFormField(
      readOnly: true,
      controller: _lastname,
    	decoration: InputDecoration(
    hintText: userdetail[9], contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0), 
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_username) {
        if (_username.length < 3)
          return 'Name must be more than 2 character';
        else if (_username == "") {
          return 'Please fill this field';
        } 
        else if(!isAlpha(_username))
        {
          return 'Invalid Name';
        }
        else
        {
          setState(() {
            formstate = formstate +1;
          });
          return null;} },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
 Widget EmailInput() {
    return TextFormField(
      readOnly: true,
      controller: _email,
    	decoration: InputDecoration(
    hintText: userdetail[1], contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	),
      keyboardType: TextInputType.text,
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
   Widget Phone() {
    return TextFormField(
      
      controller: _phone,
    	decoration: InputDecoration(
    hintText: userdetail[7], contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
      errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_phone) {
        if (_phone.length < 12)
          return 'Invalid';
        else if (_phone == "") {
          return 'Please fill this field';
        } 
        else if(!isNumeric(_phone))
        {
          return 'Invalid';
        }
        else
         setState(() {
            formstate = formstate +1;
          });
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
    Widget Street() {
    return TextFormField(
      controller: _address,
    	decoration: InputDecoration(
    hintText: userdetail[3], contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
      errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_address) {
       if (_address == "") {
          return 'Please fill this field';
        } 
         setState(() {
            formstate = formstate +1;
          });
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
     Widget state() {
    return TextFormField(
      controller: _address2,
    	decoration: InputDecoration(
    hintText: userdetail[4], contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
      errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
          validator: (_address2) {
       if (_address2 == "") {
          return 'Please fill this field';
        } 
         setState(() {
            formstate = formstate +1;
          });
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
    Widget CNIC() {
    return TextFormField(
      controller: _cnic,
    	decoration: InputDecoration(
    hintText: userdetail[8], contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
      errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_cnic) {
        if (_cnic.length < 15 && _cnic.length >0)
          return 'Invalid Cnic';
        else if (_cnic == "") {
          return 'Please fill this field';
        } 
        else if(!isAlphanumeric(_cnic))
        {
          return 'Invalid Cnic';
        }
        else
         setState(() {
            formstate = formstate +1;
          });
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
  Widget City() {
    return TextFormField(
     
      controller: _city,
    	decoration: InputDecoration(
    hintText: userdetail[5], contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
      errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_city) {
 if (_city == "") {
          return 'Please fill this field';} 
        else
         setState(() {
            formstate = formstate +1;
          });
          return null; },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
  Widget Country() {
    return TextFormField(
      controller: _country,
    	decoration: InputDecoration(
    hintText: userdetail[6], contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0),
	  hintStyle: TextStyle(color: Colors.black),
	  filled: true,
	  fillColor: Colors.grey[80],
      errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_country) {
        if (_country == "") {
          return 'Please fill this field';
        } 
        else
         setState(() {
            formstate = formstate +1;
          });
          return null;
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
}
Widget _buildLoginBtn() {
    return Container(
          width:350,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 90.0,left: 90.0,top: 15.0,bottom: 15.0),child: Text(
        "Update",
        style: GoogleFonts.montserrat(fontSize: 20,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
       onPressed: (){
        
       },
    ),
                  );
  }