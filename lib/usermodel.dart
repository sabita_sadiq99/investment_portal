class User{
  String username;
  String lastname;
  String firstname;
  String email;
  String short_name;
  String timezone;
  String localeSid;
  String emailencoding;
  String language;
  String street;
  String city;
  String country;
  String state;
  String cnic;
  String phone;
  String password;
  String file;
  String blob;

  factory User.fromJson(Map<String, dynamic> json){
  return User(
    json['pUsername'],
    json['pLastName'],
    json['pFirstName'],
    json['pEmail'],
    json['pAlias'],
    json['pTimeZoneSidKey'],
    json['pLocaleSidKey'],
    json['pEmailEncodingKey'],
    json['pLanguageLocaleKey'],
    json['pStreet'],
    json['pCity'],
    json['pCountry'],
    json['pState'],
    json['pCNIC'],
    json['pPhone'],
    json['pPassword'],
    json['pFileName'],
    json['pblobString'],
  );
}
User.novalue();
Map<String,dynamic> toJson()=>{
    'pUsername':username,
    'pLastName':lastname,
    'pFirstName':firstname,
    'pEmail':email,
    'pAlias':short_name,
    'pTimeZoneSidKey':timezone,
    'pLocaleSidKey':localeSid,
    'pEmailEncodingKey':emailencoding,
    'pLanguageLocaleKey':language,
    'pStreet':street,
    'pCity':city,
    'pCountry':country,
    'pState':state,
    'pCNIC':cnic,
    'pPhone' : phone,
    'pPassword' : password,
    'pFileName':file,
    'pblobString':blob,

};
User(
  this.username,
  this.lastname,
  this.firstname,
  this.email,
  this.short_name,
  this.timezone,
  this.localeSid,
  this.emailencoding,
  this.language,
  this.street,
  this.city,
  this.country,
  this.state,
  this.cnic,
  this.phone,
  this.password,
  this.file,
  this.blob,
);
}