class Filter{
  String Breed;
  String Category;
  String sort;
 String PriceRange;

  factory Filter.fromJson(Map<String, dynamic> json){
    return Filter(
      json['p_Breed'],
      json['p_Category'],
      json['p_SortField'],
      json['p_PriceRange']
    );
  }
  Filter.novalue();
  Map<String,dynamic> toJson()=>{
    'p_Breed': Breed,
    'p_Category' : Category,
    'p_SortField' : sort,
    'p_PriceRange' : PriceRange
  };

  Filter(
    this.Breed,
    this.Category,
    this.sort,
    this.PriceRange,
  );
}
