import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:investment_portal_window/checkout.dart';
import './welcome.dart';
import './setting.dart';
import 'dart:ui';
import 'package:flutter/gestures.dart';
import 'package:flutter/services.dart';

class Term extends StatefulWidget{
  _TermState createState() => _TermState();
}
class _TermState extends State<Term>{
   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
    int _selectedIndex = 4;
     void _onItemTapped(int index) {
    if(index==0)
    {
       Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Welcome()),
  );
    }

   if(index == 4)
   {
      Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Setting()),
  );
   }
  }
  @override
  Widget build(BuildContext context) {
    // TODO: implement build
    return Scaffold(
    
      key: _scaffoldKey,
      backgroundColor: Colors.white,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
             
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 20.0,
                    vertical: 60.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.start,
                    children: <Widget>[
                      
                      Row(
                        children:[


                             IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: HexColor('#0071BC'),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
          Spacer(),
            Spacer(),
              Text(
                        'Terms And Condition',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                         
                          fontSize: 22.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      Spacer(),
                    Spacer(),
                    Spacer(),
                        ]
                      ),
                      SizedBox(height: 10.0,),
                      Align(
                        alignment:Alignment.topLeft+ Alignment(0.0,0.2),
                        child: Text(
                        'Agreement',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#4A4B4D'),
                         
                          fontSize: 22.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      ),
                       SizedBox(height: 20.0,),
                      Text("Please read the following terms and conditions \n('Terms and Conditions') carefully before using this\nmobile app. By accessing or using this Site, or any \nfunctions made available on, accessed through or in \nconnection with this app, agree to the following \nTerms and Conditions which shall constitute a \nlegally binding agreement between you and \nInvestment portal (collectively with its subsidiaries & \naffiliates, 'Investment portal', “we” or “our”). You \nshould review Terms and Conditions regularly as \nthey may change at any time at our sole discretion.",
                      style:GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                         
                          fontSize: 15.0,
                          fontWeight: FontWeight.normal,
                        ),),
                          SizedBox(height: 20.0,),
                          Align(
                            alignment:Alignment.centerLeft,
                            child: Text("If you do not agree to a term or condition set out \nin these Terms and Conditions, you should not \naccess or otherwise use this app.",
                      style:GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                         
                          fontSize: 15.0,
                          fontWeight: FontWeight.normal,
                        ),),
                          ),
                            SizedBox(height: 20.0,),
                            RichText(text: TextSpan(children: [WidgetSpan(
        child:
        Padding(padding: EdgeInsets.only(bottom:2.0),child:Icon(Icons.circle,size: 12.0,color: HexColor('#0071BC'),)
        ),),
      TextSpan(text: '  Customers agree to be bound by the terms \nand conditions that may be agreed between the \nBank and purchaser/acquirer on which a sale/transfer of Account and/or Services may be affected by the Bank.',style:GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                         
                          fontSize: 15.0,
                          fontWeight: FontWeight.normal,
                        )),
    ],
  ),
),
 SizedBox(height: 20.0,),
                            RichText(text: TextSpan(children: [WidgetSpan(
        child:
        Padding(padding: EdgeInsets.only(bottom:2.0),child:Icon(Icons.circle,size: 12.0,color: HexColor('#0071BC'),)
        ),),
      TextSpan(text: '  Customers agree to be bound by the terms and \nconditions that may be agreed between the Bank.',style:GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                         
                          fontSize: 15.0,
                          fontWeight: FontWeight.normal,
                        )),
    ],
  ),
),
SizedBox(height: 20.0,),
                            RichText(text: TextSpan(children: [WidgetSpan(
        child:
        Padding(padding: EdgeInsets.only(bottom:2.0),child:Icon(Icons.circle,size: 12.0,color: HexColor('#0071BC'),)
        ),),
      TextSpan(text: '  Customers agree to be bound by the terms and \nconditions that may be agreed between the Bank.',style:GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                         
                          fontSize: 15.0,
                          fontWeight: FontWeight.normal,
                        )),
    ],
  ),
),SizedBox(height: 20.0,),
Align(
                        alignment:Alignment.topLeft+ Alignment(0.0,0.2),
                        child: Text(
                        'Undertaking',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#4A4B4D'),
                         
                          fontSize: 22.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                      ),
                      SizedBox(height: 20.0,),
                      Text("I further confirm my understanding that compliance \nwith such instructions and provision of the Services \nshall be subject to the internal policies of the Bank.",
                      style:GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                         
                          fontSize: 15.0,
                          fontWeight: FontWeight.normal,
                        ),),
                        SizedBox(height: 10.0,),
                      Text("In consideration of your agreeing to send/provide \nme the electronic statements and Alerts via app,text \nmessage, voice mail, IVR or other electronic means,\nI hereby waive any requirement to send any paper \nor electronic statements or notices by post or \ncourier to my/our address.",
                      style:GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                         
                          fontSize: 15.0,
                          fontWeight: FontWeight.normal,
                        ),),
                          SizedBox(height:20.0),
                     Row(children: [
                      RichText(text: TextSpan(
                        text:'Not right now..',style: TextStyle(color:Colors.black),
                      recognizer: TapGestureRecognizer()
                    ..onTap = () {
                  
                    } 
                      )),
                       Spacer(),
                       _buildLoginBtn(),

                     ],)
    ])))]))),
     bottomNavigationBar:BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag_outlined),
            label: 'My Orders',
          ),
            BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
            
          ),
       
          
        ],
        iconSize: 30.0,
        currentIndex: _selectedIndex,
        selectedItemColor: HexColor('#0071BC'),
        unselectedItemColor: HexColor('#7C7D7E'),
        unselectedLabelStyle: GoogleFonts.montserrat(color:Colors.black),
        onTap: _onItemTapped,
      ),
    );
  }
    _onAlertButtonsPressed(context) {
    AlertDialog(
  title: Text('Reset settings?'),
  content: Text('This will reset your device to its default factory settings.'),
  actions: [
    FlatButton(
      textColor: Color(0xFF6200EE),
      onPressed: () {},
      child: Text('CANCEL'),
    ),
    FlatButton(
      textColor: Color(0xFF6200EE),
      onPressed: () {},
      child: Text('ACCEPT'),
    ),
  ],
);
  }

  Widget _buildLoginBtn() {
    return Container(
          width:200,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 20.0,left: 20.0,top: 15.0,bottom: 15.0),child: Text(
        "I Agree",
        style: GoogleFonts.montserrat(fontSize: 20,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
    onPressed: () {Navigator.push(context,MaterialPageRoute(builder: (context) => Checkout(result: 1,)),);}
    ),
                  );
  }
 
}
