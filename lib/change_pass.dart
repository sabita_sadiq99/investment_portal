import 'dart:ui';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:investment_portal_window/passwordmodel.dart';
import 'package:validators/validators.dart';
import './setting.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import './welcome.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
import 'account_menu.dart';
import 'api.dart';
import 'dart:convert';
class ChangePass extends StatefulWidget{
  _ChangePassState createState() => _ChangePassState();
}
class _ChangePassState extends State<ChangePass>{
final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
 final _formKey = GlobalKey<FormState>();
final _current = TextEditingController();
final _new = TextEditingController();
final _confirm = TextEditingController();
String email = 'testportal@test.com.qa';

  void _onItemTapped(int index) {
    if(index==0)
    {
       Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Welcome()),
  );}
   if(index == 4)
   {
      Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Setting()),
  );}
  }
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
    drawer: Drawer(
        child: Container(
          color: HexColor('#1b2f35'),
          width: double.infinity,
          height: double.infinity,
          child: ListView(
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: HexColor('#1b2f35'),
              ),
              child: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft+ Alignment(0.2,0),
                    child: new Image.asset(
              'logo-white.png',
              width: 100.0,
              height: 100.0,
              fit: BoxFit.contain,
            ),
            
                  ),
                 
                  Align(
                    alignment: Alignment.bottomLeft + Alignment(0.2,1.5),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage('https://avatars0.githubusercontent.com/u/8264639?s=460&v=4'),
                      radius: 30.0,
                    ),
                  ),
                  Align(
                    alignment: Alignment(0.4, 1.5),
                    child: Text(
                      'Alec Reynolds',
                      style: TextStyle(color: Colors.white, fontSize: 20.0),
                    ),
                  ),
                  SizedBox(height: 2.0,),
                 Align(
                    alignment: Alignment(0.5, 1.8),
                    child: Text(
                      'johndoe@gmail.com',
                      style: TextStyle(color: Colors.white, fontSize: 15.0),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height:50.0),
             new Divider(
            color: Colors.white,
            thickness: 0.8,
          ),
          ListTile(
                leading: Icon(Icons.home,color: Colors.white,),
                title: Text('Home',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
               ListTile(
                leading: Icon(Icons.shopping_bag,color: Colors.white,),
                title: Text('My Orders',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
              ListTile(
                leading: Icon(Icons.account_balance_wallet,color: Colors.white,),
                title: Text('My Wallet',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
              ListTile(
                leading: Icon(Icons.settings,color: Colors.white,),
                title: Text('Account Settings',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: () {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Setting()),
  );
},
              ),
               ListTile(
                leading: Icon(Icons.power_settings_new_rounded,color: Colors.white,),
                title: Text('Sign Out',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
          ],
          
        ),
        ),
      ),
      backgroundColor: Colors.white,
      body: AnnotatedRegion<SystemUiOverlayStyle>(
        
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
             
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 20.0,
                    vertical: 60.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      
                      Row(
                        children:[
                          Padding(padding: EdgeInsets.only(left:0.0),child:
                                 IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: HexColor('#0071BC'),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),
                          ),
                          
                    
                               CircularProfileAvatar(
          'https://avatars0.githubusercontent.com/u/8264639?s=460&v=4', //sets image path, it should be a URL string. default value is empty string, if path is empty it will display only initials
          radius: 20, // sets radius, default 50.0              
          backgroundColor: Colors.transparent, // sets background color, default Colors.white
          // borderWidth: 10,  // sets border, default 0.0

          // borderColor: Colors.brown, // sets border color, default Colors.white
          elevation: 5.0, // sets elevation (shadow of the profile picture), default value is 0.0
          // foregroundColor: Colors.brown.withOpacity(0.5), //sets foreground colour, it works if showInitialTextAbovePicture = true , default Colors.transparent
          cacheImage: true, // allow widget to cache image against provided url
          onTap: () => _scaffoldKey.currentState.openDrawer(),
          showInitialTextAbovePicture: true, // setting it true will show initials text above profile picture, default false  
          ),
          Spacer(),
         
          Column(
            children:[
                     Text(
                        'Change',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                         
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),
                         Text(
                        'Password',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                         
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ), ),] ),
                      Spacer(),
                      Spacer(),
                        ]
                      ),
                      Form(
                          key:_formKey,
                          child:Column(children:[
                               SizedBox(height:20.0),
                      CurrentInput(),
                      SizedBox(height:10.0),
                      NewPass(),
                       SizedBox(height:10.0),
                      ConfirmPass(),
                      SizedBox(height:20.0),
                      message(),
                       SizedBox(height:20.0),
                      _changeBTN(),
                          ] )
      )])))]))),
                  bottomNavigationBar:BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag_outlined),
            label: 'My Orders',
          ),
            BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings', ),],
        iconSize: 30.0,
        currentIndex: 0,
        selectedItemColor: HexColor('#0071BC'),
        unselectedItemColor: HexColor('#7C7D7E'),
        unselectedLabelStyle: TextStyle(color:Colors.black),
        onTap: _onItemTapped,
      ),
                 );
}
   Widget CurrentInput() {
    return TextFormField(
      controller: _current,
      obscureText: true,
    	decoration: InputDecoration(
    hintText: 'Current Password', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0), 
	  hintStyle: TextStyle(color: HexColor('#7C7D7E')),
	  filled: true,
	  fillColor: Colors.grey[80],
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
     
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
   Widget NewPass() {
    return TextFormField(
      controller: _new,
      obscureText: true,
    	decoration: InputDecoration(
    hintText: 'New Password', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0), 
	  hintStyle: TextStyle(color: HexColor('#7C7D7E')),
	  filled: true,
	  fillColor: Colors.grey[80],
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_new){
          if(_new.length <8 || isAlpha(_new) || isNumeric(_new))
          {
              return "Invalid Password";
          }
          else
          {
            return null;
          }
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
     Widget ConfirmPass() {
    return TextFormField(
      controller: _confirm,
      obscureText: true,
    	decoration: InputDecoration(
    hintText: 'Re-Type New Password', contentPadding: const EdgeInsets.only(left:30.0,bottom: 35.0,top: 15.0), 
	  hintStyle: TextStyle(color: HexColor('#7C7D7E')),
	  filled: true,
	  fillColor: Colors.grey[80],
    errorBorder:OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.red, width: 2),
	   ), 
	  enabledBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white, width: 2),
	   ),
	  focusedBorder: OutlineInputBorder(
		borderRadius: BorderRadius.all(Radius.circular(50.0)),
		borderSide: BorderSide(color: Colors.white),
	  ),
	),
      keyboardType: TextInputType.text,
      validator: (_confirm){
          if(_confirm.length ==0)
          {return "Please re-enter password";}    
          else if(_confirm != _new.text)
          { return "Passwords doesnot match"; }
          else
          {return null; }
      },
      onEditingComplete: () => FocusScope.of(context).nextFocus(),
    );
  }
  Widget message()
  {
    return Container(
      width:300,
      child:Column(children: [
        Padding(padding: EdgeInsets.all(10.0),
        child:Text("For security reasons, your password needs at least 8 characters, consisting of:\n• upper and lower case letters\n• numbers",style: GoogleFonts.montserrat(fontSize: 14,fontWeight: FontWeight.w400,color: HexColor('#7C7D7E') )
        ) ),],),
     decoration: BoxDecoration(
       border:Border.all(
       color:Colors.white,
     ),
     borderRadius: BorderRadius.circular(8.0),
        color: Colors.white,
        boxShadow: [
           BoxShadow(
                    color: Colors.grey.shade400,
                    offset: Offset(0.0, 1.0), //(x,y)
                    blurRadius: 6.0,
                  ),
        ],),);}
    Widget _changeBTN() {
    return Container(
          width:380,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 50.0,left: 50.0,top: 16.0,bottom: 16.0),child: Text(
        "Change Password",
        style: GoogleFonts.montserrat(fontSize: 20,fontWeight: FontWeight.w600 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          ) ) ),
        onPressed: () {
          FocusManager.instance.primaryFocus.unfocus();
          validateInputs();}),);}
  validateInputs()
  {
    if (_formKey.currentState.validate()) {
       _formKey.currentState.save();
       PassUpdate obj = new PassUpdate.novalue();
       obj.username = email;
       obj.newpass = base64.encode(utf8.encode(_new.text));
       API.Api.update_password(obj, session_id).then((value) {
        if(value.statusCode == 200)
        {
          _onBasicAlertPressed();
        }}); }}
  _onBasicAlertPressed() {
    Alert(
      context: context,
      type: AlertType.success,
      title: "Password Update Successfully",
      style: AlertStyle(
          titleStyle: GoogleFonts.montserrat(
              color: HexColor('#4A4B4D'),
              fontSize: 18,
              fontWeight: FontWeight.w700),
          descStyle: GoogleFonts.montserrat(
              color: HexColor('#4A4B4D'),
              fontSize: 17,
              fontWeight: FontWeight.w400)),
     
      buttons: [
        DialogButton(
            child: Text(
              "Cancel",
              style: GoogleFonts.montserrat(
                  color: HexColor('#4A4B4D'),
                  fontSize: 18,
                  fontWeight: FontWeight.w700),
            ),
            onPressed: () => Navigator.pop(context),
            color: Colors.white,
            border: Border.fromBorderSide(BorderSide(
                color: Colors.black, width: 0.8, style: BorderStyle.solid)),
            radius: const BorderRadius.all(Radius.circular(50))),
      ],
    ).show();
  } 
}

  
