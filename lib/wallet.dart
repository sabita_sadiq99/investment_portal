import 'dart:ui';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import './setting.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import './welcome.dart';
import './terms.dart';
import './myorders.dart';

class Wallet extends StatefulWidget{
  _WalletState createState() => _WalletState();
}
class _WalletState extends State<Wallet>{
  final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
  
       void _onItemTapped(int index) {
    if (index == 0) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Welcome()),
      );
    }
    if (index == 2) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Orders()),
      );
    }

    if (index == 4) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Setting()),
      );
    }
  }
 @override
 Widget build(BuildContext context) {
    return Scaffold(
       key: _scaffoldKey,
    drawer: Drawer(
        child: Container(
          color: HexColor('#1b2f35'),
          width: double.infinity,
          height: double.infinity,
          child: ListView(
          children: <Widget>[
            DrawerHeader(
              decoration: BoxDecoration(
                color: HexColor('#1b2f35'),
              ),
              child: Stack(
                children: <Widget>[
                  Align(
                    alignment: Alignment.centerLeft+ Alignment(0.2,0),
                    child: new Image.asset(
              'logo-white.png',
              width: 100.0,
              height: 100.0,
              fit: BoxFit.contain,
            ),
            
                  ),
                 
                  Align(
                    alignment: Alignment.bottomLeft + Alignment(0.2,1.5),
                    child: CircleAvatar(
                      backgroundImage: NetworkImage('https://avatars0.githubusercontent.com/u/8264639?s=460&v=4'),
                      radius: 30.0,
                    ),
                  ),
                  Align(
                    alignment: Alignment(0.4, 1.5),
                    child: Text(
                      'Alec Reynolds',
                      style: TextStyle(color: Colors.white, fontSize: 20.0),
                    ),
                  ),
                  SizedBox(height: 2.0,),
                 Align(
                    alignment: Alignment(0.5, 1.8),
                    child: Text(
                      'johndoe@gmail.com',
                      style: TextStyle(color: Colors.white, fontSize: 15.0),
                    ),
                  ),
                ],
              ),
            ),
            SizedBox(height:50.0),
             new Divider(
            color: Colors.white,
            thickness: 0.8,
          ),
          ListTile(
                leading: Icon(Icons.home,color: Colors.white,),
                title: Text('Home',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
               ListTile(
                leading: Icon(Icons.shopping_bag,color: Colors.white,),
                title: Text('My Orders',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
              ListTile(
                leading: Icon(Icons.account_balance_wallet,color: Colors.white,),
                title: Text('My Wallet',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
              ListTile(
                leading: Icon(Icons.settings,color: Colors.white,),
                title: Text('Account Settings',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: () {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => Setting()),
  );
},
              ),
               ListTile(
                leading: Icon(Icons.power_settings_new_rounded,color: Colors.white,),
                title: Text('Sign Out',style: TextStyle(color:Colors.white),),
               contentPadding: EdgeInsets.only(left:50.0),
                onTap: null,
              ),
          ],
          
        ),
        ),
      ),
       bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag_outlined),
            label: 'My Orders',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
          ),
        ],
        iconSize: 30.0,
      selectedItemColor:HexColor('#7C7D7E'),
      showSelectedLabels: false,
      showUnselectedLabels: false,
        unselectedItemColor: HexColor('#7C7D7E'),
        unselectedLabelStyle: TextStyle(color: Colors.black),
        onTap: _onItemTapped,
      ),
      backgroundColor: Colors.white,
            body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[

              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 14.0,
                    vertical: 70.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children:[
                        Padding(padding: EdgeInsets.only(left:20.0,right:35.0),child:CircularProfileAvatar(
          'https://avatars0.githubusercontent.com/u/8264639?s=460&v=4', 
          radius: 20,
          backgroundColor: Colors.transparent, 
          elevation: 5.0, 
          cacheImage: true, 
          onTap: () => _scaffoldKey.currentState.openDrawer(),
          showInitialTextAbovePicture: true, 
          ) ,),
          Spacer(),
         
          Column(
            children:[
                     Text(
                        'My Wallet',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),

                      ),
                        ]),
                      Spacer(),
                      Spacer(),
                    Spacer(),
                      ]
                      ),
                      SizedBox(height: 50.0,),
                      Row(children: [
                        Text('Cash Available',style: GoogleFonts.montserrat(
                                                      color:HexColor('#00A14B'),
                                                      fontSize: 18.0)),
                                                      Spacer(),
                                                   Padding(padding: EdgeInsets.only(bottom:7.0),child: Text('Rs',style:GoogleFonts.montserrat(color: HexColor('#00A14B'),fontSize: 18.0,fontWeight: FontWeight.w700),),),
                                                   Text(' 95,000',style:GoogleFonts.montserrat(color: HexColor('#00A14B'),fontSize: 23.0,fontWeight: FontWeight.w700)),
                                                   Padding(padding: EdgeInsets.only(bottom:1.0,top:2.0),child: Text(' PKR',style:GoogleFonts.montserrat(color: HexColor('#00A14B'),fontSize: 14.0,),),),],),
                      Row(children: [ Expanded(child: new Container(child: Divider(color: HexColor('#00A14B'),thickness: 1.0,)),),],),
                      SizedBox(height: 10.0,),
                       Row(children: [
                        Text('Total Amount Invested',style: GoogleFonts.montserrat(
                                                      color:HexColor('#00A14B'),
                                                      fontSize: 18.0)),
                                                      Spacer(),
                                                   Padding(padding: EdgeInsets.only(bottom:7.0),child: Text('Rs',style:GoogleFonts.montserrat(color: HexColor('#00A14B'),fontSize: 18.0,fontWeight: FontWeight.w700),),),
                                                   Text(' 95,000',style:GoogleFonts.montserrat(color: HexColor('#00A14B'),fontSize: 23.0,fontWeight: FontWeight.w700)),
                                                   Padding(padding: EdgeInsets.only(bottom:1.0,top:2.0),child: Text(' PKR',style:GoogleFonts.montserrat(color: HexColor('#00A14B'),fontSize: 14.0,),),),],),
                      Row(children: [ Expanded(child: new Container(child: Divider(color: HexColor('#00A14B'),thickness: 1.0,)),),],),
                      SizedBox(height:30.0),
                      Container(  
    width: double.infinity,  
    height: 120.0,  
    padding: EdgeInsets.only(bottom:20.0),
    color: HexColor('#00A14B'),   
    child: Column(children: [
      Row(children: [
        Spacer(),
        Padding(padding: EdgeInsets.only(top:10.0),child: Text('Profit Gained',style: GoogleFonts.montserrat(color:Colors.white,fontSize: 22.0,fontWeight: FontWeight.w300,),textAlign: TextAlign.center,),),
        Spacer(),
        ],),   
        Row(children: [ Expanded(child: new Container(child: Divider(color:Colors.white,thickness: 1.0,)),),],),
        SizedBox(height:10.0),
        Row(children: [
          Spacer(),
              Padding(padding: EdgeInsets.only(bottom:7.0),child: Text('Rs',style:GoogleFonts.montserrat(color: Colors.white,fontSize: 18.0,fontWeight: FontWeight.w700),),),
              Text(' 95,000',style:GoogleFonts.montserrat(color: Colors.white,fontSize: 25.0,fontWeight: FontWeight.w700)),
              Padding(padding: EdgeInsets.only(bottom:0.0,top:5.0),child: Text('   PKR',style:GoogleFonts.montserrat(color: Colors.white,fontSize: 14.0,),),),
              Spacer(), ]), ],),  ),
              SizedBox(height: 20.0,),
              Row(children: [Text('Withdraw Amount',style: GoogleFonts.montserrat(
                color:HexColor('#00A14B'),
                fontSize: 18.0)),
                Spacer(),
                Padding(padding: EdgeInsets.only(bottom:7.0),child: Text('Rs',style:GoogleFonts.montserrat(color: HexColor('#00A14B'),fontSize: 18.0,fontWeight: FontWeight.w700),),),
                Text(' 0',style:GoogleFonts.montserrat(color: HexColor('#00A14B'),fontSize: 23.0,fontWeight: FontWeight.w700)),
                Padding(padding: EdgeInsets.only(bottom:1.0,top:2.0),child: Text(' PKR',style:GoogleFonts.montserrat(color: HexColor('#00A14B'),fontSize: 14.0,),),),],),
                SizedBox(height:30.0),
                Text('Transfer you amount to your bank',style: GoogleFonts.montserrat(
                color:Colors.black,
                fontSize: 18.0,fontWeight: FontWeight.w300)),
                SizedBox(height:20.0),
                Align(
                  alignment:Alignment(-0.98,5.0),
                  child: Text('Select Method',style: GoogleFonts.montserrat(
                color:Colors.black,
                fontSize: 18.0,fontWeight: FontWeight.w300)),
                ),
                SizedBox(height: 20.0),
                        Container(  
                              width: double.infinity,  
                              height: 120.0,  
                            
                              padding: EdgeInsets.only(bottom:20.0),
                              color: Colors.grey[100],   
                              child: Wrap(children: [
                                            Container(
                                              margin: EdgeInsets.only(top:30.0,left:10.0),
                                              width: 100.0,
                                              height: 60.0,
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    fit: BoxFit.cover,
                                                    image: AssetImage('assets/jazzcash.png')),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8.0)),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top:10.0,left:4.0),
                                                height: 90,
                                                child: VerticalDivider(
                                                    color: Colors.black)),
                                                    Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Padding(padding: EdgeInsets.only(top:30.0),child: Text('Jazzcash',style: GoogleFonts.montserrat(color:Colors.black,fontSize: 18.0,fontWeight: FontWeight.w500)),),
                                                          Text('Transfer to Jazzcash wallet',style: GoogleFonts.montserrat(color:Colors.black, fontSize: 15.0,fontWeight: FontWeight.w300)),
                                                    ],),
                                                    Padding(padding: EdgeInsets.only(top:50.0,left:10.0),child:InkWell( child: Icon( Icons.arrow_forward_ios, size: 20.0, color: HexColor('#00A14B'), ),
                                              onTap: () {

                                              },),), 
                                              ]),  ), 
                                              SizedBox(height:5.0),
                                                  Container(  
                              width: double.infinity,  
                              height: 120.0,  
                            
                              padding: EdgeInsets.only(bottom:20.0),
                              color: Colors.grey[100],   
                              child: Wrap(children: [
                                            Container(
                                              margin: EdgeInsets.only(top:30.0,left:10.0),
                                              width: 100.0,
                                              height: 60.0,
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    fit: BoxFit.cover,
                                                    image: AssetImage('assets/easypaisa.png')),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8.0)),
                                              ),
                                            ),
                                            Container(
                                              margin: EdgeInsets.only(top:10.0,left:4.0),
                                                height: 90,
                                                child: VerticalDivider(
                                                    color: Colors.black)),
                                                    Column(
                                                      crossAxisAlignment: CrossAxisAlignment.start,
                                                      children: [
                                                        Padding(padding: EdgeInsets.only(top:30.0),child: Text('Easypaisa',style: GoogleFonts.montserrat(color:Colors.black,fontSize: 18.0,fontWeight: FontWeight.w500)),),
                                                          Text('Transfer to easypaisa wallet.',style: GoogleFonts.montserrat(color:Colors.black, fontSize: 15.0,fontWeight: FontWeight.w300)),
                                                    ],),
                                                    Padding(padding: EdgeInsets.only(top:50.0,left:0.0),child:InkWell( child: Icon( Icons.arrow_forward_ios, size: 20.0, color: HexColor('#00A14B'), ),
                                              onTap: () {

                                              },),), 
                                              ]),  ),
                                               ])))]))));
                  }}
