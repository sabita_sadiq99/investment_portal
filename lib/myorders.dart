import 'dart:ui';
import 'package:circular_profile_avatar/circular_profile_avatar.dart';
import 'package:flutter/gestures.dart';
import './setting.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:hexcolor/hexcolor.dart';
import 'package:google_fonts/google_fonts.dart';
import './welcome.dart';
import './transaction.dart';
import 'favourites.dart';
import 'wallet.dart';
import './userimage.dart';
import 'package:investment_portal_window/sign_in.dart';
import 'package:investment_portal_window/account_menu.dart';
import 'package:rflutter_alert/rflutter_alert.dart';
class Orders extends StatefulWidget{
   _OrdersState createState() => _OrdersState();
}
class _OrdersState extends State<Orders>{
   final GlobalKey<ScaffoldState> _scaffoldKey = new GlobalKey<ScaffoldState>();
   int _selectedIndex = 2;
       void _onItemTapped(int index) {
    if (index == 0) {
      Navigator.push(
        context,
        MaterialPageRoute(builder: (context) => Welcome()),
      );
    }
    if (index == 1) {Navigator.push( context,MaterialPageRoute(builder: (context) => Favourites()),);
    }
    if (index == 2) {Navigator.push(context,MaterialPageRoute(builder: (context) => Orders()),);
    }
    if (index == 4) { Navigator.push(context,MaterialPageRoute(builder: (context) => Setting()),);
    }
  }
 @override
 Widget build(BuildContext context) {
    return Scaffold(
       key: _scaffoldKey,
    drawer:Drawer(
        child: Container(
          //child: Your widget,
          color: HexColor('#1b2f35'),
          width: double.infinity,
          height: double.infinity,
          child: ListView(
            children: <Widget>[
              DrawerHeader(
                decoration: BoxDecoration(
                  color: HexColor('#1b2f35'),
                ),
                child: Stack(
                  children: <Widget>[
                    Align(
                      alignment: Alignment.centerLeft + Alignment(0.2, 0),
                      child: new Image.asset(
                        'logo-white.png',
                        width: 100.0,
                        height: 100.0,
                        fit: BoxFit.contain,
                      ),
                    ),
                    Align(
                      alignment: Alignment.bottomLeft + Alignment(0.2, 1.5),
                      child: CircleAvatar(
                        backgroundImage: NetworkImage(
                           get_image()),
                        radius: 30.0,
                      ),
                    ),
                    Align(
                      alignment: Alignment(0.3, 1.5),
                      child: Text(
                        get_name(),
                        style: TextStyle(color: Colors.white, fontSize: 20.0),
                      ),
                    ),
                    SizedBox(
                      height: 2.0,
                    ),
                    Align(
                      alignment: Alignment(0.6, 1.8),
                      child: Text(
                        get_email(),
                        style: TextStyle(color: Colors.white, fontSize: 15.0),
                      ),
                    ),
                  ],
                ),
              ),
              SizedBox(height: 50.0),
              new Divider(
                color: Colors.white,
                thickness: 0.8,
              ),
              ListTile(
                leading: Icon(
                  Icons.home,
                  color: Colors.white,
                ),
                title: Text(
                  'Home',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: null,
              ),
              ListTile(
                leading: Icon(
                  Icons.shopping_bag,
                  color: Colors.white,
                ),
                title: Text(
                  'My Orders',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: (){
                   Navigator.push(context,MaterialPageRoute(builder: (context) => Orders()));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.account_balance_wallet,
                  color: Colors.white,
                ),
                title: Text(
                  'My Wallet',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
               onTap: (){
                   Navigator.push(context,MaterialPageRoute(builder: (context) => Wallet()));
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.settings,
                  color: Colors.white,
                ),
                title: Text(
                  'Account Settings',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: () {
                  Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => Setting()),
                  );
                },
              ),
              ListTile(
                leading: Icon(
                  Icons.power_settings_new_rounded,
                  color: Colors.white,
                ),
                title: Text(
                  'Sign Out',
                  style: TextStyle(color: Colors.white),
                ),
                contentPadding: EdgeInsets.only(left: 50.0),
                onTap: () => _onBasicAlertPressed(context),
              ),
            ],
          ),
        ),
      ),
       bottomNavigationBar: BottomNavigationBar(
        items: const <BottomNavigationBarItem>[
          BottomNavigationBarItem(
            icon: Icon(Icons.home),
            label: 'Home',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.favorite),
            label: 'Favourites',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.shopping_bag_outlined),
            label: 'My Orders',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.notifications),
            label: 'Notifications',
          ),
          BottomNavigationBarItem(
            icon: Icon(Icons.settings),
            label: 'Settings',
          ),
        ],
        iconSize: 30.0,
      selectedItemColor: HexColor('#0071BC'),
         currentIndex: _selectedIndex,
        unselectedItemColor: HexColor('#7C7D7E'),
        unselectedLabelStyle: TextStyle(color: Colors.black),
        onTap: _onItemTapped,
      ),
      backgroundColor: Colors.white,
            body: AnnotatedRegion<SystemUiOverlayStyle>(
        value: SystemUiOverlayStyle.light,
        child: GestureDetector(
          onTap: () => FocusScope.of(context).unfocus(),
          child: Stack(
            children: <Widget>[
              Container(
                height: double.infinity,
                child: SingleChildScrollView(
                  physics: AlwaysScrollableScrollPhysics(),
                  padding: EdgeInsets.symmetric(
                    horizontal: 20.0,
                    vertical: 60.0,
                  ),
                  child: Column(
                    mainAxisAlignment: MainAxisAlignment.center,
                    children: <Widget>[
                      Row(
                        children:[
                          Padding(padding: EdgeInsets.only(left:0.0),child:
                   IconButton(
                  icon: Icon(
                    Icons.arrow_back_ios,
                    color: HexColor('#0071BC'),
                  ),
                  onPressed: () {
                    Navigator.pop(context);
                  },
                ),),
         CircularProfileAvatar(
         get_image(), 
          radius: 25,
          backgroundColor: Colors.transparent, 
          elevation: 5.0, 
          cacheImage: true, 
          onTap: () => _scaffoldKey.currentState.openDrawer(),
          showInitialTextAbovePicture: true, 
          ),
          Spacer(),
          Column(
            children:[
                     Text(
                        'My Orders',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#0071BC'),
                          fontSize: 25.0,
                          fontWeight: FontWeight.bold,
                        ),
                      ),]),
                     Spacer(),
                      Spacer(),
                      Spacer(),
                      ]
                      ),
                        SizedBox(height: 10.0),
                      Text(
                        'See the list of assets you have invested in',
                        style: GoogleFonts.montserrat(
                          color: HexColor('#7C7D7E'),
                          fontSize: 15.0,
                          letterSpacing: 0.1,
                          fontWeight: FontWeight.w400,
                        ),
                        textAlign: TextAlign.center,
                      ),
                      SizedBox(height: 20.0,),
                               Container(
                        height: 45,
                        width: MediaQuery.of(context).size.width / 1.1,
                        child: Row(
                          children: [
                            Expanded(
                              child: new Theme(
                                data: new ThemeData(
                                  primaryColor: HexColor('#0071BC'),
                                  primaryColorDark: HexColor('#0071BC'),
                                ),
                                child: TextField(
                                  decoration: InputDecoration(
                                    hintText: 'Search Here',
                                    hintStyle: GoogleFonts.montserrat(
                                      color: HexColor('#7C7D7E'),
                                      fontSize: 14.0,
                                      letterSpacing: 0.1,
                                      fontWeight: FontWeight.w300,
                                    ),
                                    contentPadding:
                                        EdgeInsets.only(top: 5.0, left: 25.0),
                                    enabledBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(50.0)),
                                      borderSide: BorderSide(
                                        color: Colors.transparent
                                      ),
                                    ),
                                    focusedBorder: OutlineInputBorder(
                                      borderRadius: BorderRadius.all(
                                          Radius.circular(50.0)),
                                      borderSide: BorderSide(
                                        color: HexColor('#0071BC'),
                                      ),
                                    ),
                                    fillColor: Colors.grey[100],
                                    filled: true,
                                       suffixIcon: Material(
                            elevation: 5.0,
                            color:  HexColor('#0071BC'),
                            borderRadius: BorderRadius.only(
                              topRight: Radius.circular(30.0),
                              bottomRight: Radius.circular(30.0),
                            ),
                            child: Icon(Icons.search, color: Colors.white),
                          ),),),),),
                            SizedBox(width: 10),
                          ],),),
                      Row(
                        children: [
                          Spacer(),
                          Padding(padding: EdgeInsets.only(top: 20.0,right:13.0),child:    
                          RichText(text:  TextSpan(
                                  text:'View Transaction History',
                                   recognizer: TapGestureRecognizer()
                    ..onTap = () {
                      Navigator.of(context).push(MaterialPageRoute(builder: (context,) {
                   return Transaction();},),);
                    },style: TextStyle(fontSize:15.0,color:HexColor('#0071BC'),
                                )
                              )) ,) 
                        ],
                      ),Container(
                              child: ListView.builder(
                              scrollDirection: Axis.vertical,
                              shrinkWrap: true,
                              itemExtent: 164.0,
                              padding: EdgeInsets.only(top:0.0,bottom: 0.0),
                              itemCount:2,
                              itemBuilder: (BuildContext context, int index) {
                                return Container( decoration: new BoxDecoration(
                                  border: Border.all(color: Colors.transparent),
                                    ),
                                    margin: EdgeInsets.only(bottom:0.0),
                                    child: Card(
                                    child: Column(
                                      children: [
                                        Container(
                                               decoration:
                                            BoxDecoration(color: Colors.white,   
                                                ),
                                        child: ListTile(
                                         horizontalTitleGap: 5.0,
                                          leading: Wrap(children: [
                                            Container(
                                              width: 80.0,
                                              height: 90.0,
                                              decoration: BoxDecoration(
                                                image: DecorationImage(
                                                    fit: BoxFit.cover,
                                                    image: NetworkImage('https://avatars0.githubusercontent.com/u/8264639?s=460&v=4')),
                                                borderRadius: BorderRadius.all(
                                                    Radius.circular(8.0)),),
                                            ),
                                            Container(
                                                height: 85,
                                                child: VerticalDivider(
                                                    color: Colors.black,
                                                    thickness: 0.8,)),
                                          ]),
                                          
                                          title: Padding(
                                            padding: EdgeInsets.only(
                                                top: 5.0, left: 0.0),
                                            child: Text(
                                              'COW',
                                              style: GoogleFonts.montserrat(
                                                color: HexColor('#4A4B4D'),
                                                fontSize: 20.0,
                                                letterSpacing: 0.1,
                                                fontWeight: FontWeight.w600,
                                              ),
                                            ),
                                          ),
                                          subtitle: Column(
                                            crossAxisAlignment:
                                                CrossAxisAlignment.start,
                                            children: [
                                              SizedBox(height: 10.0),
                                              RichText(
                                                text: TextSpan(
                                                  text: 'Price',
                                                  style: GoogleFonts.montserrat(
                                                      color: Colors.black,
                                                      fontSize: 15.0),
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                        text: '    Rs 50,000',
                                                        style: TextStyle(
                                                            color: HexColor(
                                                                '#7C7D7E'),
                                                            fontSize: 15.0))
                                                  ],
                                                ),
                                              ),
                                              RichText(
                                                text: TextSpan(
                                                  text: 'Mts',
                                                  style: GoogleFonts.montserrat(
                                                      color: Colors.black,
                                                      fontSize: 15.0),
                                                  children: <TextSpan>[
                                                    TextSpan(
                                                       text: '       6 months',
                                                        style: TextStyle(
                                                            color: HexColor(
                                                                '#7C7D7E'),
                                                            fontSize: 15.0))
                                                  ],
                                                ),
                                              ),
                                              SizedBox(height:0.0),
                                              Row(children: [
                                                Spacer(),
                                                Text('ID: 42198726',textAlign: TextAlign.right,style: GoogleFonts.montserrat(
                                                      color: Colors.black,
                                                      fontSize: 15.0),)
                                              ],)
                                           
                                            ], ),),),SizedBox(height:5.0), _buildLoginBtn() ], ),),); },)),])))]))) );}
                                              _onBasicAlertPressed(context) {
                                                    Alert(
                                                      context: context,
                                                      title: "Are you sure you want to logout?",
                                                      style: AlertStyle(
                                                          titleStyle: GoogleFonts.montserrat(
                                                              color: HexColor('#4A4B4D'),
                                                              fontSize: 18,
                                                              fontWeight: FontWeight.w700),
                                                          descStyle: GoogleFonts.montserrat(
                                                              color: HexColor('#4A4B4D'),
                                                              fontSize: 17,
                                                              fontWeight: FontWeight.w400)),
                                                      desc: "We can't notify you of new stocks if you do.",
                                                      buttons: [
                                                        DialogButton(
                                                            child: Text(
                                                              "Cancel",
                                                              style: GoogleFonts.montserrat(
                                                                  color: HexColor('#4A4B4D'),
                                                                  fontSize: 18,
                                                                  fontWeight: FontWeight.w700),
                                                            ),
                                                            onPressed: () => Navigator.pop(context),
                                                            color: Colors.white,
                                                            border: Border.fromBorderSide(BorderSide(
                                                                color: Colors.black, width: 0.8, style: BorderStyle.solid)),
                                                            radius: const BorderRadius.all(Radius.circular(50))),
                                                        DialogButton(
                                                            child: Text(
                                                              "Logout",
                                                              style: GoogleFonts.montserrat(
                                                                  color: Colors.white,
                                                                  fontSize: 18,
                                                                  fontWeight: FontWeight.w700),
                                                            ),
                                                            onPressed: () =>{
                                                            if(get_login_method() == 1)
                                                            {
                                                              signOutGoogle(),
                                                              session_id = null,
                                                            }
                                                            else if(get_login_method() == 2)
                                                            {

                                                            },
                                                          
                                                            Navigator.push(context, MaterialPageRoute(builder: (context) => Account_select()),),
                                                            },
                                                            color: HexColor('#00A14B'),
                                                            border: Border.fromBorderSide(BorderSide(
                                                                color: HexColor('#00A14B'),
                                                                width: 2,
                                                                style: BorderStyle.solid)),
                                                            radius: const BorderRadius.all(Radius.circular(50))),
                                                      ],
                                                    ).show();}}
   Widget _buildLoginBtn() {
    return Container(
          width:400,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 10.0,left: 10.0,top: 0.0,bottom: 0.0),child: Text(
        "Successfully Invested",
        style: GoogleFonts.montserrat(fontSize: 15,fontWeight: FontWeight.w500 )
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(0),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
      onPressed: null,
//         onPressed: () {
//   Navigator.push(
//     context,
//     MaterialPageRoute(builder: (context) => LoginScreen()),
//   );
// }
    ),
                  );
  }