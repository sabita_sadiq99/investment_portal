import 'dart:convert';

import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:hexcolor/hexcolor.dart';
import './login.dart';
import './create.dart';
import 'api.dart';
String session_id = "";
class Account_select extends StatefulWidget{
 _Account_selectState createState() => _Account_selectState();
}
 class  _Account_selectState extends State<Account_select>{
 @override
   void initState() {
    API.Api.APIuser().then((value) {
      var data = json.decode(value.body);
      setState(() {
        session_id = data['access_token'];
        print(session_id);
      });
    });
    super.initState();
  }
 Widget build(BuildContext context) {

    return Scaffold(
      backgroundColor: Colors.black,
      body: Center(
        // Center is a layout widget. It takes a single child and positions it
        // in the middle of the parent.
        child: Column(
          children: <Widget>[
            SizedBox(height:200.0),
           new Image.asset(
              'logo-white.png',
              width: 200.0,
              height: 200.0,
              fit: BoxFit.contain,
            ),
            SizedBox(
              height: 10.0
            ),
           
    Container(
          width:350,
          child:  ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 90.0,left: 90.0,top: 15.0,bottom: 15.0),child: Text(
        "Login",
        style: TextStyle(fontSize: 25,fontWeight: FontWeight.bold ,fontFamily: 'Montserrat')
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(HexColor("#00A14B")),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
        onPressed: () {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => LoginScreen()),
  );
}
    ),
                  ),
      SizedBox(
              height: 20.0
            ),
             Container(
          width:350,
          child:
      ElevatedButton(
      child: Padding(padding: EdgeInsets.only(right: 35.0,left: 50.0,top: 15.0,bottom: 15.0),child: Text(
        "Create an account",
        style: TextStyle(fontSize: 25.0,fontWeight: FontWeight.bold ,fontFamily: 'Montserrat')
      ),),
      style: ButtonStyle(
        foregroundColor: MaterialStateProperty.all<Color>(Colors.white),
        backgroundColor: MaterialStateProperty.all<Color>(Colors.transparent),
        shape: MaterialStateProperty.all<RoundedRectangleBorder>(
          RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(50),
            side: BorderSide(color: HexColor("#00A14B"))
          )
        )
      ),
     onPressed: () {
  Navigator.push(
    context,
    MaterialPageRoute(builder: (context) => CreateAccount()),
  );
}
    ),)
          ],
        ),
      ),
  
    );
  }
 }